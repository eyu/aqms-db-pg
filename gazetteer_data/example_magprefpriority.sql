/* This governs HOW the preferred magnitude is set (event.prefmag) if a new magnitude is added to the
   netmag table. The fields below are all referenced from the netmag table

   possible magalgo's:
      ---------------
      hypoinverse - auto Md
      trimag      - auto Ml,Me
      locmag      - auto Ml -> do not allow while meta-data is not maintained.
      HypoinvMd   - Jiggle Md
      trimag_prel - auto prelim Ml
      RichterMl2  - Jiggle Ml
      HAND        - hand magnitude (should override the others)

   possible subsource:
      empty = any subsource
      Jiggle
      hypomag = done by running Hypomag pcsProcessor

   Region is from the gazetteer_region table. UW region is smaller than the PNSN_Auth+ alarming region, however,
   having the DEFAULT region rules the same as the UW one assures that we use the same magprefpriority rules
   everywhere.

   Once ssh'ed on an RT host, then cd to db/ directory and run:

    ./purge_snap.pl QC_01_MASTER rtdb2

   to push the magprefpriority to the rt host in question (rtdb2 is brothers)
*/

delete from magprefpriority;

-- Jiggle Hand magnitude no longer highest priority
insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings) values
('UW', 		0, 	   3250400000,        95, 'UW',  'Jiggle',     'h',  'HAND',  -5.0,10.0, 0);

insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings) values
('DEFAULT',	0, 	   3250400000,        95, 'UW',  'Jiggle',     'h',  'HAND',  -5.0,10.0, 0);

-- Hypomag magnitudes have 1st dibs for small magnitudes
insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings) values
('UW', 		0, 	   3250400000,        115, 'UW',  'hypomag',     'd',  'HypoinvMd',  -3.0,1.0,           1);
insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings) values
('DEFAULT', 	0, 	   3250400000,        115, 'UW',  'hypomag',     'd',  'HypoinvMd',  -3.0,1.0,           1);

insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings,maxuncertainty) values
('UW', 		0, 	   3250400000,        120, 'UW',  'hypomag',     'l',    'RichterMl2',   -0.5,10.0,           1, 1.0);
insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings, maxuncertainty) values
('DEFAULT', 	0, 	   3250400000,        120, 'UW',  'hypomag',     'l',    'RichterMl2',   -0.5,10.0,           1, 1.0);

-- Jiggle ml and md next, which is pref depends on magnitudea and accuracy
insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings) values
('UW', 		0, 	   3250400000,        100, 'UW',  'Jiggle',     'd',  'HypoinvMd',  -3.0,1.0,           1);
insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings) values
('DEFAULT', 	0, 	   3250400000,        100, 'UW',  'Jiggle',     'd',  'HypoinvMd',  -3.0,1.0,           1);

insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings,maxuncertainty) values
('UW', 		0, 	   3250400000,        110, 'UW',  'Jiggle',     'l',    'RichterMl2',   -0.5,10.0,           1, 1.0);
insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings, maxuncertainty) values
('DEFAULT', 	0, 	   3250400000,        110, 'UW',  'Jiggle',     'l',    'RichterMl2',   -0.5,10.0,           1, 1.0);

-- Finally: automatic magnitudes
insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings) values
('UW', 		0, 	   3250400000,         80, 'UW',     NULL,      'd', 'hypoinverse', -3.0,10.0,           1);
insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings) values
('DEFAULT',	0, 	   3250400000,         80, 'UW',     NULL,      'd', 'hypoinverse', -3.0,10.0,           1);

insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings, maxuncertainty) values
('UW', 		0, 	   3250400000,        90, 'UW',  NULL,           'l',  'trimag',   2.8,10.0,            5, 0.5);

insert into magprefpriority 
(region_name, datetime_on, datetime_off, priority, auth, subsource, magtype, magalgo, minmag, maxmag, minreadings, maxuncertainty) values
('DEFAULT', 		0, 	   3250400000,        90, 'UW',  NULL,           'l',  'trimag',   2.8,10.0,            5, 0.5);

