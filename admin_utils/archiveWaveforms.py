#!/usr/bin/python3
# archiveWaveforms.py
# Victor Kress, PNSN, July 2012
# converted to postgreSQL and python3 7/2021
#program to take all AQMS waveforms in specified date range and move into
#archival storage

import sys
import os
import os.path
import shutil
import glob
import psycopg2
import traceback
from getpass import getpass

usage='''archiveWaveforms.py [options] startdate enddate [test]
startdate and enddate of form YYYY/MM/DD
prompts for rtem@localhost db user password
must be run on primary postprocessing machine.
'test' prevents any actual modifications to db or files
'''

#list of events that have known problems and should be skipped for now
badlist=[60017303]

#twfRoot='/scratch/wavestore/'    #debug
#archRoot='/scratch/wavearchive/'  #debug
trashRoot='/archive/trash/'
trashDir=None

def dbConnect():
    '''get connection object for local database.  Must be run on primary PP
    param password password for rtem@localhost
    return open connection object or None
    '''
    pwd=getpass(prompt='password for rtem@localhost:')
    constr="host=localhost dbname=archdb user=rtem password=%s"%pwd
    try:
        con=psycopg2.connect(constr)
    except psychopg2.Error as e:
        print (e.pgerror)
        return None
    return con

def getTWFdir(curs):
    '''get directory root for unarchived waveforms
    param curs open cursor object
    return trigger waveform file root directory
    '''
    curs.execute("SELECT fileroot FROM waveroots WHERE status='T'")
    twfRoot=curs.fetchone()[0]
    if twfRoot[-1]!='/': twfRoot=twfRoot+'/'
    return twfRoot

def getAWFdir(curs):
    '''get directory root for archived waveforms
    param curs open cursor object
    return archive waveform file root directory
    '''
    curs.execute("SELECT fileroot FROM waveroots WHERE status='A'")
    archRoot=curs.fetchone()[0]
    if archRoot[-1]!='/': archRoot=archRoot+'/'
    return archRoot

def isShadow(curs):
    '''determine if database on localhost is streaming shadow db
    param curs open cursor object
    return boolean shadow status
    '''
    curs.execute("SELECT count(*) FROM pg_stat_wal_receiver")
    if curs.fetchone()[0]==0:
        return False
    return True
    
def getTriggerWaveforms(evid,curs):
    '''
    Get unarchived waveform table data.
    @param evid event id to query
    @param curs connected cxoracle connection object
    @return list of dictionaries containing waveform table data for unarchived waveforms (status='T')
    '''
    print ('Retrieving waveform file list for event %s'%evid)
    twfDir='%s%d/'%(twfRoot,evid)
    twfd=[]
    getstr='''
           SELECT w.wfid,w.fileid,w.net,w.sta,w.seedchan,w.location,
                  w.datetime_on,w.datetime_off,w.foff,w.nbytes,f.dfile
           FROM waveform w,assocwae awe,filename f
           WHERE awe.evid=%s and awe.wfid=w.wfid and w.fileid=f.fileid
                 and w.status='T'
           '''
    curs.execute(getstr,(evid,))
    rs=curs.fetchall()
    if rs: newd={}
    #put into array of dictionaries
    for r in rs:
        newd={}
        newd['wfid']=r[0]
        newd['fileid']=r[1]
        newd['net']=r[2]
        newd['sta']=r[3]
        newd['chan']=r[4]
        newd['loc']=r[5]
        newd['dton']=r[6]
        newd['dtoff']=r[7]
        newd['nbytes']=r[9]
        newd['filename']=twfDir+r[10]
        newd['archive']=0  #0 to leave unchanged
        if r[8] or r[9]==0:
            print ('!!!!waveform table wfid=%d (unarchived) has foff=%d and nbytes=%d'%(r[8],r[9]))
        print ('adding file %s'%newd['filename'])
        twfd.append(newd)
    #check results for redundancy and consistency
    filedic={}
    for i in range(len(twfd)):
        f=twfd[i]['filename']
        if os.path.exists(f):
            #print ('%s exists'%f)
            twfd[i]['archive']=-1  #will select unique one to archive in next section
            nbytes=os.path.getsize(f)
            if nbytes==0:
                print ("Found zero length waveform file %s"%f)
                continue
            score=abs(nbytes-twfd[i]['nbytes'])
            if f in filedic:
                if score<filedic[f]['bestscore']:
                    filedic[f]['i']=i
                    filedic[f]['bestscore']=score
                    filedic[f]['nref']+=1
            else:
                filedic[f]={'i':i,'bestscore':score,'nref':1}
        else:
            print ('Referenced waveform file "%s" does not exist.'%f)
    #pick closest matches for each existing file for archiving
    for f in filedic.keys():
        if filedic[f]['nref']>1:
            print ('File %s has %d refs. Archiving index %d'%(f,filedic[f]['nref'],filedic[f]['i']))
        twfd[filedic[f]['i']]['archive']=1 #archive this file
    return twfd

def archiveEventWaveforms(evid,con,test=True):
    '''
    move all waveforms for specified event into single
    file in appropriate location and make required db changes.
    @param evid AQMS event ID
    @param con open connection object
    '''
    twfDir='%s%d/'%(twfRoot,evid)
    initArchFileEnd=archfile=archFileName=None
    if test:
        print ('running archiveEventWaveforms() in test mode for event %d'%evid)
    try:
        curs=con.cursor()
        narch=0 #number of waveforms moved

        # First create or check target archive file##########################
        # first get origin time
        getstr='''SELECT truetime.true2stringf(o.datetime) 
                  FROM origin o,event e 
                  WHERE e.evid=%s AND e.prefor=o.orid
               '''
        curs.execute(getstr,(evid,))
        r=curs.fetchone()
        #make archive directory string from orign time.
        ymd=r[0].split(' ')[0].split('/')
        (yr,mo,dy)=map(int,ymd)
        subDir='/%4d/%4d%02d/'%(yr,yr,mo)
        archFileDir=archRoot+subDir
        if not os.path.isdir(archFileDir):
            if test:
                print ('test mode: not creating %s'%archFileDir)
            else:
                os.makedirs(archFileDir)
        #make archive file string and open target for writing
        archFileName='%s%d'%(archFileDir,evid)
        if os.path.exists(archFileName):
            print ('Target archive file %s already exists. Appending.'%archFileName)
                
        if test:
            testfile='test.out'
            print ('opening %s rather than %s for output'%(testfile,archFileName))
            archFile=open(testfile,'ab')
        else:
            archFile=open(archFileName,'ab')
        #save initial index of eof
        initArchFileEnd=archFile.tell()
        print ('Opened archive file and write pointer set to %d'%initArchFileEnd)

        # get or create database subdirid for archive file #################
        getstr="SELECT subdirid FROM subdir WHERE subdirname LIKE '%%%s%%'"%subDir[:-1]
        curs.execute(getstr)
        r=curs.fetchall()
        if r: #exists
            subdirid=r[0][0]
            if len(r)>1:
                print ('%d entries for subdir=%s'%subDir)
        else: #create new
            if test:
                subdirid=666
                print ('would be creating subdir entry')
            else:
                #create new subdirid
                curs.execute("SELECT nextval('sdseq')")
                subdirid=curs.fetchone()[0]
                #and corresponding table row
                putstr='''
                       INSERT INTO subdir (subdirid,subdirname,lddate)
                       VALUES (%s,%s,timezone('UTC'::text, now()))
                '''
                curs.execute(putstr,(subdirid,subDir))

        # get or create db fileid for archive file ###########################
        archon=archoff=None      
        getstr="SELECT fileid,datetime_on,datetime_off FROM filename WHERE dfile='%s' AND subdirid=%s"%(evid,subdirid)
        curs.execute(getstr)
        r=curs.fetchall()
        if r:
            archfileid=r[0][0]
            archon=r[0][1]
            archoff=r[0][2]
            if len(r)>1:
                print ('%d entries for archfile=%d!!!!!!'%evid)
        else: #create new
            if test:
                archfileid=666
                print ('would be inserting archive filename row: fid=%d dfile=%d'%(archfileid,evid))
            else:
                #get new fileid
                curs.execute("SELECT nextval('fiseq')")
                archfileid=curs.fetchone()[0]
                #and create corresponding Filename row with file values to be inserted later
                putstr='''
                       INSERT INTO filename 
                       (fileid,dfile,datetime_on,datetime_off,nbytes,subdirid,lddate)
                       VALUES (%s,%s,0,0,0,%s,timezone('UTC'::text, now()))
                '''
                curs.execute(putstr,(archfileid,str(evid),subdirid))

        #retrieve all waveform files for this event########################
        trigWFdicts=getTriggerWaveforms(evid,curs)
        #and iterate through trigger waveform db entries
        for twfd in trigWFdicts:
            if twfd['archive']==1:  #archive file
                #append to archive file
                offset=archFile.tell()  #current end of file
                msf=open(twfd['filename'],'rb')
                #archFile.writelines(msf)
                archFile.write(msf.read())
                nbytes=msf.tell()
                msf.close()
                if nbytes!=archFile.tell()-offset:
                    print ('%d!=%d in wf transfer'%(nbytes,archFile.tell()-offset))
                    #raise ValueError
                if (not archon) or twfd['dton']<archon:
                    archon=twfd['dton']
                if (not archoff) or twfd['dtoff']>archoff:
                    archoff=twfd['dtoff']
                if test:
                    print ('would update waveform table wfid=%d fid=%d fof=%d'%(twfd['wfid'],
                                                                         archfileid,
                                                                         offset))
                else:
                    putstr='''
                           UPDATE waveform 
                           SET fileid=%s,foff=%s,traceoff=%s,status='A'
                           WHERE wfid=%s
                    '''
                    curs.execute(putstr,(archfileid,offset,offset,twfd['wfid']))
                    curs.execute("DELETE FROM filename WHERE fileid=%d"%twfd['fileid'])
                narch+=1
                waveformArchived=True
        #Done appending waveform file. Now update filename table entry
        filelen=archFile.tell()
        archFile.close()
        if narch>0:
            trashDir='%s%d/'%(trashRoot,evid)
            if not test:
                os.mkdir(trashDir)
            #update waveform table for achived waveforms
            putstr='''
                   UPDATE filename
                   SET datetime_on=%s,
                       datetime_off=%s,
                       nbytes=%s
                   WHERE fileid=%s
                   '''
            if test:
                print ('would be updating archive filename row: fid=%d dfile=%d'%(archfileid,evid))
            else:
                curs.execute(putstr,(archon,archoff,filelen,archfileid))
            #clean up waveform, AssocWaE and filename tables
            for twfd in trigWFdicts:
                # have to avoid deleting filenames or waveforms associated with other events
                rmFilename=rmWaveform=True
                getstr='''
                       SELECT awe.evid,w.wfid
                       FROM filename f,waveform w,assocwae awe
                       WHERE f.fileid=%s AND f.fileid=w.fileid AND w.wfid=awe.wfid 
                       AND NOT awe.evid=%s
                    '''
                curs.execute(getstr,(twfd['fileid'],evid))
                rs=curs.fetchall()
                for r in rs:
                    print ('fileid=%d refd in event %d via wfid %d. SHOULD NOT SEE.'%(twfd['fileid'],r[0],r[1]))
                    rmFilename=False
                getstr='''
                       SELECT evid
                       FROM assocwae
                       WHERE wfid=%s
                       AND NOT evid=%s
                    '''
                curs.execute(getstr,(twfd['wfid'],evid))
                rs=curs.fetchall()
                for r in rs:
                    print ('Waveform.wfid=%d referenced in event %d.'%(twfd['wfid'],r[0]))
                    rmWaveform=False
                # end of consistency check
                if twfd['archive']==1 and rmFilename:  #archived file
                    if test:
                        print ('would be deleting original file and filename entries for archived file')
                    else:
                        curs.execute('DELETE FROM filename WHERE fileid=%s',(twfd['fileid'],))
                        if os.path.exists(twfd['filename']):
                            if test:
                                print ('would be moving %s to %s'% (twfd['filename'],trashDir))
                            else:
                                shutil.move(twfd['filename'],trashDir)
                elif twfd['archive']==-1: #duplicate db entries will be removed.
                    if test:
                        print ('would be deleting waveform assocwae and filename entries for duplicate reference')
                    else:
                        if rmWaveform:
                            curs.execute('DELETE FROM AssocWaE WHERE wfid=%s',(twfd['wfid'],))
                            curs.execute('DELETE FROM waveform WHERE wfid=%s',(twfd['wfid'],))
                        if rmFilename:
                            curs.execute('DELETE FROM filename WHERE fileid=%s',(twfd['fileid'],))
                else: #db entry has no corresponding file.  Leave in place as missing data marker.
                    print ('No file for db entry.  Leaving in place.')
            
        else:
            print ('No triggers archived for this event.')

    except Exception as e:
        traceback.print_exc()
        print (e)
        print ('rolling back db changes')
        curs.close()
        if test:
            print ('Test mode.  Nothing to roll back')
        else:
            con.rollback()
            if archfile:
                if initArchFileEnd:  #only remove entries saved this session
                    print ('Rolling back %s to state before this run (%d).'%(archFileName,initArchFileEnd))
                    archfile.seek(initArchFileEnd)
                    archfile.truncate()
                    archfile.close()
                else:                #whole file created this session. Remove
                    print ('Deleting %s created this session.'%archiveFileName)
                    archfile.close()
                    os.remove(archFileName)
            if trashDir!=None and os.is_dir(trashDir()):
                for f in os.listdir(trashDir): #move trashed files back
                    os.copy(trashDir+f,twfDir+f)
        return -1

    #clean up old directory if empty
    if os.path.exists(twfDir): 
        if os.listdir(twfDir):
            print ("Source dir %s not empty. Keeping."%twfDir)
        else:
            if test:
                print ("Would be removing empty source dir "+twfDir)
            else:
                print ("Removing empty source dir "+twfDir)
                os.rmdir(twfDir)
    else:
        print ('Directory %s does not exist. Cannot clean.'%twfDir)

    #wrap up
    curs.close()
    if test:
        con.rollback()
    else:
        con.commit()
    return narch ## End of  archiveEventWaveforms()

if __name__=='__main__':
    test=False
    #test=True
    if len(sys.argv)<3:
        print (usage)
        sys.exit(0)
    start=sys.argv[1]
    end=sys.argv[2]
    if len(sys.argv)==4 and sys.argv[3]=='test':
       test=True 
    con=dbConnect()
    if not con:
        print ('could not connect to db with "%s"'%constr)
        sys.exit(-1)
    curs=con.cursor()
    if not curs:
        print ('could not establish db connection')
        con.close()
        sys.exit(1)
    if isShadow(curs):
        print ('running on shadow. Must run on primary pp machine')
        curs.close()
        con.close()
        sys.exit(1)
        
    twfRoot=getTWFdir(curs)
    archRoot=getAWFdir(curs)

    print ('Archiving files from %s to %s'%(twfRoot,archRoot))

    #get list of event ids
    getstr='''SELECT distinct e.evid 
              FROM event e,origin o,assocwae awe,waveform w
              WHERE e.prefor=o.orid AND e.evid=awe.evid 
              AND awe.wfid=w.wfid AND w.status='T'
              AND o.datetime BETWEEN truetime.string2true(%s)
                             AND truetime.string2true(%s)
           '''
    curs.execute(getstr,(start,end))
    r=curs.fetchall()
    curs.close()
    if len(r)==0:
        print ('no events found in requested time window')
        sys.exit(0)
    evids=[i[0] for i in r]
    #evids=[r[0][0]] #debug
    print ('%d events found in specified time window'%len(evids))
    for evid in evids:
        if evid in badlist:
            print ('skipping bad event %d'%evid)
            continue
        narch=archiveEventWaveforms(evid,con,test)
        if narch<0:
            print ('archiveWaveforms failed on evid=%d'%evid)
            con.close()
            sys.exit(-1)
        else:
            print ('archived %d channels for evid=%d'%(narch,evid))
    print ('done')
    con.commit()
    con.close()
    sys.exit(0)
