#!/bin/bash

# set variables
source pg.env
CMD="${PSQLDIR}/psql -p $PGPORT"
BASEDIR=$PWD

if [ "$DBNAME" = "" ]; then
  echo "DBNAME is not defined, source "pg.env" as postgres first. Exiting.."
  exit
fi

if [ -e ${HOME}/.pgpass ]; then
   PWFLAG=-w   # will not prompt for password
else
   PWFLAG=-W   # will prompt for password
fi

echo "Setting up AQMS database $DBNAME..."

echo "    next: Creating sequences in ${DBNAME}. Continue?"
select yn in "Yes" "Skip" "Exit"; do
case $yn in
   Yes ) break;;
   Skip ) break;;
   Exit ) exit;;
esac
done

if [ $yn != "Skip" ]; then
    echo "--> Creating sequences in database $DBNAME as user trinetdb..."
    echo "$CMD -U trinetdb $PWFLAG -d $DBNAME < ${BASEDIR}/sequences/create_${DBNAME}_sequences.sql"
    $CMD -U trinetdb $PWFLAG -d $DBNAME < ${BASEDIR}/sequences/create_${DBNAME}_sequences.sql

    echo "--> Granting privileges on sequences in database $DBNAME as user trinetdb..."
    echo "$CMD -U trinetdb $PWFLAG -d $DBNAME < ${BASEDIR}/sequences/grant_all_sequences.sql"
    $CMD -U trinetdb $PWFLAG -d $DBNAME < ${BASEDIR}/sequences/grant_all_sequences.sql
fi

echo "    next: Install waveform_schema tables in ${DBNAME}. Continue?"
select yn in "Yes" "Skip" "Exit"; do
case $yn in
   Yes ) break;;
   Skip ) break;;
   Exit ) exit;;
esac
done

if [ $yn != "Skip" ]; then
    echo "--> Installing waveform_schema tables in $DBNAME as user trinetdb..."
    echo "cd ${BASEDIR}/tables/waveform_schema"
    cd ${BASEDIR}/tables/waveform_schema
    echo "$CMD -U trinetdb $PWFLAG -d $DBNAME < install_waveform_tables.sql"
    $CMD -U trinetdb $PWFLAG -d $DBNAME < install_waveform_tables.sql
fi

echo "    next: Install parametric_schema tables in ${DBNAME}. Continue?"
select yn in "Yes" "Skip" "Exit"; do
case $yn in
   Yes ) break;;
   Skip ) break;;
   Exit ) exit;;
esac
done

if [ $yn != "Skip" ]; then
    echo "--> Installing parametric_schema tables in $DBNAME as user trinetdb..."
    echo "cd ${BASEDIR}/tables/parametric_schema"
    cd ${BASEDIR}/tables/parametric_schema
    echo "$CMD -U trinetdb $PWFLAG -d $DBNAME < install_parametric_tables.sql"
    $CMD -U trinetdb $PWFLAG -d $DBNAME < install_parametric_tables.sql
fi

echo "    next: Install instrument_response_schema tables in ${DBNAME}. Continue?"
select yn in "Yes" "Skip" "Exit"; do
case $yn in
   Yes ) break;;
   Skip ) break;;
   Exit ) exit;;
esac
done

if [ $yn != "Skip" ]; then
    echo "--> Installing instrument_response_schema tables in $DBNAME as user trinetdb..."
    echo "cd ${BASEDIR}/tables/instrument_response_schema"
    cd ${BASEDIR}/tables/instrument_response_schema
    echo "$CMD -U trinetdb $PWFLAG -d $DBNAME < install_instrument_response_tables.sql"
    $CMD -U trinetdb $PWFLAG -d $DBNAME < install_instrument_response_tables.sql
fi

echo "    next: Install hardware_schema tables in ${DBNAME}. Continue?"
select yn in "Yes" "Skip" "Exit"; do
case $yn in
   Yes ) break;;
   Skip ) break;;
   Exit ) exit;;
esac
done

if [ $yn != "Skip" ]; then
    echo "--> Installing hardware_schema tables in $DBNAME as user trinetdb..."
    echo "cd ${BASEDIR}/tables/hardware_schema"
    cd ${BASEDIR}/tables/hardware_schema
    echo "$CMD -U trinetdb $PWFLAG -d $DBNAME < install_hardware_tables.sql"
    $CMD -U trinetdb $PWFLAG -d $DBNAME < install_hardware_tables.sql
fi

echo "    next: Install application_schema tables in ${DBNAME}. Continue?"
select yn in "Yes" "Skip" "Exit"; do
case $yn in
   Yes ) break;;
   Skip ) break;;
   Exit ) exit;;
esac
done

if [ $yn != "Skip" ]; then
    echo "--> Installing application_schema tables in $DBNAME as user trinetdb..."
    echo "cd ${BASEDIR}/tables/application_schema"
    cd ${BASEDIR}/tables/application_schema
    echo "$CMD -U trinetdb $PWFLAG -d $DBNAME < install_application_tables.sql"
    $CMD -U trinetdb $PWFLAG -d $DBNAME < install_application_tables.sql
fi

echo "    next: Grant privileges on all tables in schema trinetdb in ${DBNAME}. Continue?"
select yn in "Yes" "Skip" "Exit"; do
case $yn in
   Yes ) break;;
   Skip ) break;;
   Exit ) exit;;
esac
done

if [ $yn != "Skip" ]; then
    echo "--> Granting privileges on all tables in schema trinetdb in database $DBNAME as user trinetdb..."
    echo "$CMD -U trinetdb $PWFLAG -d $DBNAME < ${BASEDIR}/tables/grant_all_tables.sql"
    $CMD -U trinetdb $PWFLAG -d $DBNAME < ${BASEDIR}/tables/grant_all_tables.sql
fi

echo "    next: Generate VIEWS needed by Jiggle in ${DBNAME}. Continue?"
select yn in "Yes" "Skip" "Exit"; do
case $yn in
   Yes ) break;;
   Skip ) break;;
   Exit ) exit;;
esac
done

if [ $yn != "Skip" ]; then
    echo "--> Generating pre-defined views needed by Jiggle in database $DBNAME as user trinetdb..."
    echo "cd ${BASEDIR}/../views"
    cd ${BASEDIR}/../views
    echo "$CMD -U trinetdb $PWFLAG -d $DBNAME < generate_views.sql"
    $CMD -U trinetdb $PWFLAG -d $DBNAME < generate_views.sql
    echo "$CMD -U trinetdb $PWFLAG -d $DBNAME < grant_views.sql"
    $CMD -U trinetdb $PWFLAG -d $DBNAME < grant_views.sql
fi

echo "    next: Load stored procedures (functions) in ${DBNAME}. Continue?"
select yn in "Yes" "Skip" "Exit"; do
case $yn in
   Yes ) break;;
   Skip ) break;;
   Exit ) exit;;
esac
done

if [ $yn != "Skip" ]; then
    echo "--> Loading stored procedures (functions) into database $DBNAME as user code..."
    echo "cd ${BASEDIR}/../storedprocedures"
    cd ${BASEDIR}/../storedprocedures
    echo "$CMD -U code $PWFLAG -d $DBNAME < install_as_user_code.sql"
    $CMD -U code $PWFLAG -d $DBNAME < install_as_user_code.sql
    echo "--> Loading some stored procedures (functions) into database $DBNAME as user postgres..."
    echo "$CMD -d $DBNAME < install_as_user_postgres.sql"
    $CMD -d $DBNAME < install_as_user_postgres.sql
    echo "--> Loading some stored procedures (functions) into database $DBNAME as user trinetdb..."
    echo "$CMD -U trinetdb -d $DBNAME < install_as_user_trinetdb"
    $CMD -d $DBNAME < install_as_user_trinetdb.sql
fi

echo "    Done! You can now load some data into the DB."
