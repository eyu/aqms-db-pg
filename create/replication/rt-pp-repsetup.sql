--Creates replication set for AQMS RT->PP replication.
--Script must be run on all RT data sources
--PNSN 2017/0914
--$Id$

SELECT pglogical.drop_replication_set(set_name:='rtrepset');
SELECT pglogical.create_replication_set(set_name:='rtrepset',
      replicate_insert:=TRUE,replicate_update:=TRUE,
      replicate_delete:=FALSE, replicate_truncate:=FALSE);

DO $$
DECLARE
	count INTEGER := 0;
	reptables varchar[] := ARRAY['amp','ampset',
	'arrival','assocamm','assocamo','assocaro','assoccom','assoccoo',
	'assocevampset','assocnte','assocwae','coda','credit','credit_alias',
	'event','eventprefmag','eventprefmec','eventprefor',
	'filename', 'mec','mecchannel', 'mecdata','mecfreq',
        'mecfreqdata','mecobject','netmag', 'nettrig','origin',
        'origin_error','pathname','remark','request_card','subdir',
	'trig_channel','unassocamp','waveform'];
	tbl varchar;
BEGIN
	FOREACH tbl IN ARRAY reptables LOOP
	PERFORM pglogical.replication_set_add_table(set_name:='rtrepset',
	       relation:='trinetdb.'||tbl,synchronize_data:=TRUE);
	count := count+1;
	END LOOP;
--	RETURN count ;
END;
$$;

