--- this script create tables in AQMS Instrument Response part of schema.
--- run as TRINETDB user
---
\i create_CHANNEL_COMMENT.sql
\i create_CHANNEL_DATA.sql
\i create_COEFFICIENTS.sql
\i create_DC.sql
\i create_DC_DATA.sql
\i create_DECIMATION.sql
\i create_DM.sql
\i create_D_ABBREVIATION.sql
\i create_D_COMMENT.sql
\i create_D_FORMAT.sql
\i create_D_FORMAT_DATA.sql
\i create_D_UNIT.sql
\i create_EQUIPMENT.sql
\i create_PN.sql
\i create_PN_DATA.sql
\i create_POLES_ZEROS.sql
\i create_POLYNOMIAL.sql
\i create_PZ.sql
\i create_PZ_DATA.sql
\i create_SENSITIVITY.sql
\i create_SIMPLE_RESPONSE.sql
\i create_STATION_COMMENT.sql
\i create_STATION_DATA.sql

