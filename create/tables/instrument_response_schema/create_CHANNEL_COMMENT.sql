-- 
-- TABLE: Channel_Comment 
--

CREATE TABLE Channel_Comment(
    net              VARCHAR(8)     NOT NULL,
    sta              VARCHAR(6)     NOT NULL,
    seedchan         VARCHAR(3)     NOT NULL,
    location         VARCHAR(2)     NOT NULL,
    ondate           TIMESTAMP            NOT NULL,
    comment_id       INTEGER    NOT NULL,
    channel          VARCHAR(8),
    channelsrc       VARCHAR(8),
    offdate          TIMESTAMP,
    comment_level    INTEGER    NOT NULL,
    LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT ChC00 PRIMARY KEY (net, sta, seedchan, location, ondate, 
	comment_id)
);

