-- 
-- TABLE: Equipment
--

DROP TABLE Equipment;
DROP TYPE equipment_type;
DROP SEQUENCE EQSEQ;

-- CREATE type equipment_type as enum('sensor', 'data_logger', 'preamplifier');

CREATE SEQUENCE EQSEQ
    START WITH 1000001
    INCREMENT BY 5
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
;

CREATE TABLE Equipment(
    net              VARCHAR(8)         NOT NULL,
    sta              VARCHAR(6)         NOT NULL,
    seedchan         VARCHAR(3)         NOT NULL,
    location         VARCHAR(2)         NOT NULL,
    ondate           TIMESTAMP          NOT NULL,
    channel          VARCHAR(8),
    channelsrc       VARCHAR(8),
    id               BIGINT  NOT NULL DEFAULT nextval('EQSEQ'),
    type             VARCHAR(80),
    description      VARCHAR(80),
    manufacturer     VARCHAR(80),
    vendor           VARCHAR(80),
    model            VARCHAR(80),
    serialnumber     VARCHAR(80),
    offdate          TIMESTAMP,
    calibration_dates DATE[],
    lddate           TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'
  ),
--   type             equipment_type,

    CONSTRAINT Eq00 PRIMARY KEY (net, sta, seedchan, location, ondate, id)
);

GRANT ALL on EQSEQ to rtem;
GRANT ALL on Equipment to rtem;

--
--  CONSTRAINT CHD09 CHECK (samprate>=0),
--
