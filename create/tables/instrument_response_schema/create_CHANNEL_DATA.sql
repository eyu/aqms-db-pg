-- 
-- TABLE: Channel_Data 
--

CREATE TABLE Channel_Data(
    net              VARCHAR(8)         NOT NULL,
    sta              VARCHAR(6)         NOT NULL,
    seedchan         VARCHAR(3)         NOT NULL,
    location         VARCHAR(2)         NOT NULL,
    ondate           TIMESTAMP                NOT NULL,
    channel          VARCHAR(8),
    channelsrc       VARCHAR(8),
    inid             INTEGER,
    remark           VARCHAR(60),
    unit_signal      INTEGER,
    unit_calib       INTEGER,
    lat              DOUBLE PRECISION,
    lon              DOUBLE PRECISION,
    elev             DOUBLE PRECISION,
    edepth           DOUBLE PRECISION,
    azimuth          DOUBLE PRECISION,
    dip              DOUBLE PRECISION,
    format_id        INTEGER,
    record_length    INTEGER,
    samprate         DOUBLE PRECISION,
    clock_drift      DOUBLE PRECISION,
    flags            VARCHAR(27),
    offdate          TIMESTAMP,
    lddate           TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'
       ),
    CONSTRAINT CHD01 CHECK (azimuth >=0 and azimuth <=360),
    CONSTRAINT CHD02 CHECK (clock_drift >=0),
    CONSTRAINT CHD03 CHECK (dip >=-90 and dip<=90),
    CONSTRAINT CHD04 CHECK (edepth >=0),
    CONSTRAINT CHD06 CHECK (lat >=-90 and lat <=90 ),
    CONSTRAINT CHD07 CHECK (lon >=-180 and lon <=180 ),
    CONSTRAINT CHD08 CHECK (record_length>=8 and  record_length <=12 ),
    CONSTRAINT CHD09 CHECK (samprate>=0),
    CONSTRAINT ChD00 PRIMARY KEY (net, sta, seedchan, location, ondate)
);
