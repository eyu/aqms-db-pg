-- 
-- TABLE: DM 
--

CREATE TABLE DM(
    key           INTEGER        NOT NULL,
    name          VARCHAR(80),
    samprate      DOUBLE PRECISION    NOT NULL,
    factor        INTEGER        NOT NULL,
    i_offset        INTEGER,
    delay         DOUBLE PRECISION,
    correction    DOUBLE PRECISION    NOT NULL,
    LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT DM00 PRIMARY KEY (key)
);
