-- 
-- TABLE: D_Format 
--

CREATE TABLE D_Format(
    id        INTEGER    NOT NULL,
    name      VARCHAR(80),
    family    INTEGER    NOT NULL,
    ms_id     INTEGER    NOT NULL,
    CONSTRAINT D_F00 PRIMARY KEY (id)
);
