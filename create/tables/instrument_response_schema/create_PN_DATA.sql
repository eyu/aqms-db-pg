-- 
-- TABLE: PN_Data 
--

CREATE TABLE PN_Data(
    key         INTEGER        NOT NULL,
    row_key     INTEGER        NOT NULL,
    pn_value    DOUBLE PRECISION,
    CONSTRAINT PND00 PRIMARY KEY (key, row_key)
);
