--
-- TABLE: Station_Sensor
--

CREATE TABLE Station_Sensor(
    sta             VARCHAR(6)         NOT NULL,
    net             VARCHAR(8)         NOT NULL,
    sensor_nb       INTEGER        NOT NULL,
    ondate          TIMESTAMP                NOT NULL,
    sensor_id       INTEGER        NOT NULL,
    lat             DOUBLE PRECISION,
    lon             DOUBLE PRECISION,
    elev            DOUBLE PRECISION,
    edepth          DOUBLE PRECISION,
    nb_component    INTEGER        NOT NULL,
    datumhor        VARCHAR(8),
    datumver        VARCHAR(8),
    offdate         TIMESTAMP,
    lddate          TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT StSe00 PRIMARY KEY (sta, net, sensor_nb, ondate)
)
;

