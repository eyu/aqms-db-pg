--
-- TABLE: Sensor
--

CREATE TABLE Sensor(
    sensor_id       INTEGER    NOT NULL,
    name            VARCHAR(80),
    serial_nb       VARCHAR(80),
    ondate          TIMESTAMP            NOT NULL,
    offdate         TIMESTAMP,
    nb_component    INTEGER    NOT NULL,
    lddate          TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT Sensor00 PRIMARY KEY (sensor_id)
)
;

