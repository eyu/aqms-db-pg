--
-- TABLE: Filter_Sequence
--

CREATE TABLE Filter_Sequence(
    seqfil_id    INTEGER        NOT NULL,
    name         VARCHAR(32)        NOT NULL,
    nb_filter    INTEGER        NOT NULL,
    gain         DOUBLE PRECISION,
    frequency    DOUBLE PRECISION,
    lddate       TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT FiS00 PRIMARY KEY (seqfil_id)
)
;

