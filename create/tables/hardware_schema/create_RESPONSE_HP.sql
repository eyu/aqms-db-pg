--
-- TABLE: Response_HP
--

CREATE TABLE Response_HP(
    hp_id            INTEGER        NOT NULL,
    filter_type      VARCHAR(2)         NOT NULL,
    nb_pole          INTEGER        NOT NULL,
    corner_freq      DOUBLE PRECISION    NOT NULL,
    damping_value    DOUBLE PRECISION    NOT NULL,
    lddate           TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT RHP00 PRIMARY KEY (hp_id)
)
;

