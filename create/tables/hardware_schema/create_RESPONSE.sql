--
-- TABLE: Response
--

CREATE TABLE Response(
    seqresp_id    INTEGER    NOT NULL,
    resp_nb       INTEGER    NOT NULL,
    resp_type     VARCHAR(1)     NOT NULL,
    resp_id       INTEGER    NOT NULL,
    unit_in       INTEGER    NOT NULL,
    unit_out      INTEGER    NOT NULL,
    r_type        VARCHAR(1),
    lddate        TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT Re00 PRIMARY KEY (seqresp_id, resp_nb)
)
;
