--
-- TABLE: Filter_Sequence_Data
--

CREATE TABLE Filter_Sequence_Data(
    seqfil_id    INTEGER    NOT NULL,
    filter_nb    INTEGER    NOT NULL,
    filter_id    INTEGER    NOT NULL,
    CONSTRAINT FiSeD00 PRIMARY KEY (seqfil_id, filter_nb)
)
;

