--
-- TABLE: Response_PN_Data
--

CREATE TABLE Response_PN_Data(
    pn_id       INTEGER        NOT NULL,
    pn_nb       INTEGER        NOT NULL,
    pn_value    DOUBLE PRECISION    NOT NULL,
    CONSTRAINT RPND00 PRIMARY KEY (pn_id, pn_nb)
)
;

