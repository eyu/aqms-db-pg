--
-- TABLE: Response_PZ
--

CREATE TABLE Response_PZ(
    pz_id      INTEGER        NOT NULL,
    pz_nb      INTEGER        NOT NULL,
    type       VARCHAR(1)         NOT NULL,
    r_value    DOUBLE PRECISION    NOT NULL,
    r_error    DOUBLE PRECISION,
    i_value    DOUBLE PRECISION    NOT NULL,
    i_error    DOUBLE PRECISION,
    lddate     TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT RPZ00 PRIMARY KEY (pz_id, pz_nb)
)
;

