--
-- TABLE: Filamp
--

CREATE TABLE Filamp(
    filamp_id      INTEGER    NOT NULL,
    name           VARCHAR(80),
    serial_nb      VARCHAR(80),
    ondate         TIMESTAMP            NOT NULL,
    offdate        TIMESTAMP,
    nb_pchannel    INTEGER    NOT NULL,
    lddate         TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT Filamp00 PRIMARY KEY (filamp_id)
)
;

