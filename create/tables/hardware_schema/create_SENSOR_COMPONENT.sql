--
-- TABLE: Sensor_Component
--

CREATE TABLE Sensor_Component(
    sensor_id         INTEGER        NOT NULL,
    component_nb      INTEGER        NOT NULL,
    channel_comp      VARCHAR(2),
    component_type    VARCHAR(1),
    sensitivity       DOUBLE PRECISION    NOT NULL,
    frequency         DOUBLE PRECISION,
    seqresp_id        INTEGER,
    lddate            TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC') ,
    CONSTRAINT SeC00 PRIMARY KEY (sensor_id, component_nb)
)
;

