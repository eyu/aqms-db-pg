--
-- TABLE: Filter
--

CREATE TABLE Filter(
    filter_id      INTEGER        NOT NULL,
    gain           DOUBLE PRECISION,
    frequency      DOUBLE PRECISION,
    in_sp_rate     DOUBLE PRECISION,
    out_sp_rate    DOUBLE PRECISION,
    i_offset         INTEGER,
    delay          DOUBLE PRECISION,
    correction     DOUBLE PRECISION    NOT NULL,
    seqresp_id     INTEGER,
    lddate         TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT Fi00 PRIMARY KEY (filter_id)
)
;

