--
-- TABLE: Station_Digitizer_PChannel
--

CREATE TABLE Station_Digitizer_PChannel(
    sta              VARCHAR(6)     NOT NULL,
    net              VARCHAR(8)     NOT NULL,
    digi_nb          INTEGER    NOT NULL,
    pchannel_nb      INTEGER    NOT NULL,
    ondate           TIMESTAMP            NOT NULL,
    data_nb          INTEGER    NOT NULL,
    data_pchannel    INTEGER    NOT NULL,
    digi_type        VARCHAR(3)     NOT NULL,
    digi_polarity    VARCHAR(1)     NOT NULL,
    digi_channel     INTEGER    NOT NULL,
    offdate          TIMESTAMP,
    lddate           TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT StDiP00 PRIMARY KEY (sta, net, digi_nb, pchannel_nb, ondate)
)
;

