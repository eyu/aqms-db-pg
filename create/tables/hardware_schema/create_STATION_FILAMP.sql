--
-- TABLE: Station_Filamp
--

CREATE TABLE Station_Filamp(
    sta            VARCHAR(6)     NOT NULL,
    net            VARCHAR(8)     NOT NULL,
    filamp_nb      INTEGER    NOT NULL,
    ondate         TIMESTAMP            NOT NULL,
    filamp_id      INTEGER    NOT NULL,
    nb_pchannel    INTEGER    NOT NULL,
    offdate        TIMESTAMP,
    lddate         TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT StFi00 PRIMARY KEY (sta, net, filamp_nb, ondate)
)
;

