--
-- TABLE: Station_Digitizer
--

CREATE TABLE Station_Digitizer(
    sta                VARCHAR(6)     NOT NULL,
    net                VARCHAR(8)     NOT NULL,
    digi_nb            INTEGER    NOT NULL,
    ondate             TIMESTAMP            NOT NULL,
    serial_nb          VARCHAR(80)    NOT NULL,
    nb_pri_pchannel    INTEGER    NOT NULL,
    nb_aux_pchannel    INTEGER    NOT NULL,
    offdate            TIMESTAMP,
    lddate             TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT StDi00 PRIMARY KEY (sta, net, digi_nb, ondate)
)
;

