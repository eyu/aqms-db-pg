--
-- TABLE: Filter_FIR
--

CREATE TABLE Filter_FIR(
    fir_id      INTEGER        NOT NULL,
    name        VARCHAR(80),
    symmetry    VARCHAR(1)         NOT NULL,
    gain        DOUBLE PRECISION,
    lddate      TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT FFIR00 PRIMARY KEY (fir_id)
)
;

