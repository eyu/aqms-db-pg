--
-- TABLE: Station
--

CREATE TABLE Station(
    sta          VARCHAR(6)         NOT NULL,
    net          VARCHAR(8)         NOT NULL,
    lat          DOUBLE PRECISION,
    lon          DOUBLE PRECISION,
    elev         DOUBLE PRECISION,
    staname      VARCHAR(60),
    nb_sensor    INTEGER,
    nb_filamp    INTEGER,
    nb_digi      INTEGER        NOT NULL,
    nb_data      INTEGER        NOT NULL,
    datumhor     VARCHAR(8),
    datumver     VARCHAR(8),
    ondate       TIMESTAMP                NOT NULL,
    offdate      TIMESTAMP,
    lddate       TIMESTAMP                DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT St00 PRIMARY KEY (sta, net, ondate)
)
;

