create table AMPSETTYPES 
(AMPSETTYPE VARCHAR(20) not null,
DESCRIPTION VARCHAR(80));

alter table AMPSETTYPES add constraint ampsettypeskey01 primary key
(ampsettype);


comment on table AMPSETTYPES is 'This table describes the list of valid amplitude set types';
comment on column AMPSETTYPES.AMPSETTYPE is 'The unique identifier of an amp set type';
comment on column AMPSETTYPES.DESCRIPTION is 'The long description of an amplitude set type.';

-- create or replace public synonym ampsettypes for ampsettypes;
-- grant select on ampsettypes to trinetdb_read, code;
-- grant insert, update, delete on ampsettypes to trinetdb_write, code;

---- populate table ---
insert into ampsettypes values ('sm','strong ground motion');
