
CREATE TABLE EventPrefMec (
    evid          BIGINT      NOT NULL,
    mechtype      VARCHAR(2)     NOT NULL,
    mecid         BIGINT      NOT NULL,
    lddate        TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT EventPrefMec_PK PRIMARY KEY (evid, mechtype),
    CONSTRAINT EventPrefMec_FK01 FOREIGN KEY (evid) REFERENCES EVENT(evid),
    CONSTRAINT EventPrefMec_FK02 FOREIGN KEY (mecid) REFERENCES MEC(mecid)
);

