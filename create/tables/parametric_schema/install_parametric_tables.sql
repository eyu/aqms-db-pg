--- this script creates tables of CISN Parametric schema.
--- run as TRINETDB user
--- these are ordered for dependencies
\i create_EVENTTYPE.sql
\i create_EVENTCATEGORY.sql
\i create_EVENT.sql
--
\i create_AMP.sql
\i create_AMPSETTYPES.sql
\i create_AMPSET.sql
\i create_ARRIVAL.sql
\i create_ASSOCAMM.sql
\i create_ASSOCAMO.sql
\i create_ASSOCARO.sql
\i create_ASSOCCOM.sql
\i create_ASSOCCOO.sql
\i create_ASSOCTYPECAT.sql
\i create_ASSOCEVAMPSET.sql
\i create_ASSOCEVENTS.sql
--
\i create_CODA.sql
--
\i create_MEC.sql
\i create_MECFREQ.sql
\i create_MECFREQDATA.sql
\i create_MECDATA.sql
\i create_MECCHANNEL.sql
\i create_MECOBJECT.sql
\i create_EVENTPREFMEC.sql
--
\i create_NETMAG.sql
\i create_EVENTPREFMAG.sql
--
\i create_ORIGIN_ERROR.sql
\i create_ORIGIN.sql
--
\i create_REMARK.sql
\i create_EVENTPREFOR.sql
--
