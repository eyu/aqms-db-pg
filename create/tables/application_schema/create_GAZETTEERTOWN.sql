--
-- TABLE: gazetteertown
--

CREATE TABLE gazetteertown(
    gazid     BIGINT    NOT NULL,
    pop       BIGINT,
    remark    VARCHAR(80),
    lddate    TIMESTAMP             DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT gaztown_pk_gazid PRIMARY KEY (gazid)
)
;

