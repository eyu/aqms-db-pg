CREATE TABLE appchannels(
    progid      INTEGER    NOT NULL,
    net         VARCHAR(8)     NOT NULL,
    sta         VARCHAR(6)     NOT NULL,
    seedchan    VARCHAR(3)     NOT NULL,
    location    VARCHAR(2)     NOT NULL,
    config      VARCHAR(64),
    ondate      TIMESTAMP,
    offdate     TIMESTAMP,
    lddate      TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT config_chankey01_1 PRIMARY KEY (progid, net, sta, seedchan, location, ondate),
    CONSTRAINT config_chankey02_1 FOREIGN KEY (progid)
	references applications(progid)
)
;
