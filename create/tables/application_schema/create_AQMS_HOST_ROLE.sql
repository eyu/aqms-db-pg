CREATE TABLE aqms_host_role(
    primary_system   VARCHAR(16)     NOT NULL,
    host             VARCHAR(32)     NOT NULL,
    modification_time      TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC')
)
;
