--
-- Create the tables used to associate a group of named region polygons with a network authority
--
-- TABLE gazetteer_region_group
--       grpid: is a tag to id a grouping of gazetteer_region polygons
-- description: is a verbose description of application group
create table gazetteer_region_group (
   grpid VARCHAR(4)        not null,
   description VARCHAR(80) not null,
   constraint pk_gazreggrp  primary key (grpid)
)
;
-- Create public synonyms to make tables accessible to users/roles
-- create or replace public synonym gazetteer_region_group for gazetteer_region_group;

-- Substitute in grant statements the appropriate role/user privileges
-- grant select on gazetteer_region_group to trinetdb_read;
-- grant select,insert,update,delete on gazetteer_region_group to trinetdb_write;
-- grant select,insert,update,delete on gazetteer_region_group to code;

-- Populate gazetteer_region_group table with basic groups
insert into gazetteer_region_group values ('AUTH','Authority network group');
--
-- NOTE: Before inserting rows into assoc_region_group the gazetteer_region table must be populated
--       with the named polygons used for the authoritative regions where the polygons are closed,
--       i.e. the ending lat,lon vertices of polygon are the same. Example:
-- /*
--    insert into gazetteer_region values (
--      REGION(
--        'my_region_name',
--         _latlon(
--           LATLON(35.75, -118.25),
--           LATLON(36.25, -118.25),
--           LATLON(36.25, -117.75),
--           LATLON(35.75, -117.75),
--           LATLON(35.75, -118.25),
--         )
--      )
--   );
-- */
