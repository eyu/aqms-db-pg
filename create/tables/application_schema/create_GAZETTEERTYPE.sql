--
-- TABLE: gazetteertype
--

CREATE TABLE gazetteertype(
    code      SMALLINT    NOT NULL,
    name      VARCHAR(32)    NOT NULL,
    lddate    TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT gaztype_pk_code PRIMARY KEY (code)
)
;

