CREATE TABLE gazetteerquake(
    gazid     BIGINT    NOT NULL,
    remark    VARCHAR(80)     NOT NULL,
    lddate    TIMESTAMP             DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT gazeq_pk_gazid PRIMARY KEY (gazid)
)
;

