--
-- TABLE: stamapping
--

CREATE TABLE stamapping(
    net          VARCHAR(8)     NOT NULL,
    sta          VARCHAR(6)     NOT NULL,
    stanumber    INTEGER,
    LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    locdescr     VARCHAR(40),
    CONSTRAINT stamapkey01 PRIMARY KEY (net, sta)
)
;

