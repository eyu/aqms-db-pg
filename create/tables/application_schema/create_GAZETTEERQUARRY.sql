--
--
-- QB_TIME type in Oracle DB:
-- days varchar2(7), -- 7-day String mask (Su-Sa=> char position 1-7). Char value= "1" blasting active, "0" inactive
-- stime varchar2(8), -- starting UTC time String of the blasting window of the form HH24:MI:SS
-- etime varchar2(8) -- ending UTC time String the blasting window of the form HH24:MI:SS
--
--
 CREATE TABLE GAZETTEERQUARRY 
 (	gazid BIGINT NOT NULL, 
	ondate TIMESTAMP NOT NULL, 
	offdate TIMESTAMP DEFAULT ('3000-01-01 00:00:00'::Timestamp), 
	RADIUS DOUBLE PRECISION, 
	MDEPTH DOUBLE PRECISION, 
	MMAG DOUBLE PRECISION, 
	DAYS VARCHAR(7)[],
        STIME VARCHAR(8)[],
        ETIME VARCHAR(8)[],
	TFLG INTEGER, 
	REMARK VARCHAR(80), 
	LDDATE TIMESTAMP DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
	 CONSTRAINT GAZQRY_PK_GAZID PRIMARY KEY (GAZID, ONDATE) 
 ); 
