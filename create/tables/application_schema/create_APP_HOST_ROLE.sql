CREATE TABLE app_host_role(
    role_mode   VARCHAR(16)     NOT NULL,
    appname     VARCHAR(16)     NOT NULL,
    host        VARCHAR(32)     NOT NULL,
    state       VARCHAR(32)     NOT NULL,
    ondate      TIMESTAMP,
    offdate     TIMESTAMP,
    lddate      TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT config_apphostkey01_1 PRIMARY KEY (role_mode, appname, ondate)
)
;
