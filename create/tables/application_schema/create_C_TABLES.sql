CREATE TABLE c_channeldatatst(
        net VARCHAR (8),
        sta VARCHAR (6),
        seedchan VARCHAR (3),
        location VARCHAR (2),
        ondate TIMESTAMP,
        offdate TIMESTAMP,
        PRIMARY KEY(net, sta, seedchan, location, ondate, offdate),
        netid NUMERIC (5),
        lgrname VARCHAR (48),
        lgrid NUMERIC (5),
        lgrbits NUMERIC (2),
        lgrfsv NUMERIC (5,2),
        snsname VARCHAR (48),
        snsid NUMERIC (5),
        maxg NUMERIC (6,4),
        vperg NUMERIC (6,4),
        metaest VARCHAR (8),
        siteid NUMERIC (5),
        orntid NUMERIC (5),
        timeid NUMERIC (5),
        datumid NUMERIC (5),
        stavs30 NUMERIC (5,2),
        stageoid NUMERIC (15),
        chan_number NUMERIC (3),
        description VARCHAR (60),
        lddate TIMESTAMP,
        ref_azi NUMERIC (126),
        recsn VARCHAR (32),
        stachan NUMERIC (3)
);

CREATE TABLE c_loggerdata(
        lgrname VARCHAR (48) PRIMARY KEY,
        lgrid NUMERIC (5) NOT NULL,
        lgrbits NUMERIC (2),
        lgrfsv NUMERIC (5,2)
);

CREATE TABLE c_net(
        netid NUMERIC (5) PRIMARY KEY,
        netabbr VARCHAR (8) NOT NULL,
        net VARCHAR (2),
        netagent VARCHAR (128)
);

CREATE TABLE c_phystyp(
        physid NUMERIC (5) PRIMARY KEY,
        physdesc VARCHAR (32) NOT NULL
);

CREATE TABLE c_units(
        unitsid NUMERIC (5) PRIMARY KEY,
        unitsdesc VARCHAR (24) NOT NULL
);

CREATE TABLE c_channeldata(
        net VARCHAR (8) NOT NULL,
        sta VARCHAR (6) NOT NULL,
        seedchan VARCHAR (3) NOT NULL,
        location VARCHAR (2) NOT NULL,
        ondate TIMESTAMP NOT NULL,
        offdate TIMESTAMP NOT NULL,
        PRIMARY KEY(net, sta, seedchan, location, ondate, offdate),
        netid NUMERIC (5),
        lgrname VARCHAR (48),
        lgrid NUMERIC (5) NOT NULL,
        lgrbits NUMERIC (2),
        lgrfsv NUMERIC (5,2),
        snsname VARCHAR (48),
        snsid NUMERIC (5) NOT NULL,
        maxg NUMERIC (6,4),
        vperg NUMERIC (11,4),
        siteid NUMERIC (5),
        orntid NUMERIC (5),
        timeid NUMERIC (5),
        datumid NUMERIC (5),
        stavs30 NUMERIC (5,2),
        stageoid NUMERIC (15),
        chan_number NUMERIC (3),
        description VARCHAR (60),
        lddate TIMESTAMP,
        ref_azi NUMERIC (126),
        recsn VARCHAR (32)
);

CREATE TABLE c_channeldatarsn(
        net VARCHAR (8),
        sta VARCHAR (6),
        seedchan VARCHAR (3),
        location VARCHAR (2),
        ondate TIMESTAMP,
        offdate TIMESTAMP,
        PRIMARY KEY(net, sta, seedchan, location, ondate, offdate),
        netid NUMERIC (5),
        lgrname VARCHAR (48),
        lgrid NUMERIC (5),
        lgrbits NUMERIC (2),
        lgrfsv NUMERIC (5,2),
        snsname VARCHAR (48),
        snsid NUMERIC (5),
        maxg NUMERIC (6,4),
        vperg NUMERIC (6,4),
        metaest VARCHAR (8),
        siteid NUMERIC (5),
        orntid NUMERIC (5),
        timeid NUMERIC (5),
        datumid NUMERIC (5),
        stavs30 NUMERIC (5,2),
        stageoid NUMERIC (15),
        chan_number NUMERIC (3),
        description VARCHAR (60),
        lddate TIMESTAMP,
        ref_azi NUMERIC (126),
        recsn VARCHAR (32)
);

CREATE TABLE c_sensordata(
        snsname VARCHAR (48) PRIMARY KEY,
        snsid NUMERIC (5) NOT NULL,
        maxg NUMERIC (6,4),
        vperg NUMERIC (6,4)
);
