--
-- TABLE: gazetteerbigtown
--

CREATE TABLE gazetteerbigtown(
    gazid     BIGINT    NOT NULL,
    pop       BIGINT,
    remark    VARCHAR(80),
    lddate    TIMESTAMP             DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT gazbigtown_pk_gazid PRIMARY KEY (gazid)
)
;

