CREATE TABLE channelmap_ampparms(
    net           VARCHAR(8)      NOT NULL,
    sta           VARCHAR(6)      NOT NULL,
    seedchan      VARCHAR(3)      NOT NULL,
    location      VARCHAR(2)      NOT NULL,
    ondate        TIMESTAMP             NOT NULL,
    offdate       TIMESTAMP             NOT NULL,
    channel       VARCHAR(3),
    channelsrc    VARCHAR(8),
    clip          DOUBLE PRECISION,
    lddate        TIMESTAMP             DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT cm_amp_key PRIMARY KEY (net, sta, seedchan, location, ondate)
)
;
