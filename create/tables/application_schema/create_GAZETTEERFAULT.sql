--
-- TABLE: gazetteerfault
--

CREATE TABLE gazetteerfault(
    gazid     DOUBLE PRECISION          NOT NULL,
    remark    VARCHAR(80),
    lddate    TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    CONSTRAINT gazfault_pk_gazid PRIMARY KEY (gazid)
)
;

