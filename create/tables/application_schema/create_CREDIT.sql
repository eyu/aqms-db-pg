--
-- TABLE: credit
--

CREATE TABLE credit(
    id       BIGINT    NOT NULL,
    tname    VARCHAR(30)     NOT NULL,
    refer    VARCHAR(80)     NOT NULL,
    CONSTRAINT pk_credit PRIMARY KEY (id, tname)
)
;

