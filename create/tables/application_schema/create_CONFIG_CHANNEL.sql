--
-- TABLE: config_channel
--

CREATE TABLE config_channel(
    progid      INTEGER    NOT NULL,
    net         VARCHAR(8)     NOT NULL,
    sta         VARCHAR(6)     NOT NULL,
    seedchan    VARCHAR(3)     NOT NULL,
    config      VARCHAR(64),
    lddate      TIMESTAMP            DEFAULT (CURRENT_TIMESTAMP AT TIME ZONE 'UTC'),
    location    VARCHAR(2)     NOT NULL,
    CONSTRAINT config_chankey01 PRIMARY KEY (progid, net, sta, seedchan, location)
)
;

