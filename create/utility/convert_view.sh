#!/bin/bash

# read variable settings pointing to location of original oracle view creation scripts
# and destination for new PostgreSQL creating scripts.

if [ "x${BINDIR}x" == "xx" ] ; then
    echo "Set environment variable BINDIR to directory that convert_all_views is running from"
    exit
fi
source ${BINDIR}/paths.env
#ORA_SRC --> source
#PG_DEST  --> destination

#### example how to use convert_table.sh:
# convert_table.sh create_ARRIVAL.sql
# creates a file pg_create_ARRIVAL.sql with PostgreSQL syntax in directory PG_DEST
#
# references:
#
# http://docs.oracle.com/cd/B19306_01/server.102/b14200/sql_elements001.htm
# http://www.postgresql.org/docs/9.4/static/datatype.html
#
# Explanation of substitutions done below:
#
# --------------------------------------------------------------
# ORACLE view identifier     => PostgreSQL view identifier
# --------------------------------------------------------------
# FORCE                          => remove, unknown by PostgreSQL
# create or replace public synonym => remove
# grant or GRANT                 => remove
# --------------------------------------------------------------

cd $ORA_SRC
file_name=$1
echo "Converting $file_name in $ORA_SRC and putting new file in $PG_DEST"

grep -i -v "PUBLIC SYNONYM" $file_name |grep -i -v "GRANT" | sed -e "s:FORCE::"gi > ${PG_DEST}/${file_name}

