#!/bin/bash

BINDIR=$PWD
ORA_BASE=${BINDIR}/../../../views
PG_BASE=${BINDIR}/../../views

#***************************************************************************
#                          VIEWS                                           *
#***************************************************************************
ORA_SRC=${ORA_BASE}
PG_DEST=$PG_BASE}

# create env-file for use by convert_table
echo "# This file created by convert_all_views.sh" > ${BINDIR}/paths.env
echo "ORA_SRC=${ORA_BASE}" >> ${BINDIR}/paths.env
echo "PG_DEST=${PG_BASE}" >> ${BINDIR}/paths.env

# start converting
cd ${ORA_SRC}
${BINDIR}/convert_view.sh create_JASI_AMPPARMS_VIEW.sql
${BINDIR}/convert_view.sh create_JASI_CHANNEL_VIEW.sql
${BINDIR}/convert_view.sh create_JASI_CONFIG_VIEW.sql
${BINDIR}/convert_view.sh create_JASI_RESPONSE_VIEW.sql
${BINDIR}/convert_view.sh create_JASI_STATION_VIEW.sql
