#!/bin/bash

BINDIR=$PWD
ORA_BASE=${BINDIR}/../../../tables
PG_BASE=${BINDIR}/../tables

#***************************************************************************
#                          waveform_schema                                 *
#***************************************************************************
ORA_SRC=${ORA_BASE}/waveform_schema
PG_DEST=$PG_BASE}/waveform_schema

# create env-file for use by convert_table
echo "# This file created by convert_all_waveforms.sh" > ${BINDIR}/paths.env
echo "BINDIR=${BINDIR}" >> ${BINDIR}/paths.env
echo "ORA_SRC=${ORA_BASE}/waveform_schema" >> ${BINDIR}/paths.env
echo "PG_DEST=${PG_BASE}/waveform_schema" >> ${BINDIR}/paths.env

# start converting
cd ${ORA_SRC}
${BINDIR}/convert_table.sh create_ASSOCWAE.sql
${BINDIR}/convert_table.sh create_FILENAME.sql
${BINDIR}/convert_table.sh create_PATHNAME.sql
${BINDIR}/convert_table.sh create_REQUEST_CARD.sql
${BINDIR}/convert_table.sh create_SUBDIR.sql
${BINDIR}/convert_table.sh create_WAVEFORM.sql

#***************************************************************************
#                          parametric_schema                               *
#***************************************************************************
ORA_SRC=${ORA_BASE}/parametric_schema
PG_DEST=$PG_BASE}/parametric_schema

# create env-file for use by convert_table
echo "# This file created by convert_all_waveforms.sh" > ${BINDIR}/paths.env
echo "ORA_SRC=${ORA_BASE}/parametric_schema" >> ${BINDIR}/paths.env
echo "PG_DEST=${PG_BASE}/parametric_schema" >> ${BINDIR}/paths.env

# start converting
cd ${ORA_SRC}
${BINDIR}/convert_table.sh create_AMPSET.sql
${BINDIR}/convert_table.sh create_AMPSETTYPES.sql
${BINDIR}/convert_table.sh create_AMP.sql
${BINDIR}/convert_table.sh create_ARRIVAL.sql
${BINDIR}/convert_table.sh create_ASSOCAMM.sql
${BINDIR}/convert_table.sh create_ASSOCAMO.sql
${BINDIR}/convert_table.sh create_ASSOCARO.sql
${BINDIR}/convert_table.sh create_ASSOCCOM.sql
${BINDIR}/convert_table.sh create_ASSOCCOO.sql
${BINDIR}/convert_table.sh create_ASSOCEVAMPSET.sql
${BINDIR}/convert_table.sh create_ASSOCTYPECAT.sql
${BINDIR}/convert_table.sh create_CODA.sql
${BINDIR}/convert_table.sh create_EVENTCATEGORY.sql
${BINDIR}/convert_table.sh create_EVENTPREFMAG.sql
${BINDIR}/convert_table.sh create_EVENTPREFMEC.sql
${BINDIR}/convert_table.sh create_EVENTPREFOR.sql
${BINDIR}/convert_table.sh create_EVENT.sql
${BINDIR}/convert_table.sh create_EVENTTYPE.sql
${BINDIR}/convert_table.sh create_MECCHANNEL.sql
${BINDIR}/convert_table.sh create_MECDATA.sql
${BINDIR}/convert_table.sh create_MECFREQDATA.sql
${BINDIR}/convert_table.sh create_MECFREQ.sql
${BINDIR}/convert_table.sh create_MECOBJECT.sql
${BINDIR}/convert_table.sh create_MEC.sql
${BINDIR}/convert_table.sh create_NETMAG.sql
${BINDIR}/convert_table.sh create_ORIGIN_ERROR.sql
${BINDIR}/convert_table.sh create_ORIGIN.sql
${BINDIR}/convert_table.sh create_REMARK.sql

#***************************************************************************
#                          instrument_response_schema                      *
#***************************************************************************
ORA_SRC=${ORA_BASE}/instrument_response_schema
PG_DEST=$PG_BASE}/instrument_response_schema

# create env-file for use by convert_table
echo "# This file created by convert_all_waveforms.sh" > ${BINDIR}/paths.env
echo "ORA_SRC=${ORA_BASE}/instrument_response_schema" >> ${BINDIR}/paths.env
echo "PG_DEST=${PG_BASE}/instrument_response_schema" >> ${BINDIR}/paths.env

# start converting
cd ${ORA_SRC}
${BINDIR}/convert_table.sh create_CHANNEL_COMMENT.sql
${BINDIR}/convert_table.sh create_CHANNEL_DATA.sql
${BINDIR}/convert_table.sh create_COEFFICIENTS.sql
${BINDIR}/convert_table.sh create_D_ABBREVIATION.sql
${BINDIR}/convert_table.sh create_DC_DATA.sql
${BINDIR}/convert_table.sh create_D_COMMENT.sql
${BINDIR}/convert_table.sh create_DC.sql
${BINDIR}/convert_table.sh create_DECIMATION.sql
${BINDIR}/convert_table.sh create_D_FORMAT_DATA.sql
${BINDIR}/convert_table.sh create_D_FORMAT.sql
${BINDIR}/convert_table.sh create_DM.sql
${BINDIR}/convert_table.sh create_D_UNIT.sql
${BINDIR}/convert_table.sh create_PN_DATA.sql
${BINDIR}/convert_table.sh create_PN.sql
${BINDIR}/convert_table.sh create_POLES_ZEROS.sql
${BINDIR}/convert_table.sh create_POLYNOMIAL.sql
${BINDIR}/convert_table.sh create_pub_syn_ir.sql
${BINDIR}/convert_table.sh create_PZ_DATA.sql
${BINDIR}/convert_table.sh create_PZ.sql
${BINDIR}/convert_table.sh create_SENSITIVITY.sql
${BINDIR}/convert_table.sh create_SIMPLE_RESPONSE.sql
${BINDIR}/convert_table.sh create_STATION_COMMENT.sql
${BINDIR}/convert_table.sh create_STATION_DATA.sql

#***************************************************************************
#                          hardware_schema                      *
#***************************************************************************
ORA_SRC=${ORA_BASE}/hardware_schema
PG_DEST=$PG_BASE}/hardware_schema

# create env-file for use by convert_table
echo "# This file created by convert_all_waveforms.sh" > ${BINDIR}/paths.env
echo "ORA_SRC=${ORA_BASE}/hardware_schema" >> ${BINDIR}/paths.env
echo "PG_DEST=${PG_BASE}/hardware_schema" >> ${BINDIR}/paths.env

# start converting
cd ${ORA_SRC}
${BINDIR}/convert_table.sh create_DATALOGGER_BOARD.sql
${BINDIR}/convert_table.sh create_DATALOGGER_MODULE.sql
${BINDIR}/convert_table.sh create_DATALOGGER.sql
${BINDIR}/convert_table.sh create_FILAMP.sql
${BINDIR}/convert_table.sh create_FILEAMP_PCHANNEL.sql
${BINDIR}/convert_table.sh create_FILTER_FIR_DATA.sql
${BINDIR}/convert_table.sh create_FILTER_FIR.sql
${BINDIR}/convert_table.sh create_FILTER_SEQUENCE_DATA.sql
${BINDIR}/convert_table.sh create_FILTER_SEQUENCE.sql
${BINDIR}/convert_table.sh create_FILTER.sql
${BINDIR}/convert_table.sh create_RESPONSE_HP.sql
${BINDIR}/convert_table.sh create_RESPONSE_LP.sql
${BINDIR}/convert_table.sh create_RESPONSE_PN_DATA.sql
${BINDIR}/convert_table.sh create_RESPONSE_PN.sql
${BINDIR}/convert_table.sh create_RESPONSE_PZ.sql
${BINDIR}/convert_table.sh create_RESPONSE.sql
${BINDIR}/convert_table.sh create_SENSOR_COMPONENT.sql
${BINDIR}/convert_table.sh create_SENSOR.sql
${BINDIR}/convert_table.sh create_STATION_DATALOGGER_LCHANNEL.sql
${BINDIR}/convert_table.sh create_STATION_DATALOGGER_PCHANNEL.sql
${BINDIR}/convert_table.sh create_STATION_DATALOGGER.sql
${BINDIR}/convert_table.sh create_STATION_DIGITIZER_PCHANNEL.sql
${BINDIR}/convert_table.sh create_STATION_DIGITIZER.sql
${BINDIR}/convert_table.sh create_STATION_FILAMP_PCHANNEL.sql
${BINDIR}/convert_table.sh create_STATION_FILAMP.sql
${BINDIR}/convert_table.sh create_STATION_SENSOR_COMPONENT.sql
${BINDIR}/convert_table.sh create_STATION_SENSOR.sql
${BINDIR}/convert_table.sh create_STATION.sql

#***************************************************************************
#                          application_schema                      *
#***************************************************************************
ORA_SRC=${ORA_BASE}/application_schema
PG_DEST=$PG_BASE}/application_schema

# create env-file for use by convert_table
echo "# This file created by convert_all_waveforms.sh" > ${BINDIR}/paths.env
echo "ORA_SRC=${ORA_BASE}/application_schema" >> ${BINDIR}/paths.env
echo "PG_DEST=${PG_BASE}/application_schema" >> ${BINDIR}/paths.env

# start converting
# not converted (changed a lot for PostgreSQL/PostGIS):
# gazetteerpt, gazetteerline, gazetteer_region, gazetteerquarry
# and (oracle version is not in ORA_SRC but in types):
# epochtimebase
cd ${ORA_SRC}
${BINDIR}/convert_table.sh create_ALARM_ACTION.sql
${BINDIR}/convert_table.sh create_APPCHANNELS.sql
${BINDIR}/convert_table.sh create_APPLICATIONS.sql
${BINDIR}/convert_table.sh create_ASSOCNTE.sql
${BINDIR}/convert_table.sh create_ASSOC_REGION_GROUP.sql
${BINDIR}/convert_table.sh create_AUTOPOSTER.sql
${BINDIR}/convert_table.sh create_CHANNELMAP_AMPPARMS.sql
${BINDIR}/convert_table.sh create_CHANNELMAP_CODAPARMS.sql
${BINDIR}/convert_table.sh create_CONFIG_CHANNEL.sql
${BINDIR}/convert_table.sh create_CONFIG_PARAM.sql
${BINDIR}/convert_table.sh create_CREDIT_ALIAS.sql
${BINDIR}/convert_table.sh create_CREDIT.sql
${BINDIR}/convert_table.sh create_EVENTMATCHPASSINGSCORE.sql
${BINDIR}/convert_table.sh create_EVENTPRIORITYPARAMS.sql
${BINDIR}/convert_table.sh create_GAZETTEERBIGTOWN.sql
${BINDIR}/convert_table.sh create_GAZETTEERFAULT.sql
${BINDIR}/convert_table.sh create_GAZETTEERQUAKE.sql
${BINDIR}/convert_table.sh create_GAZETTEER_REGION_GROUP.sql
${BINDIR}/convert_table.sh create_GAZETTEERTOWN.sql
${BINDIR}/convert_table.sh create_GAZETTEERTYPE.sql
${BINDIR}/convert_table.sh create_JASIEVENTLOCK.sql
${BINDIR}/convert_table.sh create_LEAP_SECONDS.sql
${BINDIR}/convert_table.sh create_MAGPREFPRIORITY.sql
${BINDIR}/convert_table.sh create_MCAPARMS.sql
${BINDIR}/convert_table.sh create_MECPREFPRIORITY.sql
${BINDIR}/convert_table.sh create_MESSAGETEXT.sql
${BINDIR}/convert_table.sh create_NETTRIG.sql
${BINDIR}/convert_table.sh create_ORGPREFPRIORITY.sql
${BINDIR}/convert_table.sh create_PCS_SIGNAL.sql
${BINDIR}/convert_table.sh create_PCS_STATE.sql
${BINDIR}/convert_table.sh create_PCS_TRANSITION.sql
${BINDIR}/convert_table.sh create_PEER_SYSTEM_STATUS.sql
${BINDIR}/convert_table.sh create_PROGRAM.sql
${BINDIR}/convert_table.sh create_RT_ROLE.sql
${BINDIR}/convert_table.sh create_STACORRECTIONS.sql
${BINDIR}/convert_table.sh create_STAMAPPING.sql
${BINDIR}/convert_table.sh create_SUBNET_CHANNEL.sql
${BINDIR}/convert_table.sh create_SUBNET.sql
${BINDIR}/convert_table.sh create_SWARM_EVENTS.sql
${BINDIR}/convert_table.sh create_SWARM_STATE.sql
${BINDIR}/convert_table.sh create_SYSTEM_STATUS.sql
${BINDIR}/convert_table.sh create_TRIG_CHANNEL.sql
${BINDIR}/convert_table.sh create_UNASSOCAMP.sql
${BINDIR}/convert_table.sh create_WAVEFILEROOTS.sql
${BINDIR}/convert_table.sh create_WAVEROOTS.sql
