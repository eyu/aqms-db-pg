#!/bin/bash

# Set variables
source pg.env

echo "You need sudo privileges on this machine for this script to run correctly."
echo "It creates a password-file, changes its ownership to postgres:postgres and"
echo "puts it in ~postgres. For this you need sudo. If you don't, ask a sys admin"
echo "to place the .pgpass created in your own home-directory in the postgres home dir."

if [ "$DEFAULT_TS" != "pg_default" ]; then
  echo "You are not using the standard default tablespace name."
  echo "Have you created your customized tablespaces?"
  select yn in "Yes" "No" ; do
  case $yn in
      Yes ) echo "OK, continuing"; break;;
      No ) echo "Create tablespace directories etc. first, see tablespaces/README"; exit;;
  esac
  done
fi

echo "Generating sql-scripts to enable creation of AQMS database $DBNAME with default tablespace $DEFAULT_TS"

# Create .pgpass file
echo "#hostname:port:database:username:password" > $PGPASSFILE
echo "${PGHOST}:${PGPORT}:${DBNAME}:browser:$BROWSER_PASS" >> $PGPASSFILE
echo "${PGHOST}:${PGPORT}:${DBNAME}:rtem:$RTEM_PASS" >> $PGPASSFILE
echo "${PGHOST}:${PGPORT}:${DBNAME}:tpp:$TPP_PASS" >> $PGPASSFILE
echo "${PGHOST}:${PGPORT}:${DBNAME}:dcarchiver:$DCARCHIVER_PASS" >> $PGPASSFILE
echo "${PGHOST}:${PGPORT}:${DBNAME}:trinetdb:$TRINETDB_PASS" >> $PGPASSFILE
echo "${PGHOST}:${PGPORT}:${DBNAME}:operator:$OPERATOR_PASS" >> $PGPASSFILE
echo "${PGHOST}:${PGPORT}:${DBNAME}:code:$CODE_PASS" >> $PGPASSFILE
echo "${PGHOST}:${PGPORT}:${DBNAME}:repadmin:$REPADMIN_PASS" >> $PGPASSFILE
chmod 0600 $PGPASSFILE
sudo cp $PGPASSFILE ~postgres
echo "created $PGPASSFILE and put a copy in ~postgres"

# Create sql-script that creates database DBNAME with default tablespace DEFAULT_TS
sed -e "s:DBNAME:$DBNAME:" \
    -e "s:DEFAULT_TS:$DEFAULT_TS:" \
		db/createdb.template > db/create_${DBNAME}.sql

echo "created db/create_${DBNAME}.sql"

# Create sql-script that creates roles with logins ("users")
sed -e "s:BROWSER_PASS:$BROWSER_PASS:" \
	-e "s:RTEM_PASS:$RTEM_PASS:" \
	-e "s:TPP_PASS:$TPP_PASS:" \
	-e "s:DCARCHIVER_PASS:$DCARCHIVER_PASS:" \
	-e "s:TRINETDB_PASS:$TRINETDB_PASS:" \
	-e "s:OPERATOR_PASS:$OPERATOR_PASS:" \
	-e "s:CODE_PASS:$CODE_PASS:" \
	-e "s:REPADMIN_PASS:$REPADMIN_PASS:"\
		users/create_users.template > users/create_users.sql

echo "created users/create_users.sql"

sudo chown postgres:postgres ~postgres/.pgpass

# create sql-script that creates sequences for database DBNAME
sed -e "s:EVID_START_VALUE:$evid_start_value:"  \
	-e "s:START_VALUE:$seq_start_value:" sequences/create_sequences.template > sequences/create_${DBNAME}_sequences.sql

echo "created sequences/create_${DBNAME}_sequences.sql"
