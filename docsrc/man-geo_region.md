# geo_region {#man-db-geo_region}
## Overview
 version: 1.0.1 2020-07-10
 
 used in production: yes, utc version
 
 function/procedures for testing whether geographic point lies
 inside or outside a polygonal region specified by lat,lon coordinates.
 
 dependent applications:
 
 - java org.trinet.jasi.tn.solutiontn using eventselectionproperties
 
 depends on:
 
 - refs gazetteer_region, gazetteerpt tables objects
 - oracle user defined datatype latlon, coordinates in public schema (synonym ok)
 
 example:
 ```
 select evid, orid, lat,lon from event,origin
 where orid=prefor and datetime > truetime.putepoch(truetime.string2true('2004/01/30 00:00:00'),'utc')
 and geo_region.inside_border('airportlake', lat, lon);
 ```
 
## Functions
 ### geo_region.getPkgId() RETURNS varchar(80) 
 get version number
 ### geo_region.inside_border(v_name gazetteer_region.name%TYPE, v_lat double precision, v_lon double precision) RETURNS integer 
 A pl/pgsql version of geo_region.inside_border(name,lat,lon)
 Returns: 1 if lat,lon are inside of gazetteer_region with name=name, 0 if not,
 and -1 if region not found
 Requires: PostGIS extension, gazetteer_region table.
 Dependencies: eventcoordinator (ec)
 ### geo_region.authOfRegionInGroup(p_lat double precision, p_lon double precision, p_grpid varchar)    RETURNS varchar 
 Returns name of the gazetteer_region associated with the specified p_grpid in the
 assoc_region_group table whose polygon the input location is inside of, and in the
 case of multiple overlapping region boundaries within the same group, the one with
 the the lowest rg_order value.
 Returns NULL if input lat,lon point is outside of all regions associated with
 input group id.
 ### geo_region.inside_border(p_latlon double precision[], p_lat double precision, p_lon double precision) RETURNS integer 
 Returns 0 if outside, 1 if inside, 2 if on the defined region border.
 
 Double array should be in (lat,long) order
 
 Uses "linestring" when input contains 3 lat/longs (i.e. looks for border)
 
 Uses "polygon" when 4 or more lat/longs. Polygon must be closed.
 
 Using exceptions instead of return code "-1" on error as Jiggle checks for "<> 0" which results in "true". Exception is displayed by Jiggle.
 
 Sample usage: select * from geo_region.inside_border(array[[37.3642, -121.90188], [ 37.2583, -121.7986], [37.3320, -122.0553], [37.3642, -121.90188]],37.3331, -121.8974)
