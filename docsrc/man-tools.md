# tools {#man-db-tools}
## Overview
 Current version: 1.0.0 2021-08-20*
 
 Functions/Procedures in this package are needed by dbselect.
 
 Used in production: yes, UTC version
 
 ### DEPENDENT APPLICATIONS
 - dbselect
 
## Functions
 ### tools.latlon(v_lat DOUBLE PRECISION, v_lon DOUBLE PRECISION, v_km DOUBLE PRECISION, v_az DOUBLE PRECISION, v_flag INTEGER) RETURNS DOUBLE PRECISION 
 The function latlon computes the latitude and longitude in degrees
 of a point that is specified by a great circle distance and azimuth
 from north from a given latitude and longitude.
 
 Latitudes and longitudes are given in decimal degrees.
 The epicentral distance is given in kilometers.
 The azimuth is given in decimal degrees.
 flag = 0 ==> Returns latitude ; flag <> 0 ==> Returns longitude.
 ### tools.inpoly(v_spoly VARCHAR(5120), v_lat DOUBLE PRECISION, v_lon DOUBLE PRECISION) RETURNS INTEGER 
 The function inpoly determines if a point (v_lat, v_lon) is within an
 arbitrary polygon defined by string p. POLYGON((lon lat, lon lat, ..))
 The function inpoly returns 1 if p is inside  the number of crossings.
