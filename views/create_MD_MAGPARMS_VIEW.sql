CREATE OR REPLACE VIEW MD_MAGPARMS_VIEW (NET, STA, SEEDCHAN, LOCATION, CHANNEL, CHANNELSRC, 
ONDATE, OFFDATE, CORR, CORR_FLAG, CORR_TYPE, AUTH, CLIP, CUTOFF, GAIN_CORR, SUMMARY_WT) 
AS                             
  WITH parms AS ( 
      SELECT cp.*,ap.clip FROM channelmap_codaparms cp INNER JOIN channelmap_ampparms ap 
      ON cp.net=ap.net and cp.sta=ap.sta and cp.seedchan=ap.seedchan and 
         cp.location=ap.location and cp.ondate=ap.ondate
  ) 
  SELECT parms.net, parms.sta, parms.seedchan, parms.location, parms.channel, parms.channelsrc,
         parms.ondate, parms.offdate, sc.corr, sc.corr_flag, sc.corr_type, sc.auth,
         parms.clip, parms.cutoff, parms.gain_corr, parms.summary_wt 
  FROM parms
  LEFT OUTER JOIN stacorrections sc
  ON sc.net=parms.net and sc.sta=parms.sta and sc.seedchan=parms.seedchan and sc.location=parms.location and sc.corr_type='md'
  ORDER BY parms.net, parms.sta, parms.seedchan, parms.location;


-- CREATE OR REPLACE VIEW MD_MAGPARMS_VIEW (NET, STA, SEEDCHAN, LOCATION, CHANNEL, 
-- CHANNELSRC, 
-- ONDATE, OFFDATE, CORR, CORR_FLAG, CORR_TYPE, AUTH, CLIP, CUTOFF, GAIN_CORR, 
-- SUMMARY_WT) AS                             
-- SELECT NET,STA,SEEDCHAN,LOCATION,CHANNEL,CHANNELSRC,ONDATE,OFFDATE,CORR,CORR_FLAG,
-- CORR_TYPE,AUTH,CLIP,
-- CUTOFF,GAIN_CORR,SUMMARY_WT FROM (
-- -- need to replace driving table channel_data with simple_response here, date range problem
-- SELECT cp.net, cp.sta, cp.seedchan, cp.location, cp.channel, cp.channelsrc, 
-- cp.ondate, cp.offdate,
-- sc.corr, sc.corr_flag, sc.corr_type, sc.auth,
-- ap.clip, cp.cutoff, cp.gain_corr, cp.summary_wt
-- FROM
-- channelmap_codaparms cp,
-- stacorrections sc,
-- channelmap_ampparms ap
-- WHERE
-- -- template for seedchan types for which a coda measurement is legit 
-- -- cp.seedchan IN ('EHZ', 'HHZ')
-- cp.net = sc.net(+)
-- AND cp.sta = sc.sta(+)
-- AND cp.seedchan = sc.seedchan(+)
-- AND cp.location = sc.location(+)
-- AND sc.offdate(+) > SYSDATE
-- AND sc.corr_flag(+) <> 'D'
-- AND sc.corr_type(+) = 'md'
-- --
-- AND cp.net = ap.net(+)
-- AND cp.sta = ap.sta(+)
-- AND cp.seedchan = ap.seedchan(+)
-- AND cp.location = ap.location(+)
-- -- clip amp dates must by within codaparm date range
-- AND cp.offdate <= ap.offdate(+)
-- AND cp.ondate >= ap.ondate(+)
-- );
-- 
