CREATE OR REPLACE  VIEW JASI_RESPONSE_VIEW (NET, STA, 
    SEEDCHAN, CHANNEL, CHANNELSRC, LOCATION, NATURAL_FREQUENCY, 
    DAMPING_CONSTANT, GAIN, GAIN_UNITS, LOW_FREQ_CORNER, HIGH_FREQ_CORNER, 
    ONDATE, OFFDATE) AS
      select net,sta,seedchan,channel,channelsrc,location,                          
        natural_frequency,damping_constant,                                         
        gain,gain_units,low_freq_corner,high_freq_corner,ondate,offdate             
      from simple_response;                                                         
--
