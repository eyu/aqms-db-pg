grant create on schema trinetdb to trinetdb_write, code;
grant select on JASI_AMPPARMS_VIEW to trinetdb_read, code;
grant select on JASI_CHANNEL_VIEW to trinetdb_read, code;
grant select on JASI_CONFIG_VIEW to trinetdb_read, code;
grant select on JASI_RESPONSE_VIEW to trinetdb_read, code;
grant select on JASI_STATION_VIEW to trinetdb_read, code;
grant select on MD_MAGPARMS_VIEW to trinetdb_read, code;
grant select on ML_MAGPARMS_VIEW to trinetdb_read, code;
