CREATE OR REPLACE  VIEW JASI_CHANNEL_VIEW 
  (NET, STA, SEEDCHAN, CHANNEL, CHANNELSRC, LOCATION, LAT, LON, ELEV, 
  EDEPTH, azimuth, dip, SAMPRATE, ONDATE, OFFDATE) AS 
    select net,sta,seedchan,channel,channelsrc,location,                          
       lat,lon,elev, edepth,azimuth,dip, samprate,ondate,offdate from channel_data;                      
--
