set search_path to "$user", trinetdb, code, public;

create schema if not exists wave authorization code;
grant usage on schema wave to trinetdb_read, trinetdb_execute;
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--  WAVE.getWaveformBlob(p_filename...) is invoked in SQL query.
--  This is a postgres c-extension in the shared library AQMSpg_ext.so
--  Jiggle application and other apps invoke WaveformTN methods
--  to load waveforms from database archive via SeedReader methods.
--
--  org.trinet.jasi.DataSource utcCompliant method invokes isJavaUTC function
--                               just returns True
-- 
--  Jiggle uses to read waveforms into its GUI from local miniseed files.
--
--- <PACKAGE DEPENDS ON >
--  refs package WAVEFILE installation
--  AQMSpg_ext.so contains c-extensions (on server)
--  All other Java classes included by above classes
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
--
-- Return SVN Id for package specification.
DROP FUNCTION IF EXISTS wave.getPkgId();
CREATE OR REPLACE FUNCTION wave.getPkgId() RETURNS TEXT as $$
  DECLARE
    v_id TEXT = 'wave.sql 8129 2017-01-27 postgresql $';
  BEGIN
    RETURN v_id;
  END
$$ LANGUAGE plpgsql; 

-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- dummy function to satisfy java client
DROP FUNCTION IF EXISTS wave.isJavaUTC();
CREATE OR REPLACE FUNCTION wave.isJavaUTC() RETURNS INTEGER AS $$
  BEGIN
    RETURN 1;
  END
$$ LANGUAGE plpgsql;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return BLOB of binary data, waveform timeseries SEED mini-packets,
-- that exist within the bounds of the input timespan for the input waveform identifier.
-- The data file location is recovered by SQL query of waveform associated parametric tables.
-- Filename path is that from the WAVEROOTS table with specified wcopy.
DROP FUNCTION IF EXISTS  wave.get_waveform_blob(p_copy waveroots.wcopy%TYPE, p_wfid waveform.wfid%TYPE, p_startTime DOUBLE PRECISION, p_endTime DOUBLE PRECISION);
CREATE OR REPLACE FUNCTION wave.get_waveform_blob(p_copy waveroots.wcopy%TYPE, p_wfid waveform.wfid%TYPE,
                                  p_startTime DOUBLE PRECISION, p_endTime DOUBLE PRECISION) RETURNS BYTEA AS $$
  DECLARE 
    v_filename TEXT;
    v_offset waveform.traceoff%TYPE;
    v_length waveform.tracelen%TYPE;
  BEGIN
    SELECT traceoff, tracelen, wavefile.name(p_wfid, p_copy) INTO v_offset, v_length, v_filename
       FROM waveform WHERE wfid = p_wfid;
    --get waveform	
    RETURN wave.get_waveform_blob(v_filename, v_offset, v_length, p_startTime, p_endTime);	
  END;
$$ LANGUAGE plpgsql;
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Like above except WAVEROOTS table row wcopy column value matches the first input parameter = 1.

DROP FUNCTION IF EXISTS wave.get_waveform_blob(p_copy waveroots.wcopy%TYPE, p_wfid waveform.wfid%TYPE, p_startTime DOUBLE PRECISION, p_endTime DOUBLE PRECISION);
CREATE OR REPLACE FUNCTION wave.get_waveform_blob(p_copy waveroots.wcopy%TYPE, p_wfid waveform.wfid%TYPE,
                                  p_startTime DOUBLE PRECISION, p_endTime DOUBLE PRECISION) RETURNS BYTEA AS $$
  BEGIN
    RETURN wf = wave.get_waveform_blob(1, p_wfid, p_startTime, p_endTime);	
  END;
$$ LANGUAGE plpgsql;

--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return BLOB of binary data, wavefrom timeseries SEED mini-packets,
-- that exist within the bounds of input timespan, for the input event id (evid), seedchan, 
-- and as well as location, if input channel location parameter is not null.
-- The data file location is recovered by SQL query of waveform associated parametric tables.
-- Filename path is that from the WAVEROOTS table with specified wcopy column value is 1.
DROP FUNCTION IF EXISTS wave.get_waveform_blob(p_copy waveroots.wcopy%TYPE, p_evid event.evid%TYPE, p_net waveform.net%TYPE, p_sta waveform.sta%TYPE, p_chan waveform.seedchan%TYPE, p_loc waveform.location%TYPE, p_startTime DOUBLE PRECISION, p_endTime DOUBLE PRECISION);
CREATE OR REPLACE FUNCTION wave.get_waveform_blob(p_copy waveroots.wcopy%TYPE, p_evid event.evid%TYPE,
       			          p_net waveform.net%TYPE, p_sta waveform.sta%TYPE,
                                  p_chan waveform.seedchan%TYPE, p_loc waveform.location%TYPE, 
                                  p_startTime DOUBLE PRECISION, p_endTime DOUBLE PRECISION) RETURNS BYTEA AS $$
  DECLARE 
    v_filename TEXT;
    v_offset waveform.traceoff%TYPE;
    v_length waveform.tracelen%TYPE;
  BEGIN
    IF (p_loc IS NULL) THEN -- NO location spec
      -- no location tie-breaker argument is used to discrimate unique channelname id - aww
      SELECT w.traceoff, w.tracelen, wavefile.name(w.wfid, p_copy) INTO v_offset, v_length, v_filename
         FROM waveform w, assocwae awe WHERE w.net = p_net AND w.sta = p_sta AND w.seedchan = p_chan 
         AND w.wfid = awe.wfid AND awe.evid=p_evid;
    ELSE -- has location spec
      SELECT w.traceoff, w.tracelen, wavefile.name(w.wfid, p_copy) INTO v_offset, v_length, v_filename
         FROM waveform w, assocwae awe WHERE w.net = p_net AND w.sta = p_sta AND w.seedchan = p_chan AND w.location = p_loc
           AND w.wfid = awe.wfid AND awe.evid=p_evid;
    END IF;
    RETURN wave.get_waveform_blob(v_filename, v_offset, v_length, p_startTime, p_endTime);	
  END;
$$ LANGUAGE plpgsql;

-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Like above except WAVEROOTS table row wcopy column defaults to 1.
DROP FUNCTION IF EXISTS wave.get_waveform_blob(p_evid  event.evid%TYPE, p_net waveform.net%TYPE, p_sta waveform.sta%TYPE, p_chan waveform.seedchan%TYPE, p_loc waveform.location%TYPE, p_startTime DOUBLE PRECISION, p_endTime DOUBLE PRECISION);
CREATE OR REPLACE FUNCTION wave.get_waveform_blob(p_evid  event.evid%TYPE,
       			          p_net waveform.net%TYPE, p_sta waveform.sta%TYPE,
                                  p_chan waveform.seedchan%TYPE, p_loc waveform.location%TYPE, 
                                  p_startTime DOUBLE PRECISION, p_endTime DOUBLE PRECISION) RETURNS BYTEA AS $$
  BEGIN
    RETURN wave.get_waveform_blob(1,p_evid,p_net, P_sta,p_chan,p_loc,p_startTime,p_endTime);	
  END;
$$ LANGUAGE plpgsql;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return BLOB of binary data, wavefrom timeseries SEED mini-packets,
-- that exist within the bounds of the input timespan, from the named input file,
-- starting data retrieval at the specified byte offset, for the specified length.
-- The starting and ending bytes must be the beginning and the end of valid packets. 
DROP FUNCTION IF EXISTS wave.get_waveform_blob(p_filename TEXT, p_traceoff BIGINT, p_nbytes BIGINT, p_startTime DOUBLE PRECISION, p_endTime DOUBLE PRECISION);
CREATE OR REPLACE FUNCTION wave.get_waveform_blob(p_filename TEXT, p_traceoff BIGINT, p_nbytes BIGINT,
                             p_startTime DOUBLE PRECISION, p_endTime DOUBLE PRECISION) RETURNS BYTEA
			     AS '/usr/local/lib/AQMSpg_ext.so','getWaveformBlob'
			     LANGUAGE C STRICT;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
-- Return BLOB of binary data, waveform timeseries SEED mini-packets,
-- from the named input file starting at the specified byte offset, for 
-- the specified total length in bytes.
-- The starting and ending bytes must be at the beginning and end of valid packets. 
DROP FUNCTION IF EXISTS wave.get_waveform_blob(p_filename TEXT, p_traceoff BIGINT, p_nbytes BIGINT);
CREATE OR REPLACE FUNCTION wave.get_waveform_blob(p_filename TEXT, p_traceoff BIGINT, p_nbytes BIGINT) RETURNS BYTEA AS $$
  BEGIN
    RETURN wave.get_waveform_blob(filename, traceoff, nbytes, NULL, NULL);	
  END;
$$ LANGUAGE plpgsql;
	

