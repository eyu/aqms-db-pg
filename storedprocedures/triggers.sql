DROP TRIGGER post_new_event ON trinetdb.event;
/**
*    -- This function is run after a new row is inserted into the Event table.
*    -- It figures out what to do for an event of this type from the Autoposter table.
*    -- It then uses the PCS stored procedure putState (post_id) to insert an 
*    -- entry into the PCS_State table.
*    --
*    --                  Table "trinetdb.autoposter"
*    --    Column    |            Type             | Modifiers 
*    ----------------+-----------------------------+-----------
*    -- instancename | character varying(30)       | not null
*    -- controlgroup | character varying(20)       | not null
*    -- sourcetable  | character varying(20)       | not null
*    -- state        | character varying(20)       | not null
*    -- rank         | numeric                     | not null
*    -- result       | numeric                     | 
*    -- acton        | character varying(6)        | 
*    -- eventtype    | character varying(2)        | 
*    -- lastid       | numeric                     | 
*    -- lastlddate   | timestamp without time zone | 
*    -- lddate       | timestamp without time zone | 
*    -- resulttext   | character varying(80)       | 
*    --Indexes:
*    --    "autoposter_pkey" PRIMARY KEY, btree (instancename)
*    --Check constraints:
*    --    "actionlist" CHECK (acton::text = ANY (ARRAY['EVENT'::character varying, 'ORIGIN'::character varying, 'NETMAG'::character varying]::text[]))
*    --
*    -- SQL> select * from autoposter;
*    --
*    --INSTANCENAME       CONTROLGROUP    SOURCETABLE 	 STATE	    RANK     RESULT ACTON  EV	  LASTID LASTLDDATE	     LDDATE		 RESULTTEXT
*------------------------------------------------------------------------------------------------------------------------------------------------------------
*    --NewEventPoster      EventStream	    archdb NewEvent	     100	  1 EVENT  eq	61250626 2017/02/16 22:00:33 2017/02/16 22:41:56
*    --NewTriggerPoster    EventStream	    archdb NewTrigger	     100	  1 EVENT  st	61250636 2017/02/17 03:03:03 2017/02/17 03:03:43
*/

CREATE OR REPLACE FUNCTION post_new_event() RETURNS TRIGGER AS $post_new_event$

    DECLARE
    v_group trinetdb.pcs_state.controlgroup%TYPE;
    v_source trinetdb.pcs_state.sourcetable%TYPE;
    v_state trinetdb.pcs_state.state%TYPE;
    v_rank trinetdb.pcs_state.rank%TYPE;
    v_result trinetdb.pcs_state.result%TYPE := -1;
    v_status INTEGER := 0;
    
    BEGIN
        -- NEW is the new record that was inserted
        -- Check that event ID is not null
        IF NEW.evid IS NULL THEN
            RAISE EXCEPTION 'evid cannot be null';
        END IF;
        -- only post selectflag=1 evids
        IF NEW.selectflag = 0 THEN
            RETURN NULL;
        END IF;


        -- Figure out which state to insert based on table name (EVENT) and eventtype (eq,st)
        SELECT  controlgroup, sourcetable, state, rank, result INTO 
        v_group, v_source, v_state, v_rank, v_result FROM trinetdb.autoposter WHERE
        acton='EVENT' and eventtype=NEW.etype;

        -- insert new PCS state, explicitely using the NEW event row lddate
        v_status := PCS.putState(v_group, v_source, NEW.evid, v_state, v_rank, v_result, NEW.lddate);

        -- using lddate from Event table
        UPDATE trinetdb.autoposter set (LASTID, LASTLDDATE) = (NEW.evid, NEW.lddate)  WHERE acton='EVENT' and eventtype=NEW.etype;

        RETURN NULL;
    END
$post_new_event$ LANGUAGE plpgsql;

-- Triggers have to be created AFTER the function is defined.
CREATE TRIGGER post_new_event AFTER INSERT ON trinetdb.event FOR EACH ROW EXECUTE PROCEDURE post_new_event();

-- Alter the table so trigger will always be fired, event if insert is from replication
ALTER TABLE trinetdb.event ENABLE ALWAYS TRIGGER post_new_event;
