-- Functions/Procedures to retrieve geographic description information for
-- points specified by lat,lon, or name.
--
-- Used in production: yes, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--  Package MATCH
--  Package QUARRY
--  Package UTIL
--
--  /tpp/bin/cattail.pl
--  /tpp/bin/ampdump.pl
--  /tpp/bin/stadist.pl
--  /tpp/bin/archive_db_total.pl
--
--  Java org.trinet.gazetteer.TN.WhereIsClosest... subclasses (CLASSES NOT IN USE?)
--  Java org.trinet.jasi.TN.SolutionTN
--  Java org.trinet.jiggle.CatalogPanel
--  Java org.trinet.util.graphics.table.CatalogPanel
--  Java org.trinet.util.graphics.table.JasiSolutionListPanel
--  Java org.trinet.util.graphics.table.AbstractJasiSolutionEditorPanel
--
-- <DEPENDS ON> (objects needing to be loaded on server)
--   PostGIS functions:
--   ST_GeographyFromText
--   ST_GeomFromEWKT
--   ST_AsText
--   ST_MakePoint
--   ST_Collect
--   ST_X
--   ST_Y
--   ST_Z
--   ST_3DClosestPoint
--   ST_Azimuth
--   ST_Distance
-- -------------------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION wheres.compass_pt(p_az DOUBLE PRECISION) RETURNS VARCHAR AS $$
DECLARE
v_compassPoints VARCHAR[] := ARRAY[ 'N', 'NNE', 'NE', 'ENE', 'E', 'ESE', 'SE', 'SSE', 'S', 'SSW', 'SW', 'WSW', 'W', 'WNW', 'NW', 'NNW'];
v_out VARCHAR := '';
v_az_tmp DOUBLE PRECISION;
v_index INTEGER;
BEGIN
    v_az_tmp = p_az + 11.25;
    IF (v_az_tmp >= 360.) THEN
        v_az_tmp = v_az_tmp - 360.;
    END IF;
    v_index = (v_az_tmp/22.5)::Integer;
    v_out = v_compassPoints[v_index+1];
    RETURN v_out;
END;
$$ language plpgsql;

DROP FUNCTION IF EXISTS wheres.az_to( p_lat1 DOUBLE PRECISION, p_lon1 DOUBLE PRECISION, p_lat2 DOUBLE PRECISION, p_lon2 DOUBLE PRECISION );
-- input decimal degrees latitude and longitude of two locations returns azimuth from point1 to point2.
CREATE OR REPLACE FUNCTION wheres.az_to ( p_lat1 DOUBLE PRECISION, p_lon1 DOUBLE PRECISION, p_lat2 DOUBLE PRECISION, p_lon2 DOUBLE PRECISION ) RETURNS DOUBLE PRECISION AS $$
DECLARE
   v_pt1 VARCHAR;
   v_pt2 VARCHAR;
   v_az DOUBLE PRECISION = 999;
BEGIN
   v_pt1 := 'SRID=4326;POINT(' || p_lon1 || ' ' || p_lat1 || ')';
   v_pt2 := 'SRID=4326;POINT(' || p_lon2 || ' ' || p_lat2 || ')';
   v_az := degrees(ST_Azimuth(ST_GeographyFromText(v_pt1),ST_GeographyFromText(v_pt2)));
   IF ( v_az < 0 ) THEN
       v_az := v_az + 360.;
   END IF;

   RETURN v_az;
END;
$$ language plpgsql;


DROP FUNCTION IF EXISTS wheres.separation_km ( p_lat1 DOUBLE PRECISION,
                                     p_lon1 DOUBLE PRECISION,
                                     p_lat2 DOUBLE PRECISION, 
                                     p_lon2 DOUBLE PRECISION);
-- input decimal degrees latitude and longitude of two locations
-- returns horizontal kilometers distance from point1 to point2.
CREATE OR REPLACE FUNCTION wheres.separation_km ( p_lat1 DOUBLE PRECISION,
                             p_lon1 DOUBLE PRECISION,
                             p_lat2 DOUBLE PRECISION,
                             p_lon2 DOUBLE PRECISION) RETURNS DOUBLE PRECISION AS $$
DECLARE
   v_pt1 VARCHAR;
   v_pt2 VARCHAR;
   v_dist_diff DOUBLE PRECISION = -1;
BEGIN
   v_pt1 := 'SRID=4326;POINT(' || p_lon1 || ' ' || p_lat1 || ')';
   v_pt2 := 'SRID=4326;POINT(' || p_lon2 || ' ' || p_lat2 || ')';
      -- calculate distance in km
   v_dist_diff := ST_Distance(ST_GeographyFromText(v_pt1),ST_GeographyFromText(v_pt2))/1000.0;

   RETURN v_dist_diff;
END;
$$ language plpgsql;

-- input decimal degrees latitude and longitude of two locations, elevation/-depth in kilometers.
-- returns 3-D distance from point1 to point2.
CREATE OR REPLACE FUNCTION wheres.separation_km ( p_lat1 DOUBLE PRECISION,
                             p_lon1 DOUBLE PRECISION,
                             p_z1   DOUBLE PRECISION,
                             p_lat2 DOUBLE PRECISION,
                             p_lon2 DOUBLE PRECISION,
                             p_z2   DOUBLE PRECISION ) RETURNS DOUBLE PRECISION AS $$
DECLARE
   v_pt1 VARCHAR;
   v_pt2 VARCHAR;
   v_dist_diff DOUBLE PRECISION = -1;
BEGIN
   v_pt1 := 'SRID=4326;POINT(' || p_lon1 || ' ' || p_lat1 || ' ' || p_z1 ||')';
   v_pt2 := 'SRID=4326;POINT(' || p_lon2 || ' ' || p_lat2 || ' ' || p_z2 ||')';
      -- calculate distance in km
   v_dist_diff := ST_Distance(ST_GeographyFromText(v_pt1),ST_GeographyFromText(v_pt2))/1000.0;

   RETURN v_dist_diff;
END;
$$ language plpgsql;


-- Return string describing closest town to input coordinates
DROP FUNCTION IF EXISTS wheres.getNearestTown(p_lat DOUBLE PRECISION, p_lon DOUBLE PRECISION);
-- with a State address two-letter code appended like: "Barstow, CA"
-- Example: select WHERES.getnearesttown(34., -118.);
CREATE OR REPLACE FUNCTION wheres.getNearestTown(p_lat DOUBLE PRECISION, p_lon DOUBLE PRECISION) RETURNS VARCHAR AS $$
DECLARE
   p_z DOUBLE PRECISION := 0.;
   v_dist DOUBLE PRECISION := NULL;
   v_az DOUBLE PRECISION := NULL;
   v_elev DOUBLE PRECISION := NULL;
   v_place VARCHAR := NULL;
    --
  BEGIN
    select * from wheres.point('town',p_lat,p_lon,p_z) into v_dist, v_az, v_elev, v_place;
    return v_place;
  END;
$$ language plpgsql;

-- input point gazetteertype name, latitude and longitude in decimal degrees.  and elevation/-depth in kilometers.
-- OUT parameters store calculated distance and azimuth from closest point of input type to specified source.
--CREATE OR REPLACE FUNCTION wheres.point (  p_type   IN VARCHAR,
--                     p_lat    IN DOUBLE PRECISION,
--                     p_lon    IN DOUBLE PRECISION,
--                     p_dist  OUT DOUBLE PRECISION,
--                     p_az    OUT DOUBLE PRECISION,
--                     p_place OUT VARCHAR ) AS $$
--DECLARE
--   v_dist DOUBLE PRECISION := NULL;
--   v_az DOUBLE PRECISION := NULL;
--   v_place VARCHAR := NULL;
--  BEGIN
--      select wheres.point(p_type,p_lat,p_lon,0.) into v_dist, v_az, v_place;
--      return v_place;
--  END;
--$$ language plpgsql;

DROP FUNCTION IF EXISTS wheres.point ( p_type   IN VARCHAR, 
                     p_lat    IN DOUBLE PRECISION, 
                     p_lon    IN DOUBLE PRECISION, 
                     p_z      IN DOUBLE PRECISION,
                     p_dist  OUT DOUBLE PRECISION,
                     p_az    OUT DOUBLE PRECISION,
                     p_elev  OUT DOUBLE PRECISION,
                     p_place OUT VARCHAR );
-- input point gazetteertype name, latitude and longitude in decimal degrees and elevation/-depth in kilometers.
-- OUT parameters store calculated distance, azimuth and elevation from closest point of input type to specified source.
CREATE OR REPLACE FUNCTION wheres.point (  p_type   IN VARCHAR,
                     p_lat    IN DOUBLE PRECISION,
                     p_lon    IN DOUBLE PRECISION,
                     p_z      IN DOUBLE PRECISION,
                     p_dist  OUT DOUBLE PRECISION,
                     p_az    OUT DOUBLE PRECISION,
                     p_elev  OUT DOUBLE PRECISION,
                     p_place OUT VARCHAR ) AS $$
DECLARE
   pt GEOMETRY := NULL;
   multi_pt GEOMETRY := NULL;
   closest_pt GEOMETRY := NULL;
   mytext VARCHAR := NULL;
   v_rowcount INTEGER := 0;
   v_state VARCHAR := NULL;
BEGIN
    -- translate input-point to a PostGIS point assuming WGS84 coordinates
    pt := ST_GeomFromEWKT( 'SRID=4326;POINT(' || p_lon || ' ' || p_lat || ' ' || p_z || ')' );
    --RAISE NOTICE USING MESSAGE= p_type || ' point: ' || ST_AsText(pt);

    -- First, retrieve all points of requested type and create a multipoint geometry from it 
    SELECT ST_Collect( ST_SetSRID(ST_MakePoint(lon, lat, COALESCE(z,0)), 4326)) INTO multi_pt 
    FROM trinetdb.gazetteerpt WHERE type = (select code from trinetdb.gazetteertype where name=p_type);

    GET DIAGNOSTICS v_rowcount = ROW_COUNT;
    --RAISE NOTICE USING MESSAGE='rows returned: ' || v_rowcount;    
    IF (ST_IsEmpty(multi_pt) OR multi_pt IS NULL) THEN
        -- no geographic points of this type found in table
        --RAISE NOTICE USING MESSAGE='No ' || p_type || 's found in gazetteerpt';
        p_dist := NULL;
        p_az := NULL;
        p_elev := NULL;
        p_place := NULL;
    ELSE
        -- geographic points of this type found in table
        --RAISE NOTICE USING MESSAGE= 'Claims to have points of type ' || p_type || ' found.';

        closest_pt := ST_3DClosestPoint(multi_pt,pt);
        -- change distance unit from meter to km 
        p_dist := ST_Distance(pt::geography,closest_pt::geography)/1000.0;
        p_az := degrees(ST_Azimuth(closest_pt::geography,pt::geography));
        IF ( p_az < 0 ) THEN
            p_az := p_az + 360.;
        END IF;
        p_elev := (ST_Z(closest_pt) - ST_Z(pt))::DOUBLE PRECISION;
        
        mytext := ST_AsText(closest_pt);
    
        --RAISE NOTICE USING MESSAGE='closest: ' || mytext;
    
        SELECT name, state  INTO mytext, v_state FROM trinetdb.gazetteerpt WHERE lon=ST_X(closest_pt) and lat=ST_Y(closest_pt) and type=(select code from trinetdb.gazetteertype where name=p_type);

        p_place := mytext || ', ' || v_state;
    
        GET DIAGNOSTICS v_rowcount = ROW_COUNT;
        IF ( v_rowcount > 0  AND p_place IS NOT NULL ) THEN
           --RAISE NOTICE USING MESSAGE='output:' || ' p_dist: ' || p_dist || ' p_az: ' || p_az || ' p_elev: ' || p_elev || ' p_place: ' || p_place;
        END IF;
    END IF;
    
END;
$$ language plpgsql;

DROP FUNCTION IF EXISTS wheres.locale_by_type(p_lat DOUBLE PRECISION, p_lon DOUBLE PRECISION, p_depth DOUBLE PRECISION, p_type VARCHAR);
CREATE OR REPLACE FUNCTION wheres.locale_by_type(p_lat DOUBLE PRECISION, p_lon DOUBLE PRECISION, p_z DOUBLE PRECISION, p_type VARCHAR) RETURNS VARCHAR AS $$
DECLARE
  v_dist DOUBLE PRECISION;
  v_az   DOUBLE PRECISION;
  v_elev DOUBLE PRECISION;
  v_place VARCHAR;
  v_direction VARCHAR;
  v_string VARCHAR;
BEGIN
   select * from wheres.point(p_type,p_lat,p_lon,p_z) into v_dist, v_az, v_elev, v_place;
   SELECT * INTO v_direction FROM wheres.compass_pt(v_az);
   v_string := format('%s km (%s mi) %s (az=%s) from %s',to_char(v_dist,'999D9'),to_char(v_dist*0.621371,'999D9'),v_direction,to_char(v_az,'999D9'),v_place);
   RETURN v_string;
END;
$$ language plpgsql;

-- difference between above: only give compass direction, don't spell out azimuth
CREATE OR REPLACE FUNCTION wheres.locale_by_type2(p_lat DOUBLE PRECISION, p_lon DOUBLE PRECISION, p_z DOUBLE PRECISION, p_type VARCHAR) RETURNS VARCHAR AS $$
DECLARE
  v_dist DOUBLE PRECISION;
  v_az   DOUBLE PRECISION;
  v_elev DOUBLE PRECISION;
  v_place VARCHAR;
  v_direction VARCHAR;
  v_string VARCHAR;
BEGIN
   select * from wheres.point(p_type,p_lat,p_lon,p_z) into v_dist, v_az, v_elev, v_place;
   SELECT * INTO v_direction FROM wheres.compass_pt(v_az);
   v_string := format('%s km (%s mi) %s from %s',to_char(v_dist,'999D9'),to_char(v_dist*0.621371,'999D9'),v_direction,v_place);
   RETURN v_string;
END;
$$ language plpgsql;
