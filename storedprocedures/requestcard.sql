--CREATE SCHEMA IF NOT EXISTS requestcard AUTHORIZATION code;
--GRANT USAGE ON SCHEMA requestcard TO trinetdb_read, trinetdb_execute;

-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------
--  v_pkgId VARCHAR2(80) := '$Id: requestcard_pkg_body.sql 7400 2014-06-20 00:15:39Z awwalter $';
-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------
--    FUNCTION getPkgId RETURN VARCHAR2 AS
--    BEGIN
--      RETURN v_pkgId;
--    END getPkgId;
--    --
--    FUNCTION getPkgSpecId RETURN VARCHAR2 AS
--    BEGIN
--      RETURN PKG_SPEC_ID;
--    END getPkgSpecId;
--
-- ------------------------------------------------------------------------
-- ------------------------------------------------------------------------
  -- REQUEST_CARD TABLE COLUMNS:
   -- RCID (IS THE PRIMARY KEY)
   -- EVID AUTH SUBSOURCE NET STA SEEDCHAN LOCATION STAAUTH DATETIME_ON DATETIME_OFF
   -- REQUEST_TYPE
   -- RETRY LASTRETRY PRIORITY WFID PCSTORED STATUS
   --
   -- NOTE A UNIQUE INDEX FOR COLS: AUTH SUBSOURCE NET STA SEEDCHAN LOCATION STAAUTH DATETIME_ON DATETIME_OFF REQUEST_TYPE 
   --
DROP FUNCTION IF EXISTS requestcard.make_t_request(p_evid REQUEST_CARD.evid%TYPE,
                            p_auth REQUEST_CARD.auth%TYPE,
                            p_subsource REQUEST_CARD.subsource%TYPE,
                            p_staauth REQUEST_CARD.staauth%TYPE,
                            p_net REQUEST_CARD.net%TYPE,
                            p_sta REQUEST_CARD.sta%TYPE,
                            p_seedchan REQUEST_CARD.seedchan%TYPE,
                            p_location REQUEST_CARD.location%TYPE,
                            p_channel REQUEST_CARD.channel%TYPE,
                            p_start REQUEST_CARD.datetime_on%TYPE,
                            p_end REQUEST_CARD.datetime_off%TYPE,
                            p_commit INTEGER);
CREATE OR REPLACE FUNCTION requestcard.make_t_request(p_evid REQUEST_CARD.evid%TYPE,
                            p_auth REQUEST_CARD.auth%TYPE,
                            p_subsource REQUEST_CARD.subsource%TYPE,
                            p_staauth REQUEST_CARD.staauth%TYPE,
                            p_net REQUEST_CARD.net%TYPE,
                            p_sta REQUEST_CARD.sta%TYPE,
                            p_seedchan REQUEST_CARD.seedchan%TYPE,
                            p_location REQUEST_CARD.location%TYPE,
                            p_channel REQUEST_CARD.channel%TYPE,
                            p_start REQUEST_CARD.datetime_on%TYPE,
                            p_end REQUEST_CARD.datetime_off%TYPE,
                            p_commit INTEGER) RETURNS INTEGER AS $$
    --
   DECLARE 
        v_status INTEGER := 0;
        v_rowid BIGINT := NULL;
        -- Use defaults for type and priority:
        v_type VARCHAR(1) := 'T';
        v_priority INTEGER := 1;
    BEGIN
      --RAISE INFO USING MESSAGE = 'calling requestcard.make_t_request for ' || p_sta || '.' || p_seedchan || '.' || p_location;
      SELECT COUNT(*) INTO v_status FROM event e WHERE
          e.EVID=p_evid;
      IF (v_status = 0) THEN
        RETURN -2;
      END IF;
      --
      IF (p_location = '--') THEN
	p_location := '  ';
      END IF;
      -- Do we have an AssocWaE row for this channel?
      SELECT COUNT(*) INTO v_status FROM Waveform w, AssocWaE a WHERE
          a.EVID=p_evid AND w.wfid=a.wfid AND
          w.NET=p_net AND w.STA=p_sta AND w.SEEDCHAN=p_seedchan AND w.LOCATION=p_location;
      --
      BEGIN
        SELECT rcid INTO STRICT v_rowid FROM REQUEST_CARD WHERE
          EVID=p_evid AND AUTH=p_auth AND SUBSOURCE=p_subsource AND
          NET=p_net AND STA=p_sta AND SEEDCHAN=p_seedchan AND LOCATION=p_location AND STAAUTH=p_staauth 
          AND request_type=v_type;
          --
          -- Hmmm existing request_card row, do we want to re-window its time span?
          -- UPDATE REQUEST_CARD SET retry=0,datetime_on=p_start,datetime_off=p_end WHERE ROWID=v_rowid;
          -- No just update retry count here
          UPDATE REQUEST_CARD SET retry=0 WHERE rcid=v_rowid;
          --
          GET DIAGNOSTICS v_status = ROW_COUNT;
	  --RAISE INFO USING MESSAGE = 'Reset retry on existing request_card' || v_rowid || ' status code: ' || v_status;
	  RETURN v_status;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        IF (v_status > 0) THEN
          -- No-op ? we have an existing trigger AssocWaE row but no request_card row, we don't know how complete it is
	  --RAISE INFO USING MESSAGE = 'AssocWaE row but no request_card row. Do nothing but pretend it is good by returning 1';
          RETURN 1;
        ELSE 
          -- Neither AssocWaE nor request_card row exists so create a new request_card row
          INSERT INTO REQUEST_CARD
          (rcid,evid,auth,subsource,staauth,net,sta,seedchan,channel,location,request_type,datetime_on,datetime_off,retry,priority)
          VALUES
          (nextval('reqseq'),p_evid,p_auth,p_subsource,p_staauth,p_net,p_sta,p_seedchan,p_channel,p_location,v_type,p_start,p_end,0,v_priority);
          GET DIAGNOSTICS v_status = ROW_COUNT;
	  --RAISE INFO USING MESSAGE = 'Inserted ' || v_status || ' rows';
	  RETURN v_status;
        END IF;
      END;
      --
    --
      RETURN v_status;
    --
    EXCEPTION WHEN OTHERS THEN
      RAISE NOTICE USING MESSAGE='PostgreSQL Error: ' || SQLSTATE || ' ' || SQLERRM;
        --MESSAGE ='EXCEPTION make_t_request failed for ' ||
        --evid || ' ' || p_auth || ' ' || p_subsource || ' ' || p_staauth ||
        --net || '.' || p_sta || '.' || p_seedchan || '.' || p_location ||
        --from ' || p_start || ' to ' || p_end || ' '
        --' CODE: ' || SQLSTATE;
      RETURN -999;
    END;
$$ LANGUAGE plpgsql;
--
-- ---------------------------------------------------------------------------------
-- ---------------------------------------------------------------------------------
DROP FUNCTION IF EXISTS requestcard.batch_t_request(p_evid REQUEST_CARD.evid%TYPE,
                            p_auth REQUEST_CARD.auth%TYPE,
                            p_subsource REQUEST_CARD.subsource%TYPE,
                            p_staauth REQUEST_CARD.staauth%TYPE,
                            p_net REQUEST_CARD.net%TYPE,
                            p_sta REQUEST_CARD.sta%TYPE,
                            p_seedchan REQUEST_CARD.seedchan%TYPE,
                            p_location REQUEST_CARD.location%TYPE,
                            p_channel REQUEST_CARD.channel%TYPE,
                            p_start REQUEST_CARD.datetime_on%TYPE,
                            p_end REQUEST_CARD.datetime_off%TYPE,
                            p_commit INTEGER);
CREATE OR REPLACE FUNCTION requestcard.batch_t_request(p_evid REQUEST_CARD.evid%TYPE,
                            p_auth REQUEST_CARD.auth%TYPE,
                            p_subsource REQUEST_CARD.subsource%TYPE,
                            p_staauth REQUEST_CARD.staauth%TYPE,
                            p_net REQUEST_CARD.net%TYPE,
                            p_sta REQUEST_CARD.sta%TYPE,
                            p_seedchan REQUEST_CARD.seedchan%TYPE,
                            p_location REQUEST_CARD.location%TYPE,
                            p_channel REQUEST_CARD.channel%TYPE,
                            p_start REQUEST_CARD.datetime_on%TYPE,
                            p_end REQUEST_CARD.datetime_off%TYPE,
                            p_commit INTEGER) RETURNS VOID AS $$
    DECLARE
       v_stat INTEGER := 0;
    BEGIN
      v_stat := requestcard.make_t_request(p_evid, p_auth, p_subsource, p_staauth, p_net, p_sta, p_seedchan, p_location, p_channel, p_start, p_end, p_commit);
    END;
$$ LANGUAGE plpgsql;
