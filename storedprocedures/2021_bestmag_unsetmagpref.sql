
create or replace  FUNCTION epref.unsetprefmag(p_evid    EVENT.EVID%TYPE,  p_magid   NETMAG.MAGID%TYPE) RETURNS INTEGER AS $$

 DECLARE
    -- init local variables
    v_pm_type NETMAG.MAGTYPE%TYPE  := NULL;
    v_pm_id   NETMAG.MAGID%TYPE    := 0;
    v_orid    ORIGIN.ORID%TYPE     := 0;
    v_prefor  EVENT.PREFOR%TYPE    := 0;
    v_status    INTEGER  := 0;
    v_bestmagid INTEGER  := 0;
    v_prefmagid INTEGER  := 0;

 BEGIN
    IF (p_evid IS NULL OR p_magid IS NULL) THEN
      RETURN -1;
    END IF;

    -- Get the best magid considering only origins associated with this p_evid:
    v_bestmagid := magpref.magpref_bestMagidOfEvent(p_evid);
    -- Get current event.prefmag
    SELECT coalesce(prefmag, 0) INTO v_prefmagid FROM event WHERE evid = p_evid;

    --RAISE INFO 'MTH: unsetprefmag(p_evid:%, p_magid:%) bestmagid=% event.prefmag=%', p_evid, p_magid, v_bestmagid, v_prefmagid;

    -- Check if we're trying to unset v_bestmagid
    IF (v_bestmagid = p_magid) THEN
      RAISE INFO 'unsetprefmag(p_evid:%, p_magid:%) p_magid=%=bestmagid --> STOP', p_evid, p_magid, p_magid;
      RETURN -1;
    END IF;

    IF (v_bestmagid = v_prefmagid) THEN
      RAISE INFO 'unsetprefmag(%) p_magid=%=event.prefmag --> event.prefmag already set correctly', p_evid, p_magid;
      RETURN 0;
    END IF;

    -- Is p_magid in eventprefmag ?
    SELECT coalesce(magid, 0) INTO v_status FROM eventprefmag WHERE evid = p_evid AND magid = p_magid;
    --RAISE INFO 'MTH: unsetprefmag(p_evid:%, p_magid:%) check for p_magid in eventprefmag returned v_status:%', p_evid, p_magid, v_status;
    IF (v_status IS NOT NULL) THEN
    -- Delete p_magid from eventprefmag
      DELETE FROM eventprefmag WHERE evid = p_evid AND magid = p_magid;
      GET DIAGNOSTICS v_status = ROW_COUNT;
      --RAISE INFO 'MTH: unsetprefmag --> DELETE (p_evid:%, p_magid:%) from eventprefmag returned v_status:%', p_evid, p_magid, v_status;
    ELSE
      --RAISE INFO 'MTH: unsetprefmag --> No entry in eventprefmag for p_evid:% p_magid:% to delete', p_evid, p_magid;

    END IF;

    -- Insert v_bestmagid into eventprefmag if it's not already there:
    --  v_status = 1 when successful
    v_status := epref.setprefmag_magtype(p_evid, v_bestmagid);
    --RAISE INFO 'MTH: unsetprefmag --> setprefmag_magtype(p_evid:%, p_magid:%) returned v_status:%', p_evid, v_bestmagid, v_status;

    -- Get bestmagid from among eventprefmag rows and use it to set event.prefmag:
    v_status := magpref.setprefmagofevent(p_evid);
    --RAISE INFO 'MTH: unsetprefmag --> setprefmagofevent(p_evid:%) returned v_status:%', p_evid, v_status;

    RETURN 1;
 END
$$ LANGUAGE plpgsql;


create or replace FUNCTION magpref.magpref_bestMagidOfEvent(p_evid Event.evid%TYPE) RETURNS BIGINT AS $$
 DECLARE
        v_bestmagid  INT;
        v_bestprio   INT;
        v_date       TIMESTAMP(0);

        --
        -- Get list of current mags in order of priority
        -- Ordered by priority, then 1st one is used. If there is a priority tie
        -- The latest one is used (largest lddate).
        --
        -- Include the origin.prefmag although its orid may be different !!!
        -- c_mags CURSOR FOR
    BEGIN
        --RAISE INFO 'MTH: magpref.magpref_bestMagidOfEvent(%)', p_evid;
        --SELECT n.magid, MAGPREF.GETMAGPRIORITY(v_bestmagid), n.lddate INTO v_bestmagid, v_bestprio, v_date
        SELECT n.magid, MAGPREF.GETMAGPRIORITY(n.magid), n.lddate INTO v_bestmagid, v_bestprio, v_date
        FROM netmag n, origin o
        where (n.orid=o.orid OR n.magid=o.prefmag) AND o.evid=p_evid
          ORDER BY 2 DESC, 3 DESC;

        --RAISE INFO 'MTH: magpref.magpref_bestMagidOfEvent(%) v_bestmagid:% v_bestprio:%', p_evid, v_bestmagid, v_bestprio;
        --return v_bestmagid;

        --SELECT n.magid, MAGPREF.GETMAGPRIORITY(n.magid, o.datetime, o.lat, o.lon), n.lddate
          --as v_bestmagid, v_bestprio, v_date
          --FROM NetMag n, Origin o
          --WHERE (n.orid = o.orid OR n.magid=o.prefmag) AND o.evid = p_evid
          --ORDER BY 2 DESC, 3 DESC;

        --
        --
        -- Get the first row in the CURSOR list, it will be the one with the highest priority
        -- ( If there is a tie, the latest in time in the list will be used. If no rows, return -1. )
        --
        -- The OPEN actually executes the cursor SELECT statment
        -- OPEN  c_mags;
        -- FETCH c_mags INTO v_bestmagid, v_bestprio, v_date;
        -- CLOSE c_mags;
        --
        -- No good mags for this evid (or no such evid)
        IF (v_bestmagid IS NULL OR v_bestprio < 0) THEN -- added v_bestprio >= 0 condition -aww 2009/03/15
          RETURN 0;
        END IF;
        --
        -- Do not use undefined mag types. (Not sure if this is the right thing to do)
        -- COMMENTED OUT: so any type will be used so long as it is not overriden by
        -- one with a better priority.
        --
        /*
        -- get priority
        IF (v_bestprio = -1) THEN
        RETURN -1;
        END IF;
        */
        --
        RETURN  v_bestmagid;
        --
        -- EXCEPTION WHEN NO_DATA_FOUND THEN
        --  RETURN 0;
  END --bestMagidOfEvent;
$$ LANGUAGE plpgsql;
