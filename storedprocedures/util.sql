-- ---------------------------------------------------------------------------
-- Return count of waveforms associated with an event.
-- Example: select util.waveformcount(1111);
CREATE OR REPLACE FUNCTION util.waveformCount(p_evid Event.evid%TYPE) RETURNS INTEGER AS $$
DECLARE
    v_cnt INTEGER := 0;
BEGIN
    SELECT count(wfid) INTO v_cnt FROM AssocWaE WHERE evid = p_evid;
    RETURN v_cnt;
END;
$$ LANGUAGE plpgsql;

-- ---------------------------------------------------------------------------
-- Return count of distinct channel waveform SNCL associated with an event.
-- Value is less than waveformCount when any channel has multiple waveform rows.
-- RH: Oracle version of this function returns a NUMBER of any precision/scale.
-- I like integer better for a count.
CREATE OR REPLACE FUNCTION util.waveformChannelCount(p_evid Event.evid%TYPE) RETURNS INTEGER AS $$
DECLARE
    v_cnt INTEGER := 0;
BEGIN
    SELECT count(*) into v_cnt FROM (SELECT count(*) FROM Waveform w, AssocWaE a
      WHERE w.wfid=a.wfid and a.evid=p_evid group by w.net,w.sta,w.seedchan,w.location) AS jnk;
    --
    RETURN v_cnt;
END;
$$ LANGUAGE plpgsql;

-- ---------------------------------------------------------------------------
-- Get string that identifies who created Origin row corresponding to input orid
-- Returns NULL if no such row or error.
DROP FUNCTION IF EXISTS util.getWhoOrigin(p_orid Origin.orid%TYPE);
CREATE OR REPLACE FUNCTION util.getWhoOrigin(p_orid Origin.orid%TYPE) RETURNS VARCHAR(80) AS $$
DECLARE
v_who Credit.refer%TYPE := NULL;
BEGIN
   SELECT c.refer INTO v_who FROM credit c WHERE c.id=p_orid and c.tname='ORIGIN';
   RETURN v_who;
   EXCEPTION WHEN OTHERS THEN
     RETURN NULL;
END;
$$ LANGUAGE plpgsql;

-- ---------------------------------------------------------------------------
-- Get string that identifies who created NetMag table row corresponing to input magid.
-- Returns NULL if no such row or error.
DROP FUNCTION IF EXISTS util.getWhoNetMag(p_magid Netmag.magid%TYPE);
CREATE OR REPLACE FUNCTION util.getWhoNetMag(p_magid Netmag.magid%TYPE) RETURNS VARCHAR(80) AS $$
DECLARE
v_who Credit.refer%TYPE := NULL;
BEGIN
   SELECT c.refer INTO v_who FROM credit c WHERE c.id=p_magid and c.tname='NETMAG';
     RETURN v_who;
   EXCEPTION WHEN OTHERS THEN
     RETURN NULL;
END;
$$ LANGUAGE plpgsql;
