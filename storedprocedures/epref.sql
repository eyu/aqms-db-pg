-- create schema if not exists epref authorization code;
-- grant usage on schema epref to trinetdb_read, trinetdb_execute;

DROP FUNCTION IF EXISTS epref.insertNetMag( p_orid      NetMag.orid%TYPE,
                         p_magVal    NetMag.magnitude%TYPE,
                         p_magType   NetMag.magtype%TYPE,
                         p_auth      NetMag.auth%TYPE,
                         p_subsrc    NetMag.subsource%TYPE,
                         p_algo      NetMag.magalgo%TYPE,
                         p_nsta      NetMag.nsta%TYPE,
                         p_nobs      NetMag.nobs%TYPE,
                         p_unc       NetMag.uncertainty%TYPE,
                         p_gap       NetMag.gap%TYPE,
                         p_dist      NetMag.distance%TYPE,
                         p_qual      NetMag.quality%TYPE,
                         p_rflag     NetMag.rflag%TYPE,
                         p_commit  integer);
-- Insert a new NetMag row as described by input values.
-- Return the new Netmag.magid taken from the id sequencer.
-- Use when values of nsta, uncertainty, gap, distance are unknown.
-- Note this DOES NOT UPDATE event prefmag, just inserts a new row.
-- Returns value of new magid on success.
-- Commits transaction if successful.
create or replace FUNCTION epref.insertNetMag( p_orid      NetMag.orid%TYPE,
                         p_magVal    NetMag.magnitude%TYPE,
                         p_magType   NetMag.magtype%TYPE,
                         p_auth      NetMag.auth%TYPE,
                         p_subsrc    NetMag.subsource%TYPE,
                         p_algo      NetMag.magalgo%TYPE,
                         p_nsta      NetMag.nsta%TYPE,
                         p_nobs      NetMag.nobs%TYPE,
                         p_unc       NetMag.uncertainty%TYPE,
                         p_gap       NetMag.gap%TYPE,
                         p_dist      NetMag.distance%TYPE,
                         p_qual      NetMag.quality%TYPE,
                         p_rflag     NetMag.rflag%TYPE,
                         p_commit  integer) RETURNS bigint AS $$
DECLARE
    -- init local variable
    v_magid bigint := 0;
    v_status integer := 0;
BEGIN
    --
    BEGIN
      -- No FK constraint on NETMAG.orid, check if such an origin exists -aww
      -- SELECT 0 INTO v_magid FROM origin WHERE orid = p_orid;
      -- EXCEPTION WHEN NO_DATA_FOUND THEN
      --  DBMS_OUTPUT.PUT_LINE('EPREF.insertNetMag(...) no ORIGIN row exists for input orid: ' || p_orid);
      -- Do we want to continue or raise exception here -aww ?
      -- POSTGRESQL NOTE - paulf: this really should be a FK constraint.
    END;
    -- get the next 'magid'
    SELECT nextval('magseq') INTO v_magid;
    -- inserting null values when not null constraints are enforced will raise exception
    INSERT INTO NetMag
    (magid,orid,magnitude,magtype,auth,subsource,magalgo,nsta,nobs,uncertainty,gap,distance,quality,rflag)
      VALUES
    (v_magid,p_orid,p_magVal,p_magType,p_auth,p_subsrc,p_algo,p_nsta,p_nobs,p_unc,p_gap,p_dist,p_qual,p_rflag);
    --
    --IF p_commit THEN
    --  COMMIT; 
    --END IF;
    RETURN v_magid;
END
$$ LANGUAGE plpgsql;


-- ---------------------------------------------------------------------------
--
-- If the ORGPREFPRIORITY table has rules that apply to the input origins's type,
-- the new origin must have the highest ranked priority >= 0, otherwise this function is a no-op,
-- no row updates and no row insertions.
--
-- Set Event.prefor table row prefor value to input orid regardless of its type.
-- Update or insert row in EventPrefor table corresponding to the Origin.type
-- Bumps version if event preferred origin changed, commits changes.
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefor was updated.
--
-- Does NOT check for foreign key consistency, i.e.
-- the corresponding Origin.evid or Event.prefmag's Netmag.orid.
create or replace  FUNCTION epref.setprefor_event(p_evid Event.evid%TYPE, p_orid Origin.orid%TYPE,
                            p_bump INTEGER, p_commit INTEGER) RETURNS INTEGER AS $$
DECLARE
    v_otype Origin.type%TYPE := NULL;
BEGIN
    SELECT type INTO v_otype FROM Origin WHERE orid = p_orid;
    --
    -- check for a NULL value
    IF (v_otype IS NULL) THEN
      RETURN -1;
    END IF;
    --
    RETURN epref.setprefor_type(p_evid, p_orid, v_otype, 1, p_bump, p_commit);
END 
$$ LANGUAGE plpgsql;

-- ---------------------------------------------------------------------------
--
-- Set preferred origin for an event corresponding to the type of the input orid
-- if input orid <> event.prefor and the event.prefor is the same type as orid,
-- updates both the Event table prefor and EVENTPREFOR table.
-- Insert a new EVENTPREFOR table row if none already exists.
--
-- If the ORGPREFPRIORITY table has rules that apply to the input origins's type,
-- the new origin must have the highest ranked priority >= 0, otherwise this function is a no-op,
-- no row updates and no row insertions.
--
-- A row must already exist in the Event table whose key is input evid.
-- A row must already exist in the Origin table whose key is orid value of the Origin row.
-- If either the Event or the Origin row is missing the transaction fails.
--
-- Bumps version if event preferred origin (prefor) changes, commits changes.
--
-- Returns value > 0 on success, <= 0 if not. Return value >= 2 if event.prefor was updated.
-- As above checks priority rule, with added control flags:
-- p_evtpref  > 0, set p_orid event preferred regardless of type.
--
-- p_bump     > 0, bump event version if Event.prefor changed.
--              NOTE: if Event.prefor is NULL or existing Event.prefor has same type
--              as input orid you must set p_bump=1 for Event.version to be incremented.
--
-- p_commit   > 0, commit transaction changes.



-- ---------------------------------------------------------------------------
create or replace FUNCTION epref.setprefor_event(p_evid Event.evid%TYPE, p_orid Origin.orid%TYPE,
                            p_bump INTEGER, p_commit INTEGER) RETURNS INTEGER AS $$
DECLARE
    v_otype Origin.type%TYPE := NULL;
BEGIN
    SELECT type INTO v_otype FROM Origin WHERE orid = p_orid;
    --
    -- check for a NULL value
    IF (v_otype IS NULL) THEN
      RETURN -1;
    END IF;
    --
    RETURN epref.setprefor_type(p_evid, p_orid, v_otype, 1, p_bump, p_commit);
END
$$ LANGUAGE plpgsql;
-- ---------------------------------------------------------------------------
create or replace FUNCTION epref.setprefor_type(p_evid    EVENT.EVID%TYPE,
                          p_orid    Origin.orid%TYPE,
                          p_otype   Origin.type%TYPE,
                          p_evtpref INTEGER,
                          p_bump    INTEGER,
                          p_commit  INTEGER) RETURNS INTEGER AS $$
DECLARE
BEGIN
    RETURN epref.setprefor_type(p_evid, p_orid, p_otype, p_evtpref, p_bump, p_commit, 1);
END 
$$ LANGUAGE plpgsql;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
-- another overloaded signature to allow input option to ignore the orgpref.HIGHEST_PRIORITY rule for Jiggle? -aww
--
create or replace  FUNCTION epref.setprefor_type(p_evid           EVENT.EVID%TYPE,
                          p_orid           Origin.orid%TYPE,
                          p_otype          Origin.type%TYPE,
                          p_evtpref        INTEGER,
                          p_bump           INTEGER,
                          p_commit         INTEGER,
                          p_check_priority INTEGER) RETURNS INTEGER AS $$
DECLARE
    -- init local variables
    v_prefType Origin.type%TYPE   := NULL;
    v_prefor   EVENT.PREFOR%TYPE  := 0;
    v_status   INTEGER        := 0;
    v_eprefu   INTEGER        := 0;
    v_bump     INTEGER        := 0;
    --
BEGIN
    -- BEGIN
      IF (p_evid IS NULL OR p_orid IS NULL OR p_otype IS NULL) THEN
        RETURN -1;
      END IF;
      -- v_prefor will contain the prefered origin id for this event
      -- v_prefType will contain the prefered type for this event
      SELECT coalesce(o.orid, 0), o.type INTO v_prefor, v_prefType FROM origin o, event e
      WHERE o.orid = e.prefor AND e.evid=p_evid;
      -- current prefor may be different type
      IF (p_check_priority > 0 AND v_prefor <> p_orid and v_prefor IS NOT NULL) THEN
	-- TODO:
        -- not calling orgpref yet ...paulf, not sure orgpref is used by anythingelse just yet
        -- v_status := ORGPREF.highest_priority(v_prefor, p_orid);
        v_status := v_prefor;
        IF (v_status = v_prefor) THEN
          v_status := -99;
        END IF;
      END IF;

      --RAISE NOTICE 'v_status=%', v_status;
      IF (v_status <> -99) THEN
        -- Existing Event.prefor is not a higher priority so it's ok to update
        -- EVENT table if input flag > 0 or prefor type matches and orids do not match
        -- RAISE NOTICE '1 p_evtpref %, v_prefType %', p_evtpref, v_prefType;
        IF ( (p_evtpref > 0) OR ((v_prefType = p_otype) AND (v_prefor <> p_orid)) OR (v_prefor IS NULL) ) THEN
          -- RAISE NOTICE '2 p_evtpref %, v_prefType %', p_evtpref, v_prefType;
          IF (v_prefor <> p_orid or v_prefor IS NULL) THEN -- update only if different
            -- RAISE NOTICE 'updating event %, prefor %', p_evid, p_orid;
            UPDATE event SET prefor = p_orid WHERE evid = p_evid RETURNING prefor INTO v_prefor;
            GET DIAGNOSTICS v_status = ROW_COUNT;
            v_status := v_status + 1;
            v_eprefu := v_status;
          ELSE -- just get prefor
            -- SELECT prefor INTO v_prefor FROM event WHERE evid = p_evid;
            v_status := 1;
          END IF;
        END IF;
      END IF;
    --END
    --
    IF (v_status > 1 AND p_bump > 0) THEN
      v_bump := 1; -- flag to bump
    END IF;
    --
    -- Process data for EVENTPREFOR table DML
    --
    v_status := epref.update_eventprefor(p_evid, p_orid, p_otype, 0, p_check_priority); -- does commit action by flag
    IF (v_status = 9999) THEN
      v_status := 0;
    ELSIF ( v_status > 1 ) THEN
      -- to discriminate from v_eprefu return value
      v_status := 1;
    END IF;
    --
    -- Return result of updating event prefor -- aww 2014/05/15
    IF ( v_eprefu >= 2 ) THEN
        v_status := v_eprefu;
    END IF;
    --
    IF (v_bump > 0 AND p_bump > 0) THEN
       v_bump := epref.bump_version(p_evid, 0);
    END IF;
    --
    RETURN v_status;
END
$$ LANGUAGE plpgsql;

-- ---------------------------------------------------------------------------
--
create or replace FUNCTION epref.bump_version(p_evid Event.evid%TYPE, p_commit INTEGER) RETURNS INTEGER AS $$
DECLARE
    v_status  INTEGER := 0;
    v_version Event.version%TYPE := NULL;
BEGIN
    SELECT version INTO v_version FROM event WHERE evid = p_evid;
    IF (v_version IS NULL) THEN
          --v_version := 1;
          v_version := 0; -- aww 04/23/2008 NCEDC wants this
    END IF;
    --
    v_version := v_version + 1;
    --
    UPDATE event set version = v_version where evid = p_evid;
    GET DIAGNOSTICS v_status = ROW_COUNT;
    --
    IF (v_status > 0) THEN
        --
        RETURN v_version;
        --
    END IF;
    RETURN -1;
END
$$ LANGUAGE plpgsql;

-- this version only takes evid as argument and commits upon success.
create or replace FUNCTION epref.bump_version(p_evid Event.evid%TYPE) RETURNS INTEGER AS $$
BEGIN
    RETURN epref.bump_version(p_evid,1);
END
$$ LANGUAGE plpgsql;

create or replace FUNCTION epref.setprefmag_magtype(p_evid    EVENT.EVID%TYPE,
                              p_magid   NETMAG.MAGID%TYPE) RETURNS INTEGER AS $$
DECLARE
BEGIN
    RETURN epref.setprefmag_magtype(p_evid, p_magid,  1, 0, 0);
END
$$ LANGUAGE plpgsql;

create or replace FUNCTION epref.setprefmag_magtype(p_evid    EVENT.EVID%TYPE,
                              p_magid   NETMAG.MAGID%TYPE,
                              p_evtpref INTEGER,
                              p_bump    INTEGER,
                              p_commit  INTEGER) RETURNS INTEGER AS $$
DECLARE
    v_status  INTEGER         := 0;
    v_rows  INTEGER         := 0;
    v_orid    ORIGIN.ORID%TYPE    := 0;
    v_evid    EVENT.EVID%TYPE     := 0;
    v_magtype NETMAG.MAGTYPE%TYPE := NULL; -- not null constraint in Netmag
BEGIN
    -- check for event.evid row existance
    SELECT 1 INTO v_status FROM event WHERE evid = p_evid;
    GET DIAGNOSTICS v_rows = ROW_COUNT;
    IF (v_rows <= 0) THEN
      RETURN -1;
    END IF;
    --
    -- check for netmag.magid row existance
    SELECT orid INTO v_orid FROM netmag WHERE magid = p_magid;
    GET DIAGNOSTICS v_rows = ROW_COUNT;
    IF (v_rows <= 0) THEN
      RETURN -1;
    END IF;
    --
    -- check netmag row origin evid matches event.evid, in case of user input typo error
    SELECT coalesce(evid, 0) INTO v_evid FROM origin WHERE orid = v_orid;
    -- MTH: remove check since Mww evid/orid are different from local evid/orid
    -- IF (v_evid <> p_evid) THEN
      -- RETURN -1;
    --END IF; 
    --
    SELECT magtype INTO v_magtype FROM netmag WHERE magid = p_magid;
    --
    -- check for a NULL magtype value (redundant here Netmag table constraint is not null)
    IF (v_magtype IS NULL) THEN 
      RETURN -1;
    END IF;
    -- RAISE NOTICE 'working on magtype %, magid %, for origin %', v_magtype, p_magid, v_orid;
    --
    -- p_evtpref > 0 set Event.prefmag to input magid.
    -- otherwise set new prefmag value only if its magtype matches input.
    -- Set Origin.prefmag only if null or its current magtype matches input or
    -- the input mag is associated with this origin and p_evtpref > 0.
    RETURN epref.setprefmag_magtype(p_evid, p_magid, v_magtype, p_evtpref, p_bump, p_commit);
END 
$$ LANGUAGE plpgsql;

create or replace  FUNCTION epref.setprefmag_magtype(p_evid    EVENT.EVID%TYPE,
                              p_magid   NETMAG.MAGID%TYPE,
                              p_magtype NETMAG.MAGTYPE%TYPE,
                              p_evtpref INTEGER,
                              p_bump INTEGER,
                              p_commit INTEGER) RETURNS INTEGER AS $$
DECLARE
BEGIN
    RETURN epref.setprefmag_magtype(p_evid, p_magid, p_magtype, p_evtpref, p_bump, p_commit, 1);
END 
$$ LANGUAGE plpgsql;

-- ---------------------------------------------------------------------------
-- another overloaded signature to allow input option to ignore the magpref.HIGHEST_PRIORITY rule for Jiggle? -aww
--
create or replace  FUNCTION epref.setprefmag_magtype(p_evid    EVENT.EVID%TYPE,
                              p_magid   NETMAG.MAGID%TYPE,
                              p_magtype NETMAG.MAGTYPE%TYPE,
                              p_evtpref INTEGER,
                              p_bump INTEGER,
                              p_commit INTEGER,
                              p_check_priority INTEGER) RETURNS INTEGER AS $$
DECLARE
    -- init local variables
    v_pm_type NETMAG.MAGTYPE%TYPE  := NULL;
    v_pm_id   NETMAG.MAGID%TYPE    := 0;
    v_orid    ORIGIN.ORID%TYPE     := 0;
    v_prefor  EVENT.PREFOR%TYPE    := 0;
    v_status  INTEGER          := 0;
    v_eprefu  INTEGER          := 0;
    v_bump    INTEGER          := 0;
    v_rows  INTEGER         := 0;
BEGIN
    --
    -- Process data for EVENT table DML
    --
    -- BEGIN
      --
      IF (p_evid IS NULL OR p_magid IS NULL OR p_magtype IS NULL) THEN
        RETURN -1;
      END IF;
      -- RAISE NOTICE 'working on magtype %, magid %, for evid %', p_magtype, p_magid, p_evid; 
      --
      -- Is event preferred magnitude the same magtype as input? 
      -- get current prefmag magid and type into v_pm_id and v_pm_type vars
      SELECT coalesce(magid, 0), magtype INTO v_pm_id, v_pm_type FROM netmag
        WHERE magid = (SELECT prefmag FROM event WHERE evid = p_evid);
      --
      -- current prefmag may be different type
      IF (p_check_priority > 0 AND v_pm_id <> p_magid) THEN 
        v_status := MAGPREF.highest_priority(v_pm_id, p_magid); 
        -- RAISE NOTICE 'magpref.highest_priority(%, %) returns %', v_pm_id, p_magid, v_status;
        IF (v_status = v_pm_id) THEN
          v_status := -99;
        END IF;
      END IF;
      --
      IF (v_status <> -99) THEN
        -- Existing Event.prefmag is not a higher priority so it's ok to update
        -- EVENT table if input flag > 0 or prefmag magtype matches and magids do not match
        IF ( (p_evtpref > 0) OR ((v_pm_type = p_magtype) AND (v_pm_id <> p_magid)) ) THEN
          IF (v_pm_id <> p_magid) THEN -- update only if different -aww 2008/02/28
            UPDATE event SET prefmag = p_magid WHERE evid = p_evid RETURNING prefor INTO v_prefor;
            GET DIAGNOSTICS v_rows = ROW_COUNT;
            v_status := v_rows + 1;
            v_eprefu := v_status;
          ELSE -- just get prefor
            SELECT prefor INTO v_prefor FROM event WHERE evid = p_evid;
            v_status := 1;
          END IF;
        END IF;
      END IF;
      --
    -- RAISE NOTICE 'We got here, v_pm_id = %', v_pm_id;
    IF (v_pm_id = 0 OR v_pm_id IS NULL) THEN
        -- RAISE NOTICE 'No prior Event.prefmag, setting prefmag for %', p_evid;
      -- IF (v_pkg_debug > 0) THEN
      --   DBMS_OUTPUT.PUT_LINE('EPREF.setprefmag_magtype: No prior Event.prefmag , settting prefmag for evid: ' || p_evid );
      -- END IF;
      -- have no pref, an unusual state unless virgin
         IF (p_check_priority > 0) THEN
           v_status := MAGPREF.getMagPriority(p_magid);
           -- RAISE NOTICE 'MAGPREF.getMagPriority(%) returns %', p_magid, v_status;
           IF (v_status < 0) THEN
             -- IF (v_pkg_debug > 0) THEN 
             --  DBMS_OUTPUT.PUT_LINE('EPREF.setprefmag_magtype: input magid ' || p_magid || ' FAILED priority rule test, evid: ' || p_evid );
             -- END IF;
             v_status := -2; -- to flag mag priority rule failure
           END IF;
         ELSE
           v_status := 0;
         END IF;
         --
         IF (v_status >= 0) THEN --  priority >= 0, ok since MAGPREFPRIORITY rules allow 
           -- IF (v_pkg_debug > 0) THEN
           --   DBMS_OUTPUT.PUT_LINE('EPREF.setprefmag_magtype: input magid ' || p_magid || ' has highest priority=' || v_status || ', evid: ' || p_evid );
           -- END IF;
           UPDATE event SET prefmag = p_magid WHERE evid = p_evid
             RETURNING prefor INTO v_prefor;
           GET DIAGNOSTICS v_rows = ROW_COUNT;
           v_status := v_rows + 1;
           v_eprefu := v_status;
         ELSE -- priority < 0,  no MAGPREFPRIORITY table rule for this magnitude
           -- v_status := 0;  -- leave it be -aww 2010/01/14
           RETURN v_status; -- BAIL
         END IF;
    END IF;
    --
    IF (v_status > 1 AND p_bump > 0) THEN
      v_bump := 1; -- flag to bump 
    END IF;
    --
    -- Process data for ORIGIN table DML
    --
    -- BEGIN
      -- reset local variables for reuse
      v_pm_type := NULL;
      v_pm_id := 0; 
      --
      -- get value of Origin.prefmag for the input Netmag.orid 
      SELECT coalesce(orid,0), coalesce(prefmag, 0) INTO v_orid, v_pm_id FROM origin
        WHERE orid = (SELECT orid FROM netmag WHERE magid = p_magid);
      --
      --
     -- if Origin.prefmag is not null and not p_magid get magtype for Origin.prefmag
      IF ( (v_pm_id > 0) AND (v_pm_id <> p_magid) ) THEN
        SELECT magtype INTO v_pm_type FROM netmag WHERE magid = v_pm_id;
      END IF;
      --
      -- current prefmag may be different type
      IF (p_check_priority > 0 AND v_pm_id > 0 AND v_pm_id <> p_magid) THEN
        v_status := MAGPREF.highest_priority(v_pm_id, p_magid); 
        IF (v_status = v_pm_id) THEN
          -- IF (v_pkg_debug > 0) THEN
          --   DBMS_OUTPUT.PUT_LINE('EPREF.setprefmag_magtype of origin ' || p_magtype || ' is no-op, old prefmag: ' || v_pm_id ||
          --                        ' has higher priority than magid: '  || p_magid);
          -- END IF;
          v_status := -99;
        END IF;
      END IF;
      --
      IF (v_status <> -99) THEN
        -- Existing Origin.prefmag is not a higher priority so it's ok to update
        -- Origin table if prefmag is null or magtype matches and magids do not match,
        -- or it's flagged event preferred and its associated origin is the event.prefor. 
        IF ( (v_pm_id <= 0) OR (v_pm_type = p_magtype AND v_pm_id <> p_magid) OR (p_evtpref > 0  AND v_orid = v_prefor) ) THEN
          IF (v_pm_id <> p_magid) THEN -- update only if different -aww 2008/02/28
            -- IF (v_pkg_debug > 0) THEN 
            --   DBMS_OUTPUT.PUT_LINE('EPREF.setprefmag_magtype updating origin prefmag, input magid: ' || p_magid || ' existing prefmag: '  || v_pm_id );
            -- END IF;
            UPDATE origin SET prefmag = p_magid WHERE orid = v_orid;
            GET DIAGNOSTICS v_rows = ROW_COUNT;
            v_status := v_rows;
          ELSE
           -- IF (v_pkg_debug > 0) THEN
           --   DBMS_OUTPUT.PUT_LINE('EPREF.setprefmag_magtype: Origin.prefmag unchanged, same as input magid: ' ||  p_magid);
           -- END IF;
            v_status := 1; -- already set
          END IF;
        END IF;
      END IF;
      --
    -- END
    --EXCEPTION WHEN NO_DATA_FOUND THEN
    IF (v_orid = 0) THEN
      -- DBMS_OUTPUT.PUT_LINE('EPREF.setprefmag_magtype: ERROR, Origin row, or Netmag row is missing for input magid: ' || p_magid );
      -- Don't return, keep going we need to update the eventprefmag table still 
      -- RETURN -1; -- no netmag row for input magid or db is missing its associated origin row 
    END IF;
    --
    -- Process data for EVENTPREFMAG table DML
    -- -- does commit action by flag
    v_status := epref.update_eventprefmag(p_evid, p_magid, p_magtype, 0, p_check_priority); 
    -- RAISE NOTICE 'after epref.update_eventprefmag(%, %, %, 0, %) v_status = %', p_evid, p_magid, p_magtype, p_check_priority, v_status;
    IF (v_status = 9999) THEN 
      v_status := 0;
    ELSIF ( v_status > 1 ) THEN 
      -- to discriminate from v_eprefu return value
      v_status := 1;
    END IF;
    -- Return result of updating event prefmag -- aww 2014/05/15
    IF ( v_eprefu >= 2 ) THEN
        v_status := v_eprefu;    
    END IF;
    --
    IF (v_bump > 0 AND p_bump > 0) THEN 
       v_bump := epref.bump_version(p_evid, 0);
    END IF;
    --
    IF (p_commit > 0) THEN
      -- COMMIT; not allowed in postgresql
      -- IF (v_pkg_debug > 0) THEN
      --    DBMS_OUTPUT.PUT_LINE('EPREF.setprefmag_magtype COMMIT for evid: '  || p_evid);
      -- END IF;
    END IF;

    --
    RETURN v_status;
    --
END
$$ LANGUAGE plpgsql;
-- ---------------------------------------------------------------------------


create or replace FUNCTION epref.update_eventprefor( p_evid Event.evid%TYPE,
                               p_orid Origin.orid%TYPE,
                               p_otype Origin.type%TYPE,
                               p_commit INTEGER,
                               p_check_priority INTEGER) RETURNS INTEGER AS $$
DECLARE
    -- reset local variables for reuse
    v_prefor Origin.orid%TYPE := 0;
    v_orid Origin.orid%TYPE := 0;
    v_status  INTEGER := 0;
    --
BEGIN
      --
      IF (p_evid IS NULL OR p_orid IS NULL OR p_otype IS NULL) THEN
         -- IF (v_pkg_debug > 0) THEN
         --   DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefor NULL input FAILURE p_evid: ' || p_evid || ' p_orid: ' || p_orid || ' p_otype:' || p_otype);
         -- END IF;
         RETURN 0;
      END IF;
      --
      -- Get currently set orid for input origin type and input evid
      SELECT coalesce(orid, 0) INTO v_prefor FROM eventprefor WHERE evid = p_evid AND type = p_otype;
      --
      IF (p_check_priority > 0 AND v_prefor <> p_orid) THEN
        -- Get highest priority origin of same type for the associated origin  
        --v_orid := ORGPREF.bestOridForOriginPref(p_evid, p_otype);  -- Best of all origins for type in origin table
	-- TODO: paulf not implementing ORGPREF just yet, calling this pref or for now
        --v_orid := ORGPREF.highest_priority(v_prefor, p_orid);
        -- next line is new just, in case we do implement orgpref, remove next line and uncomment above line
        p_orid := v_prefor;
        IF (p_orid <> v_orid) THEN
          -- IF (v_pkg_debug > 0) THEN
          --   DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefor type ' || p_otype ||
          --         ' is no-op, highest priority orid for origin: ' || v_orid || ' is not the input orid: '  || p_orid);
          -- END IF;
          RETURN 9999;
        END IF;
      END IF;
      --
      -- Here if row exists so
      -- Update EVENTPREFOR table row if its orid does not match input.
      IF (p_orid <> v_prefor) THEN -- update only if different
        UPDATE eventprefor SET orid = p_orid
          WHERE evid = p_evid AND type=p_otype;
        -- IF (v_pkg_debug > 0) THEN 
        --   DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefor: updated for evid: ' || p_evid || ' orid: ' || p_orid || ' otype: ' || p_otype);
        -- END IF;
	GET DIAGNOSTICS v_status = ROW_COUNT;
        v_status := v_status + 1;
      ELSE
        -- IF (v_pkg_debug > 0) THEN
        --    DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefor no-op, origin for type:' || p_otype || ' already is input p_orid: ' || p_orid );
        -- END IF;
        v_status := 1; -- already set 
      END IF;
      --
    --EXCEPTION WHEN NO_DATA_FOUND THEN -- Row D.N.E. so create new EVENTPREFOR table row
    IF (v_prefor = 0) THEN
      IF (p_check_priority > 0) THEN -- first check for valid priority
        -- v_status := ORGPREF.getOriginPriority(p_orid);
        -- IF (v_status < 0) THEN
        --   v_status := -2; -- to flag priority rule failure
        --   IF (v_pkg_debug > 0) THEN 
        --     DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefor for orid,otype: ' || p_orid || ',' || p_otype || ' FAILED priority rule test, evid: ' || p_evid );
        --   END IF;
        -- END IF;
        v_status := 0;
      ELSE
        v_status := 0;
      END IF;
      IF (v_status >= 0) THEN
        INSERT INTO eventprefor (evid,type,orid) VALUES (p_evid, p_otype, p_orid);
        -- IF (v_pkg_debug > 0) THEN 
        --   DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefor: insert for evid: ' || p_evid || 'otype: ' || p_otype || ' orid: ' || p_orid);
        -- END IF;
	GET DIAGNOSTICS v_status = ROW_COUNT;
        v_status := v_status + 1;
      END IF;
    END IF;
    --
    IF (p_commit > 0 AND v_status > 1) THEN  -- an update or insert in db tables
      --COMMIT; not allowed in postgresql
      -- IF (v_pkg_debug > 0) THEN 
      --   DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefor: COMMIT for evid: ' || p_evid || 'otype: ' || p_otype || ' orid: ' || p_orid);
      -- END IF;
    END IF;
    --
    RETURN v_status;
    --
END
$$ LANGUAGE plpgsql;


create or replace   FUNCTION epref.update_eventprefmag(p_evid Event.evid%TYPE,
                               p_magid Netmag.magid%TYPE,
                               p_magtype Netmag.magtype%TYPE,
                               p_commit INTEGER,
                               p_check_priority INTEGER) RETURNS INTEGER AS $$

DECLARE
    --
    -- reset local variables for reuse
    v_magid NETMAG.magid%TYPE := 0;
    v_prefmag NETMAG.magid%TYPE := 0;
    v_orid Origin.orid%TYPE := 0;
    v_status  INTEGER := 0;
    v_rows  INTEGER := 0;
    --
BEGIN
    -- BEGIN
      --
      IF (p_evid IS NULL OR p_magid IS NULL OR p_magtype IS NULL) THEN
      --   IF (v_pkg_debug > 0) THEN
      --     DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefmag NULL input FAILURE p_evid: ' || p_evid || ' p_magid: ' || p_magid || ' p_magtype:' || p_magtype);
      --   END IF;
         RETURN 0;
      END IF;
      --
      -- Get orid associated with the input magnitude
      -- BEGIN
        SELECT coalesce(orid, 0) INTO v_orid FROM netmag WHERE magid = p_magid;
        IF (v_orid = 0) THEN
           -- DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefmag FAILED, no Netmag row found for input p_magid: ' || p_magid );
           RAISE NOTICE 'EPREF.update_eventprefmag FAILED, no Netmag row found for input p_magid: %', p_magid;
           RETURN 0;
        END IF;
      -- END;
      --
      -- Get currently set magid for input magtype and input evid
      SELECT coalesce(magid, 0) INTO v_prefmag FROM eventprefmag WHERE evid = p_evid AND magtype = p_magtype;
      --
      IF (p_check_priority > 0 AND v_prefmag <> p_magid AND v_prefmag is not NULL) THEN
        /* REMOVED, only compare magnitudes from same origin not different origins, see below -aww
        v_status := MAGPREF.highest_priority(v_prefmag, p_magid);
        IF (v_status = v_prefmag) THEN
          IF (v_pkg_debug > 0) THEN DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefmag type ' || p_magtype || ' is no-op, existing magid is higher priority : ' |
| v_prefmag || ' input magid: '  || p_magid); END IF;
          RETURN 9999;
        END IF;
        */
        -- Get highest priority magnitude of same magtype for the associated origin
        -- v_magid := MAGPREF.bestMagidForOriginPref(v_orid, p_magtype); -- fix below -aww 2012/11/20
        v_magid := MAGPREF.highest_priority(v_prefmag, p_magid);
        -- RAISE NOTICE 'MAGPREF.highest_priority(%, %) returns v_mag_id=%', v_prefmag, p_magid, v_magid;
        --
        IF (p_magid <> v_magid) THEN
          -- IF (v_pkg_debug > 0) THEN
          --   DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefmag type ' || p_magtype ||
          --        ' is no-op, highest priority magid for origin: ' || v_magid || ' is not the input magid: '  || p_magid);
          -- END IF;
          RETURN 9999;
        END IF;
      END IF;
      --
      -- Here if row exists so
      -- Update EVENTPREFMAG table row if its magid does not match input.
      IF (p_magid <> v_prefmag) THEN -- update only if different - aww 2008/02/28
        UPDATE eventprefmag SET magid = p_magid
           -- , lddate=SYS_EXTRACT_UTC(SYSTIMESTAMP) -- removed lddate 02/22/2007 aww
          WHERE evid = p_evid AND magtype=p_magtype;
        -- IF (v_pkg_debug > 0) THEN
        --   DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefmag: updated for evid: ' || p_evid || ' magid: ' || p_magid || ' magtype: ' || p_magtype);
        -- END IF;
        GET DIAGNOSTICS v_status = ROW_COUNT;
        v_status := v_status + 1;
      ELSE
        -- IF (v_pkg_debug > 0) THEN
        --    DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefmag no-op, prefmag for type:' || p_magtype || ' already is input magid: ' || p_magid );
        -- END IF;
        v_status := 1; -- already set
      END IF;
      --
    --EXCEPTION WHEN NO_DATA_FOUND THEN -- Row D.N.E. so create new EVENTPREFMAG table
    IF (V_PREFMAG IS NULL) THEN
      IF (p_check_priority > 0) THEN -- first check for valid priority
        v_status := MAGPREF.getMagPriority(p_magid);
        IF (v_status < 0) THEN
          v_status := -2; -- to flag mag priority rule failure
          -- IF (v_pkg_debug > 0) THEN
          --   DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefmag for magid,magtype: ' || p_magid || ',' || p_magtype || ' FAILED priority rule test, evid: ' || p_evid );
          -- END IF;
        END IF;
      ELSE
        v_status := 0;
      END IF;
      IF (v_status >= 0) THEN
        INSERT INTO eventprefmag (evid,magtype,magid) VALUES (p_evid, p_magtype, p_magid);
        -- IF (v_pkg_debug > 0) THEN
        --   DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefmag: insert for evid: ' || p_evid || 'magtype: ' || p_magtype || ' magid: ' || p_magid);
        -- END IF;
        GET DIAGNOSTICS v_rows = ROW_COUNT;
        v_status := v_rows + 1;
      END IF;
    END IF;
    --
    IF (p_commit > 0 AND v_status > 1) THEN  -- an update or insert in db tables
      --COMMIT; not allowed in postgresql
      -- IF (v_pkg_debug > 0) THEN
      --   DBMS_OUTPUT.PUT_LINE('EPREF.update_eventprefmag: COMMIT for evid: ' || p_evid || 'magtype: ' || p_magtype || ' magid: ' || p_magid);
      -- END IF;
    END IF;
    --
    RETURN v_status;
    --
END 
$$ LANGUAGE plpgsql;

-- Updates or inserts a credit table row whose fields match input values.
-- Commits transaction if successful.
-- Returns attribution id on success, null otherwise.
CREATE OR REPLACE FUNCTION epref.attribute(p_id BIGINT, p_tablename VARCHAR(30), p_reference VARCHAR(80)) RETURNS VARCHAR(80) AS $$
BEGIN
    RETURN epref.attribute(p_id, p_tablename, p_reference, 1);
END;
$$ LANGUAGE plpgsql;

-- ---------------------------------------------------------------------------
--
-- Updates or inserts a credit table row whose fields match input values.
-- Commits transaction if successful and p_commit > 0.
-- Returns attribution id on success, null otherwise.
CREATE OR REPLACE FUNCTION epref.attribute(p_id BIGINT, p_tablename VARCHAR(30), p_reference VARCHAR(80), p_commit INTEGER) RETURNS VARCHAR(80) AS $$
  DECLARE
    v_id VARCHAR(80) := NULL;
    v_count INTEGER;
  BEGIN
    select keyid into v_id from credit_alias where lower(alias) = lower(p_reference);
    GET DIAGNOSTICS v_count=ROW_COUNT;
    IF (v_count = 0) THEN    
         v_id := p_reference;
    END IF;
    --
    INSERT INTO credit (id, tname, refer) VALUES (p_id, p_tablename, v_id);
    GET DIAGNOSTICS v_count=ROW_COUNT;
    IF (v_count > 0 AND p_commit > 0) THEN
      --COMMIT; not allowed in postgresql
    END IF;
    RETURN v_id;
    --
  EXCEPTION 
     WHEN UNIQUE_VIOLATION THEN
       update credit set refer=v_id where id=p_id and tname=p_tablename;
       GET DIAGNOSTICS v_count=ROW_COUNT;
       IF (v_count > 0 AND p_commit > 0) THEN
         --COMMIT; not allowed in postgresql
       END IF;
       RETURN v_id;
     WHEN OTHERS THEN
        RAISE NOTICE USING MESSAGE='EXCEPTION epref.attribute('|| p_id ||','||p_tablename||','||p_reference||')'
                         ||' CODE: ' || TO_CHAR(SQLCODE);
       RETURN NULL;
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS epref.finalize_event(p_evid EVENT.EVID%TYPE, p_bump INTEGER);
-- Set Origin.rflag='F'.
-- Posts evid 'TPP.TPP.FINAL.100'.
-- Bumps event version if new rflag value, commits changes.
-- Return value = 1 rflag set to 'F', = 2 already posted, =3 successful new post, <= 0 an error.
-- Example: call EPREF.finalize_event(1111);
CREATE OR REPLACE FUNCTION epref.finalize_event(p_evid EVENT.EVID%TYPE, p_bump INTEGER) RETURNS BIGINT AS $$
  DECLARE
    v_status INTEGER := 0;
    v_istat BIGINT := 0;
  BEGIN
    -- Set 'rflag' = "F", does bump_version if rflag changed
    v_status = epref.setRflag(p_evid, 'F', p_bump);
    RAISE NOTICE 'updated review flag of origin v_status=%', v_status;
    -- POST event to 'FINALIZE' for alarm processing, etc.
    IF (v_status > 0) THEN -- already set or a changed flag, either case
      v_istat := 1;
      v_status := PCS.POST_ID('TPP', 'TPP', p_evid, 'FINALIZE', 100);
      RAISE NOTICE 'posted to PCS_STATE, return value: %', v_status;
      IF (v_status >= 0) THEN -- posted or already posted
        v_istat := v_istat + 1;
      END IF;
    END IF;
    --
    RAISE NOTICE 'INFO: return value of finalize_event is %', v_istat;
    RETURN v_istat;
    --
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS epref.finalize_event(p_evid Event.evid%TYPE);
CREATE OR REPLACE FUNCTION epref.finalize_event(p_evid Event.evid%TYPE) RETURNS BIGINT AS $$
  BEGIN
      RETURN epref.finalize_event(p_evid, 1);
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS epref.delete_event(p_evid Event.evid%TYPE);
-- Does NOT actually remove the record, just sets selectflag = 0
-- and runs epref.cancel_event (posts to PCS system).
CREATE OR REPLACE FUNCTION epref.delete_event(p_evid Event.evid%TYPE) RETURNS BIGINT AS $$
  DECLARE
    v_status BIGINT := 0;
    v_flag event.selectflag%TYPE := NULL;
  BEGIN
    SELECT COALESCE(selectflag, -1) INTO v_flag FROM event WHERE evid = p_evid;
    IF (v_flag <> 0) THEN
       -- update only when not 0 already
       UPDATE event SET selectflag = 0 WHERE evid = p_evid;
       GET DIAGNOSTICS v_status = ROW_COUNT;
    ELSE
        -- already set to 0
        v_status := 1;
    END IF;

    -- POST to generate alarm delete/cancel messages
    IF (v_status > 0) THEN
      v_status := v_status + epref.cancel_event(p_evid);
    END IF;

    RETURN v_status;

  EXCEPTION WHEN NO_DATA_FOUND THEN
    RETURN -1;
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS epref.cancel_event(p_evid Event.evid%TYPE);
-- Just posts to state that causes cancellation messages to be sent.
CREATE OR REPLACE FUNCTION epref.cancel_event(p_evid Event.evid%TYPE) RETURNS BIGINT AS $$
  DECLARE
    v_status BIGINT := 0;
  BEGIN
--   
    -- POST for alarm processing
    v_status := PCS.POST_ID('TPP', 'TPP', p_evid, 'DELETED', 100);
    --
    IF (v_status > 0) THEN
      v_status := epref.setRFlag(p_evid, 'C');
    END IF;
    --
    RETURN v_status;
    --
END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS epref.uncancel_event(p_evid Event.evid%TYPE);
CREATE OR REPLACE FUNCTION epref.uncancel_event(p_evid Event.evid%TYPE) RETURNS BIGINT AS $$
  DECLARE
      v_status BIGINT := 0;
      v_rflag  VARCHAR(1) := 'H';
  BEGIN
    --
    SELECT o.rflag INTO v_rflag FROM origin o, event e WHERE o.orid=e.prefor AND e.evid = p_evid;
    --
    -- purge existing action_states for evid that may block pending alarming
    UPDATE alarm_action set action_state='!CANCELLED' WHERE event_id = p_evid AND action_state LIKE 'CANCEL%';
    --
    GET DIAGNOSTICS v_status = ROW_COUNT;
    --
    IF ( v_rflag = 'C' OR v_rflag = 'A' ) THEN
      v_status := epref.accept_event(p_evid);
    END IF;
    --
    RETURN v_status;
    --
  EXCEPTION WHEN OTHERS THEN
      RETURN -1;
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS epref.accept_event(p_evid Event.evid%TYPE);
CREATE OR REPLACE FUNCTION epref.accept_event(p_evid Event.evid%TYPE)  RETURNS BIGINT AS $$
  DECLARE
    v_status BIGINT := 0;
    v_orid Origin.orid%TYPE := NULL;
    v_flag Origin.rflag%TYPE := NULL;
  BEGIN
    --
    IF (p_evid IS NULL OR p_evid < 1) THEN
      RETURN 0;
    END IF;
    --
    SELECT event.prefor, COALESCE(origin.rflag,'$') INTO v_orid, v_flag FROM event LEFT OUTER JOIN
        origin ON event.prefor = origin.orid WHERE event.evid = p_evid; 
    --
    -- Should generate alarm-type update actions
    IF (v_flag <> 'H') THEN -- update only if different -aww 2008/02/28
      UPDATE origin SET rflag = 'H' WHERE orid = v_orid;
      --
      GET DIAGNOSTICS v_status = ROW_COUNT;
      --
      IF (v_status > 0) THEN
        v_status := epref.bump_version(p_evid); -- due to rflag change
        IF ( v_status > 0 ) THEN
            v_status := v_status + 1;
        END IF;
      END IF;
      --
    ELSE
      v_status := 1; -- already set
    END IF;
    --
    RETURN v_status;
    --
  EXCEPTION WHEN NO_DATA_FOUND THEN
    RETURN -1;
  END;
$$ LANGUAGE plpgsql;

-- Only sets Event.selectflag=1, Origin.rflag='H'.
DROP FUNCTION IF EXISTS epref.accept_trigger(p_evid Event.evid%TYPE);
CREATE OR REPLACE FUNCTION epref.accept_trigger(p_evid Event.evid%TYPE) RETURNS INTEGER AS $$
  DECLARE
    v_status INTEGER := 0;
    v_select Event.selectflag%TYPE := NULL;
    v_orid Origin.orid%TYPE := NULL;
    v_flag Origin.rflag%TYPE := NULL;
  BEGIN
    --
    IF (p_evid IS NULL) THEN
      RETURN 0;
    END IF;
    --
    SELECT coalesce(prefor, 0), coalesce(selectflag, -1), coalesce(rflag, '$') INTO v_orid, v_select, v_flag
        FROM event LEFT OUTER JOIN origin ON event.prefor = origin.orid WHERE event.evid = p_evid;
    --
    IF (v_select <> 1) THEN -- update only if different -aww 2008/02/28
      UPDATE event Set selectFlag = 1 WHERE evid = p_evid;
      GET DIAGNOSTICS v_status = ROW_COUNT;
      v_status := v_status + 1;
    ELSE
      v_status := 1; -- already set
    END IF;
    --
    IF (v_orid > 0 AND v_flag <> 'H') THEN -- update only if different -aww 2008/02/28
      UPDATE origin SET rflag = 'H' WHERE orid = v_orid; --  origin.bogusflag = 0
      GET DIAGNOSTICS v_status = ROW_COUNT;
      v_status := v_status + 2;
    ELSE
      v_status := 1; -- already set
    END IF;
    --
    IF (v_status > 2) THEN
      --does commit
      v_status := epref.bump_version(p_evid); -- due to rflag change
    END IF;
    --
    RETURN v_status;
    --
END
$$ language plpgsql;

-- Set Origin.rflag to input value where orid equals the prefor of input Event.evid.
-- Bumps event version if new rflag value, commits changes.
-- Returns value > 0 on success, <= 0 if not.
-- Example: call EPREF.setRflag(1111, 'F');
CREATE OR REPLACE FUNCTION epref.setRflag(p_evid Event.evid%TYPE, p_rflag Origin.rflag%TYPE) RETURNS INTEGER AS $$
  BEGIN
      RETURN epref.setRflag(p_evid, p_rflag, 1 );
  END;
$$ LANGUAGE plpgsql;

-- Like above, but bumps the event version only if p_bump>0.
CREATE OR REPLACE FUNCTION epref.setRflag(p_evid Event.evid%TYPE, p_rflag Origin.rflag%TYPE, p_bump INTEGER) RETURNS INTEGER AS $$
  DECLARE
    v_status INTEGER := 0;
    v_orid Event.prefor%TYPE := NULL;
    v_flag VARCHAR(1) := NULL;
    v_version INTEGER := 0;
  BEGIN
    --
    IF (p_evid IS NULL OR p_rflag IS NULL) THEN
      RETURN 0;
    END IF;
    --
    SELECT event.prefor, coalesce(event.version,0), coalesce(origin.rflag,'$') INTO v_orid, v_version, v_flag 
    FROM event
    LEFT OUTER JOIN origin
    ON event.prefor=origin.orid 
    WHERE event.evid=p_evid;

    RAISE NOTICE USING MESSAGE='in setRflag: current version: ' || v_orid || '-' || v_version || '-' || v_flag;

    -- only update if requested rflag is different
    IF (v_orid IS NOT NULL AND p_rflag <> v_flag) THEN
        UPDATE origin SET rflag = p_rflag WHERE orid = v_orid;
        GET DIAGNOSTICS v_status = ROW_COUNT;

        RAISE NOTICE 'updated origin table, v_status: %', v_status;

        IF (p_bump > 0 AND v_status > 0) THEN
            v_status := epref.bump_version(p_evid);
        END IF;
        -- return latest version number 
        IF ( v_status > 0) THEN
            v_status := v_version + 1;
        END IF;
    ELSE
        -- rflag already set to that value
        v_status = 1;
    END IF;

    RETURN v_status;

    EXCEPTION WHEN NO_DATA_FOUND THEN
      -- no evid matches input or event row has a null prefor.
      return -1;
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS epref.cloneAssocWaE(p_evidOld Event.evid%TYPE, p_evidNew Event.evid%TYPE);
-- Create AssocWaE records the new event for all WaveForms that are
-- associated with the old event. Note: evid must exist.
-- Commmits the changes.
CREATE OR REPLACE FUNCTION epref.cloneAssocWaE(p_evidOld Event.evid%TYPE, p_evidNew Event.evid%TYPE) RETURNS BIGINT AS $$
  BEGIN
    --
    RETURN epref.cloneAssocWaE(p_evidOld, p_evidNew, 1);
    --
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS epref.cloneAssocWaE(p_evidOld Event.evid%TYPE,
                          p_evidNew Event.evid%TYPE,
                          p_commit INTEGER);
-- Create AssocWaE records the new event for all WaveForms that are
-- associated with the old event. Note: evid must exist.
-- Always commits, even if p_commit = 0
CREATE OR REPLACE FUNCTION epref.cloneAssocWaE(p_evidOld Event.evid%TYPE,
                          p_evidNew Event.evid%TYPE,
                          p_commit INTEGER) RETURNS BIGINT AS $$
  DECLARE
    -- Need cursor loop to handle DUP_VAL_ON_INDEX exception (existing rows) upon insert -aww
    c CURSOR FOR SELECT wfid, datetime_on, datetime_off FROM AssocWaE WHERE evid = p_evidOld;
    --
    v_status BIGINT := 0;
    v_tmp BIGINT := 0;
  BEGIN
    --
    IF (p_evidOld = p_evidNew) THEN
      RETURN 0; -- noop
    END IF;
    --
    FOR r IN c LOOP -- process existing rows
      BEGIN
          INSERT INTO AssocWaE (evid, wfid, datetime_on, datetime_off)
            VALUES (p_evidNew, r.wfid, r.datetime_on, r.datetime_off);
      EXCEPTION WHEN UNIQUE_VIOLATION THEN
          UPDATE AssocWaE SET
          datetime_on = r.datetime_on, datetime_off = r.datetime_off
          WHERE evid = p_evidNew and wfid = r.wfid;
      END;
      GET DIAGNOSTICS v_tmp = ROW_COUNT;
      v_status := v_status + v_tmp;
    END LOOP;
    --
    RETURN v_status;
    --
  END;
$$ LANGUAGE plpgsql;

-- Create AssocWaE record for an existing WaveForm and pointing at this event.
-- Note: input evid must define existing event table row key.
-- Test : select * from assocwae where wfid = 148530;
--        call EPREF.AssociateWaveform(148530, 1);
CREATE OR REPLACE FUNCTION epref.associateWaveform(p_wfid AssocWaE.wfid%TYPE, p_evid Event.evid%TYPE) RETURNS INTEGER AS $$
  DECLARE
    --
    c CURSOR FOR SELECT datetime_on, datetime_off FROM Waveform WHERE wfid = p_wfid;
    r RECORD;
    v_status INTEGER := 0;
    --
  BEGIN
    OPEN c;
    FETCH c INTO r;
    --
    IF (FOUND) THEN  -- we have waveform row
      BEGIN
        --
        INSERT INTO AssocWaE (evid, wfid, datetime_on, datetime_off)
          VALUES (p_evid, p_wfid, r.datetime_on, r.datetime_off);
        --
      EXCEPTION WHEN UNIQUE_VIOLATION THEN  -- try a UPDATE (aww added)
        --
        UPDATE AssocWaE SET
        datetime_on = r.datetime_on, datetime_off = r.datetime_off
        WHERE evid = p_evid and wfid = p_wfid;
        --
      END;
      --
      GET DIAGNOSTICS v_status = ROW_COUNT;
      --
    END IF;
    --
    CLOSE c;
    --
    RETURN v_status;
    --
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS epref.merge(p_evidm Event.evid%TYPE, p_evids Event.evid%TYPE);
-- Set prefor, prefmag, and prefmec for Event row whose key equals p_evidm
-- to the values found for row whose key equals p_evids
-- invokes delete_event(p_evids)
-- Commits transaction if successful.
CREATE OR REPLACE FUNCTION epref.merge(p_evidm Event.evid%TYPE, p_evids Event.evid%TYPE) RETURNS INTEGER AS $$
  DECLARE
    v_orid  BIGINT := 0;
    v_magid BIGINT := 0;
    v_mecid BIGINT := 0;
    v_status INTEGER := 0;
  BEGIN
    -- get slave info
    SELECT prefor, prefmag, prefmec INTO v_orid, v_magid, v_mecid FROM event WHERE evid = p_evids;
    -- Set the prefor and prefmag of the master to that of the slave
    v_status := epref.setPref(p_evidm, v_orid, v_magid, v_mecid);
    -- Delete the slave
    v_status := epref.delete_event(p_evids);
    --
    RETURN v_status;
    --
  EXCEPTION WHEN NO_DATA_FOUND THEN
      RAISE NOTICE 'WARNING : EPREF.merge(...) NO_DATA_FOUND for input p_evids: %', p_evids;
      RETURN -1;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION epref.setPref(p_evid Event.evid%TYPE, p_orid Origin.orid%TYPE, p_magid Netmag.magid%TYPE) RETURNS INTEGER AS $$
  BEGIN
      RETURN epref.setPref(p_evid, p_orid, p_magid, NULL);
  END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION epref.setPref(p_evid Event.evid%TYPE,
                   p_orid Origin.orid%TYPE,
                   p_magid Netmag.magid%TYPE,
                   p_mecid Mec.mecid%TYPE) RETURNS INTEGER AS $$
  DECLARE
    v_status INTEGER := 0;
    --
    v_prefor  Event.prefor%TYPE := NULL;
    v_otype   Origin.type%TYPE := NULL;
    --
    v_prefmag Event.prefmag%TYPE := NULL;
    v_magtype Netmag.magtype%TYPE := NULL;
    --
    v_prefmec Mec.mecid%TYPE := NULL;
    v_mechtype Mec.mechtype%TYPE := NULL;
    --
    v_bump BOOLEAN := FALSE;
  --
  BEGIN
    --
    IF (p_evid IS NULL) THEN
      RETURN -1;
    END IF;
    --
    -- Foreign key constraints on EVENT ensure origin and netmag row existance
    -- Could also check here that the Origin.evid and Netmag.orid => Origin.evid match
    -- just in case the user entered a wrong id (that of a different event).  -aww
    -- DECLARE
    --   v_evido ORIGIN.evid%TYPE := 0;
    --   v_evidn ORIGIN.evid%TYPE := 0;
    -- BEGIN
    --   SELECT o.evid INTO v_evido FROM origin o WHERE o.orid=p_orid;
    --   SELECT o.evid INTO v_evidn FROM origin o WHERE
    --       o.orid=(SELECT n.orid FROM netmag n where n.magid=p_magid);
    --   IF (p_evid <> v_evido OR p_evid <> v_evidn) THEN  NULL; END IF;
    -- END;
    --
    -- First update the Event.prefor orid reference, if input parameter value is valid
    SELECT COALESCE(prefor, 0), COALESCE(prefmag, 0), COALESCE(prefmec, 0) INTO v_prefor, v_prefmag, v_prefmec FROM event WHERE evid = p_evid;
      --
    -- proceed only if next input parameter value is valid, else no-op
    IF (p_orid IS NOT NULL AND p_orid > 0) THEN
      IF (v_prefor <> p_orid) THEN -- update only if different -aww 2008/02/28
        SELECT type INTO v_otype FROM origin WHERE orid = p_orid;
        IF (v_otype IS NULL) THEN  -- failure abort
              RAISE NOTICE 'EPREF.setPref: Check Origin table, type attribute NULL for input orid: %', p_orid;
          IF (v_bump) THEN
            v_status := epref.bump_version(p_evid); -- prefor changed,  bumped and commited
          END IF;
          RETURN -1;
        END IF;
        --
        v_status := epref.setprefor_type(p_evid, p_orid, v_otype, 1, 0, 0, 1);
        IF (v_status > 0 AND v_status <> 9999) THEN
          v_bump := TRUE;
        END IF;
        --
      ELSE
        v_status := 1; -- already set
      END IF;
      --
    END IF;
    --
   -- proceed only if next input parameter value is valid, else no-op
   IF (p_magid IS NOT NULL AND p_magid > 0) THEN
      --
      -- check for a NULL magtype value (redundant here Netmag table constraint is not null)
      IF (v_prefmag <> p_magid) THEN
        SELECT magtype INTO v_magtype FROM netmag WHERE magid = p_magid;
        --
        IF (v_magtype IS NULL) THEN  -- failure abort
          RAISE NOTICE 'EPREF.setPref: Check Netmag table, magtype attribute NULL for input magid: %', p_magid;
          IF (v_bump) THEN
            v_status := epref.bump_version(p_evid); -- prefor changed,  bumped and commited
          END IF;
          RETURN -1;
        END IF;
        --
        v_status := epref.setprefmag_magtype(p_evid, p_magid, v_magtype, 1, 0, 0, 1);
        IF (v_status > 0 AND v_status <> 9999) THEN
          v_bump := TRUE;
        END IF;
        --
      ELSE
        v_status := 1; -- already set
      END IF;
    END IF;
    --
    -- proceed only if next input parameter value is valid, else no-op
    IF (p_mecid IS NOT NULL AND p_mecid > 0) THEN
      --
      -- Input is a non-null and valid mecid
      IF (p_mecid <> v_prefmec) THEN
        SELECT mechtype INTO v_mechtype FROM mec WHERE mecid = p_mecid;
        -- check for a NULL mechtype value (Mec mechtype has no constraint )
        IF (v_mechtype IS NULL) THEN  -- failure abort
          RAISE NOTICE 'EPREF.setPref: Check Mec table, mechtype missing for input mecid: %', p_mecid;
          IF (v_bump) THEN
            v_status := epref.bump_version(p_evid); -- prefor and/or prefmag changed, bumped and commited
          END IF;
          RETURN -1;
        END IF;
        --
        v_status := epref.setprefmec_mechtype(p_evid, p_mecid, v_mechtype, 1, 0, 0); -- event.prefmec
        --
        IF (v_status > 0) THEN
          v_bump := TRUE;
        END IF;
        --
      ELSE
        v_status := 1; -- already set
      END IF;
    END IF;
    --
    IF (v_bump) THEN -- a pref id was changed
        v_status := epref.bump_version(p_evid);
    END IF;
    --
    RETURN v_status;
    --
  EXCEPTION WHEN NO_DATA_FOUND THEN
    RAISE NOTICE USING MESSAGE = 'EPREF.setPref: Check tables, missing row evid:' ||
                          p_evid || ' orid: ' || p_orid || ' magid: ' || p_magid ||' mecid: ' || p_mecid;
    RETURN -1;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION epref.setprefmec_mechtype(p_evid Event.evid%TYPE, 
  p_mecid Mec.mecid%TYPE, p_evtpref INTEGER,p_bump INTEGER, p_commit INTEGER) RETURNS INTEGER AS $$
  DECLARE
    v_prefmec Event.prefmec%TYPE := NULL;
    v_mechtype Mec.mechtype%TYPE := NULL;
  BEGIN
    IF (p_evid IS NULL OR p_mecid IS NULL OR p_mecid < 1) THEN
      RETURN -1;
    END IF;
    --
    SELECT mechtype INTO v_mechtype FROM mec WHERE mecid = p_mecid;
    --
    -- check for a NULL mechtype value (redundant here table constraint is not null)
    IF (v_mechtype IS NULL) THEN
      RAISE NOTICE 'EPREF.setprefmec_mechtype: Check mec table, mechtype missing for input mecid: %', p_mecid;
      RETURN -1;
    END IF;
    --
    RETURN epref.setprefmec_mechtype(p_evid, p_mecid, v_mechtype, p_evtpref, p_bump, p_commit);
    --
  EXCEPTION WHEN NO_DATA_FOUND THEN
    RETURN -1;
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS epref.setprefmec_mechtype(p_evid    EVENT.EVID%TYPE,
                              p_mecid   Mec.mecid%TYPE,
                              p_mectype Mec.mechtype%TYPE,
                              p_evtpref INTEGER,
                              p_bump INTEGER,
                              p_commit INTEGER,
                              p_check_priority INTEGER);
CREATE OR REPLACE FUNCTION epref.setprefmec_mechtype(p_evid    EVENT.EVID%TYPE,
                              p_mecid   Mec.mecid%TYPE,
                              p_mectype Mec.mechtype%TYPE,
                              p_evtpref INTEGER,
                              p_bump INTEGER,
                              p_commit INTEGER,
                              p_check_priority INTEGER) RETURNS INTEGER AS $$
  DECLARE
    -- init local variables
    v_pm_type Mec.mechtype%TYPE  := NULL;
    v_pm_id   Mec.mecid%TYPE    := 0;
    v_orid    Origin.orid%TYPE     := 0;
    v_prefor  Event.prefor%TYPE    := 0;
    v_status  INTEGER          := 0;
    v_eprefu  INTEGER          := 0;
    v_bump    INTEGER          := 0;
    v_chk_prior INTEGER        := 0;
    --
  BEGIN
    --
    -- Process data for EVENT table DML
    --
    BEGIN
      --
      IF (p_evid IS NULL OR p_mecid IS NULL OR p_mectype IS NULL) THEN
        RETURN -1;
      END IF;
      --
      -- Is event preferred mec the same mechtype as input?
      SELECT COALESCE(mecid, 0), mechtype INTO v_pm_id, v_pm_type FROM mec
        WHERE mecid = (SELECT prefmec FROM event WHERE evid = p_evid);
      --
      -- v_chk_priority := p_check_priority; -- once implemented
      -- current prefmec may be different type
      IF (v_chk_prior > 0 AND v_pm_id <> p_mecid) THEN
        -- v_status := MECPREF.highest_priority(v_pm_id, p_mecid);
        IF (v_status = v_pm_id) THEN
          IF (v_pkg_debug > 0) THEN
            RAISE NOTICE USING MESSAGE = 'setprefmec_mechtype of event ' || p_mectype || ' is no-op, old mecid: ' || v_pm_id || ' new mecid: '  || p_mecid;
          END IF;
          v_status := -99;
        END IF;
      END IF;
      --
      IF (v_status <> -99) THEN
        -- Existing Event.prefmec is not a higher priority so it's ok to update
        -- EVENT table if input flag > 0 or prefmec mechtype matches and mecids do not match
        IF ( (p_evtpref > 0) OR ((v_pm_type = p_mectype) AND (v_pm_id <> p_mecid)) ) THEN
          IF (v_pm_id <> p_mecid) THEN -- update only if different -aww 2008/02/28
            IF (v_pkg_debug > 0) THEN
              RAISE NOTICE USING MESSAGE = 'EPREF.setprefmec_mechtype updating event prefmec, input mecid: ' || p_mecid || ' existing prefmec: '  || v_pm_id;
            END IF;
            UPDATE event SET prefmec = p_mecid WHERE evid = p_evid RETURNING prefor INTO v_prefor;
            GET DIAGNOSTICS v_status = ROW_COUNT;
            v_status := v_status + 1;
            v_eprefu := v_status;
          ELSE -- just get prefor
            IF (v_pkg_debug > 0) THEN
              RAISE NOTICE USING MESSAGE = 'EPREF.setprefmec_mechtype: Event.prefmec unchanged, same as input mecid: ' ||  p_mecid;
            END IF;
            SELECT prefor INTO v_prefor FROM event WHERE evid = p_evid;
            v_status := 1;
          END IF;
        END IF;
      END IF;
      --
    EXCEPTION WHEN NO_DATA_FOUND THEN
      IF (v_pkg_debug > 0) THEN
        RAISE NOTICE 'EPREF.setprefmec_mechtype: No prior Event.prefmec , settting prefmec for evid: %', p_evid ;
      END IF;
      -- have no pref, an unusual state unless virgin
         IF (v_chk_prior > 0) THEN
           -- v_status := MECPREF.getMecPriority(p_mecid);
           IF (v_status < 0) THEN
             IF (v_pkg_debug > 0) THEN
               RAISE NOTICE USING MESSAGE = 'EPREF.setprefmec_mechtype: input mecid ' || p_mecid || ' FAILED priority rule test, evid: ' || p_evid ;
             END IF;
             v_status := -2; -- to flag mec priority rule failure
           END IF;
         ELSE
           v_status := 0;
         END IF;
         --
         IF (v_status >= 0) THEN --  priority >= 0, ok since MECPREFPRIORITY rules allow
           IF (v_pkg_debug > 0) THEN
             RAISE NOTICE USING MESSAGE = 'EPREF.setprefmec_mechtype: input mecid ' || p_mecid || ' has highest priority=' || v_status || ', evid: ' || p_evid;
           END IF;
           UPDATE event SET prefmec = p_mecid WHERE evid = p_evid
             RETURNING prefor INTO v_prefor;
           GET DIAGNOSTICS v_status = ROW_COUNT;
           v_status := v_status + 1;
           v_eprefu := v_status;
         ELSE -- priority < 0,  no MECPREFPRIORITY table rule for this mec
           -- v_status := 0;  -- leave it be -aww 2010/01/14
           RETURN v_status; -- BAIL
         END IF;
    END;
    --
    IF (v_status > 1 AND p_bump > 0) THEN
      v_bump := 1; -- flag to bump
    END IF;
    --

    -- Process data for ORIGIN table DML
    --
    BEGIN
      -- reset local variables for reuse
      v_pm_type := NULL;
      v_pm_id := 0;
      --
      -- get value of Origin.prefmec for the input Mec.oridin
      SELECT o.orid, COALESCE(o.prefmec, 0) INTO v_orid, v_pm_id FROM origin o, mec m
        WHERE o.orid = m.oridin AND m.mecid=p_mecid;
      --
      --
      -- if Origin.prefmec is not null and not p_mecid get mechtype for Origin.prefmec
      IF ( (v_pm_id > 0) AND (v_pm_id <> p_mecid) ) THEN
        SELECT mechtype INTO v_pm_type FROM mec WHERE mecid = v_pm_id;
      END IF;
      --
      -- current prefmec may be different type
      IF (v_chk_prior > 0 AND v_pm_id > 0 AND v_pm_id <> p_mecid) THEN
        --v_status := MECPREF.highest_priority(v_pm_id, p_mecid);
        IF (v_status = v_pm_id) THEN
          IF (v_pkg_debug > 0) THEN
            RAISE NOTICE USING MESSAGE = 'EPREF.setprefmec_mechtype of origin ' || p_mectype || ' is no-op, old prefmec: ' || v_pm_id ||
                                 ' has higher priority than mecid: '  || p_mecid;
          END IF;
          v_status := -99;
        END IF;
      END IF;
      --
      IF (v_status <> -99) THEN
        -- Existing Origin.prefmec is not a higher priority so it's ok to update
        -- Origin table if prefmec is null or mechtype matches and mecid do not match,
        -- or it's flagged event preferred and its associated origin is the event.prefor.
        IF ( (v_pm_id <= 0) OR (v_pm_type = p_mectype AND v_pm_id <> p_mecid) OR (p_evtpref > 0  AND v_orid = v_prefor) ) THEN
          IF (v_pm_id <> p_mecid) THEN -- update only if different -aww 2008/02/28
            IF (v_pkg_debug > 0) THEN
              RAISE NOTICE USING MESSAGE = 'EPREF.setprefmec_mechtype updating origin prefmec, input mecid: ' || p_mecid || ' existing prefmec: '  || v_pm_id;
            END IF;
            UPDATE origin SET prefmec = p_mecid WHERE orid = v_orid;
            GET DIAGNOSTICS v_status = ROW_COUNT;
          ELSE
           IF (v_pkg_debug > 0) THEN
             RAISE NOTICE 'EPREF.setprefmec_mechtype: Origin.prefmec unchanged, same as input mecid: %', p_mecid;
           END IF;
            v_status := 1; -- already set
          END IF;
        END IF;
      END IF;
      --
    EXCEPTION WHEN NO_DATA_FOUND THEN
      RAISE NOTICE 'EPREF.setprefmec_mechtype: ERROR, Origin row, or Mec row is missing for input mecid: %', p_mecid;
      -- Don't return, keep going we need to update the eventprefmec table still
      -- RETURN -1; -- no mec row for input mecid or db is missing its associated origin row
    END;
    --
    -- Process data for EVENTPREFMEC table DML
    --
    v_status := epref.update_eventprefmec(p_evid, p_mecid, p_mectype, 0, v_chk_prior); -- does commit action by flag
    IF (v_status = 9999) THEN
      v_status := 0;
    ELSIF ( v_status > 1 ) THEN
      -- to discriminate from v_eprefu return value
      v_status := 1;
    END IF;
    --
    -- Return result of updating event prefmec -- aww 2014/05/15
    IF ( v_eprefu >= 2 ) THEN
        v_status := v_eprefu;   
    END IF;
    --
    IF (v_bump > 0 AND p_bump > 0) THEN
       v_bump := epref.bump_version(p_evid, 0);
    END IF;
    --
    -- IF (p_commit > 0) THEN
    --   COMMIT;
    --   IF (v_pkg_debug > 0) THEN
    --      DBMS_OUTPUT.PUT_LINE('EPREF.setprefmec_mechtype COMMIT for evid: '  || p_evid);
    --   END IF;
    -- END IF;
    --
    RETURN v_status;
    --
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS epref.delete_prefmag(p_evid Event.evid%TYPE, p_magid Netmag.magid%TYPE);
-- Deletes the eventprefmag row corresponding to inputs if it is not (Event.prefmag)
-- the event preferred magnitude. Upon success commits transaction.
-- Returns 1 if row was deleted, returns 0 if no such row, or input magid is the
-- event preferred magid. Returns -1 if other exception.
CREATE OR REPLACE FUNCTION epref.delete_prefmag(p_evid Event.evid%TYPE, p_magid Netmag.magid%TYPE) RETURNS INTEGER AS $$
  DECLARE
     v_prefmag event.prefmag%TYPE := 0;
     v_status INTEGER := 0;
  BEGIN
    SELECT COALESCE(prefmag, 0) INTO v_prefmag FROM event WHERE evid = p_evid;
    IF (v_prefmag <> p_magid) THEN
      DELETE FROM eventprefmag WHERE evid = p_evid AND magid = p_magid;
      GET DIAGNOSTICS v_status = ROW_COUNT;
    END IF;
    RETURN v_status; 
  EXCEPTION 
    WHEN NO_DATA_FOUND THEN
      RETURN 0; 
    WHEN OTHERS THEN
      RAISE NOTICE USING MESSAGE = 'EXCEPTION epref.delete_prefmag('|| p_evid ||','|| p_magid ||')'
                         ||' CODE: ' || TO_CHAR(SQLCODE);
      RETURN -1; 
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS epref.delete_prefmag(p_evid Event.evid%TYPE, p_magtype NetMag.magtype%TYPE);
-- Deletes the eventprefmag row corresponding to inputs if it is not (Event.prefmag) 
-- the event preferred magnitude Upon success commits transaction.
-- Returns 1 if row was deleted, returns 0 if no eventprefmag row with input evid and
-- magtype or its magid is the event preferred magid. Returns -1 if other exception.
CREATE OR REPLACE FUNCTION epref.delete_prefmag(p_evid Event.evid%TYPE, p_magtype NetMag.magtype%TYPE) RETURNS INTEGER AS $$
  DECLARE
     v_prefmag event.prefmag%TYPE := 0;
     v_magid netmag.magid%TYPE := 0;
     v_status INTEGER := 0;
  BEGIN
    SELECT COALESCE(event.prefmag, 0), eventprefmag.magid INTO v_prefmag, v_magid FROM event, eventprefmag
        WHERE eventprefmag.evid=event.evid AND eventprefmag.magtype = p_magtype and event.evid = p_evid;
    IF (v_magid <> v_prefmag) THEN
      DELETE FROM eventprefmag WHERE evid = p_evid AND magtype = p_magtype and magid = v_magid;
      GET DIAGNOSTICS v_status = ROW_COUNT;
    END IF;
    RETURN v_status; 
  EXCEPTION WHEN NO_DATA_FOUND THEN
      RETURN 0;
    WHEN OTHERS THEN
      RAISE NOTICE USING MESSAGE = 'EXCEPTION epref.delete_prefmag('|| p_evid ||','|| p_magtype ||')'
                         ||' CODE: ' || TO_CHAR(SQLCODE);
    RETURN -1;
  END;
$$ LANGUAGE plpgsql;

DROP FUNCTION IF EXISTS epref.upsertAssocArO(
              p_orid AssocArO.ORID%TYPE,
              p_arid AssocArO.ARID%TYPE,
              p_auth AssocArO.AUTH%TYPE,
              p_ssrc AssocArO.SUBSOURCE%TYPE,
              p_iphs AssocArO.IPHASE%TYPE,
              p_impt AssocArO.IMPORTANCE%TYPE,
              p_dist AssocArO.DELTA%TYPE,
              p_azim AssocArO.SEAZ%TYPE,
              p_iwgt AssocArO.IN_WGT%TYPE,
              p_owgt AssocArO.WGT%TYPE,
              p_tres AssocArO.TIMERES%TYPE,
              p_ema  AssocArO.EMA%TYPE,
              p_sdly AssocArO.SDELAY%TYPE,
              p_rflg AssocArO.RFLAG%TYPE);

CREATE OR REPLACE FUNCTION epref.upsertAssocArO(
              p_orid AssocArO.ORID%TYPE,
              p_arid AssocArO.ARID%TYPE,
              p_auth AssocArO.AUTH%TYPE,
              p_ssrc AssocArO.SUBSOURCE%TYPE,
              p_iphs AssocArO.IPHASE%TYPE,
              p_impt AssocArO.IMPORTANCE%TYPE,
              p_dist AssocArO.DELTA%TYPE,
              p_azim AssocArO.SEAZ%TYPE,
              p_iwgt AssocArO.IN_WGT%TYPE,
              p_owgt AssocArO.WGT%TYPE,
              p_tres AssocArO.TIMERES%TYPE,
              p_ema  AssocArO.EMA%TYPE,
              p_sdly AssocArO.SDELAY%TYPE,
              p_rflg AssocArO.RFLAG%TYPE
            )  RETURNS VOID AS $$
  DECLARE
    v_istat INTEGER := 0;
  BEGIN
    v_istat := epref.insertAssocArO( p_orid, p_arid, p_auth, p_ssrc, p_iphs,
                  p_impt, p_dist, p_azim, p_iwgt, p_owgt, p_tres, p_ema , p_sdly, p_rflg);
  END;
$$ LANGUAGE plpgsql;

-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
CREATE OR REPLACE FUNCTION epref.insertAssocArO(
              p_orid AssocArO.ORID%TYPE,
              p_arid AssocArO.ARID%TYPE,
              p_auth AssocArO.AUTH%TYPE,
              p_ssrc AssocArO.SUBSOURCE%TYPE,
              p_iphs AssocArO.IPHASE%TYPE,
              p_impt AssocArO.IMPORTANCE%TYPE,
              p_dist AssocArO.DELTA%TYPE,
              p_azim AssocArO.SEAZ%TYPE,
              p_iwgt AssocArO.IN_WGT%TYPE,
              p_owgt AssocArO.WGT%TYPE,
              p_tres AssocArO.TIMERES%TYPE,
              p_ema  AssocArO.EMA%TYPE,
              p_sdly AssocArO.SDELAY%TYPE,
              p_rflg AssocArO.RFLAG%TYPE
           ) RETURNS INTEGER AS $$
  DECLARE
  v_istat INTEGER := 0;
  BEGIN
    BEGIN
        INSERT INTO ASSOCARO (ORID,ARID,AUTH,SUBSOURCE,IPHASE,IMPORTANCE,DELTA,SEAZ,IN_WGT,WGT,TIMERES,EMA,SDELAY,RFLAG)
          VALUES (p_orid,p_arid,p_auth,p_ssrc,p_iphs,p_impt,p_dist,p_azim,p_iwgt,p_owgt,p_tres,p_ema,p_sdly,p_rflg);
        --
    EXCEPTION WHEN UNIQUE_VIOLATION THEN
        -- Dont' update just RETURN;
        --UPDATE ASSOCARO SET
        --  AUTH=p_auth,SUBSOURCE=p_ssrc,IPHASE=p_iphs,IMPORTANCE=p_impt,DELTA=p_dist,SEAZ=p_azim,
        --  IN_WGT=p_iwgt,WGT=p_owgt,TIMERES=p_tres,EMA=p_ema,SDELAY=p_sdly,RFLAG=p_rflg
        --WHERE ORID=p_orid AND ARID=p_arid;
        --
        RETURN 0;
        --
    END;
    --
    GET DIAGNOSTICS v_istat = ROW_COUNT;
    RETURN v_istat;
  --
  EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE USING MESSAGE = 'EXCEPTION epref.insertAssocArO('|| p_orid ||','||p_arid ||' CODE: ' || TO_CHAR(SQLCODE);
        -- RETURN -1;
        RAISE;
  END;
$$ LANGUAGE plpgsql;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
DROP FUNCTION IF EXISTS epref.upsertAssocAmO(
              p_orid AssocAmO.ORID%TYPE,
              p_ampid AssocAmO.AMPID%TYPE,
              p_auth AssocAmO.AUTH%TYPE,
              p_ssrc AssocAmO.SUBSOURCE%TYPE,
              p_dist AssocAmO.DELTA%TYPE,
              p_azim AssocAmO.SEAZ%TYPE,
              p_rflg AssocAmO.RFLAG%TYPE);

CREATE OR REPLACE FUNCTION epref.upsertAssocAmO(
              p_orid AssocAmO.ORID%TYPE,
              p_ampid AssocAmO.AMPID%TYPE,
              p_auth AssocAmO.AUTH%TYPE,
              p_ssrc AssocAmO.SUBSOURCE%TYPE,
              p_dist AssocAmO.DELTA%TYPE,
              p_azim AssocAmO.SEAZ%TYPE,
              p_rflg AssocAmO.RFLAG%TYPE
           ) RETURNS VOID AS $$
  DECLARE
    v_istat INTEGER := 0;
  BEGIN
    v_istat := epref.insertAssocAmO(p_orid, p_ampid, p_auth, p_ssrc, p_dist, p_azim, p_rflg);
  END;
$$ LANGUAGE plpgsql;
--
--
DROP FUNCTION IF EXISTS epref.insertAssocAmO(
              p_orid AssocAmO.ORID%TYPE,
              p_ampid AssocAmO.AMPID%TYPE,
              p_auth AssocAmO.AUTH%TYPE,
              p_ssrc AssocAmO.SUBSOURCE%TYPE,
              p_dist AssocAmO.DELTA%TYPE,
              p_azim AssocAmO.SEAZ%TYPE,
              p_rflg AssocAmO.RFLAG%TYPE);
CREATE OR REPLACE FUNCTION epref.insertAssocAmO(
              p_orid AssocAmO.ORID%TYPE,
              p_ampid AssocAmO.AMPID%TYPE,
              p_auth AssocAmO.AUTH%TYPE,
              p_ssrc AssocAmO.SUBSOURCE%TYPE,
              p_dist AssocAmO.DELTA%TYPE,
              p_azim AssocAmO.SEAZ%TYPE,
              p_rflg AssocAmO.RFLAG%TYPE
           ) RETURNS INTEGER AS $$
  DECLARE
    v_istat INTEGER := 0;
  BEGIN
    BEGIN
        INSERT INTO ASSOCAMO (ORID,AMPID,AUTH,SUBSOURCE,DELTA,SEAZ,RFLAG) 
          VALUES (p_orid,p_ampid,p_auth,p_ssrc,p_dist,p_azim,p_rflg);
        --
    EXCEPTION WHEN UNIQUE_VIOLATION THEN
        -- Dont' update just RETURN;
        --UPDATE ASSOCAMO SET AUTH=p_auth,SUBSOURCE=p_ssrc,DELTA=p_dist,SEAZ=p_azim,RFLAG=p_rflg
        --WHERE ORID=p_orid AND AMPID=p_ampid;
        --
        RETURN 0;
        --
    END;
    --
    GET DIAGNOSTICS v_istat = ROW_COUNT;
    RETURN v_istat;
  --
  EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE USING MESSAGE = 'EXCEPTION epref.insertAssocAmO('|| p_orid ||','||p_ampid ||' CODE: ' || TO_CHAR(SQLCODE);
        --RETURN -1;
        RAISE;
  END; 
$$ LANGUAGE plpgsql;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
DROP FUNCTION IF EXISTS epref.upsertAssocCoO(
              p_orid AssocCoO.ORID%TYPE,
              p_coid AssocCoO.COID%TYPE,
              p_auth AssocCoO.AUTH%TYPE,
              p_ssrc AssocCoO.SUBSOURCE%TYPE,
              p_dist AssocCoO.DELTA%TYPE,
              p_azim AssocCoO.SEAZ%TYPE,
              p_rflg AssocCoO.RFLAG%TYPE);
CREATE OR REPLACE FUNCTION epref.upsertAssocCoO(
              p_orid AssocCoO.ORID%TYPE,
              p_coid AssocCoO.COID%TYPE,
              p_auth AssocCoO.AUTH%TYPE,
              p_ssrc AssocCoO.SUBSOURCE%TYPE,
              p_dist AssocCoO.DELTA%TYPE,
              p_azim AssocCoO.SEAZ%TYPE,
              p_rflg AssocCoO.RFLAG%TYPE
           ) RETURNS VOID AS $$
  DECLARE
    v_istat INTEGER := 0;
  BEGIN
    v_istat := epref.insertAssocCoO(p_orid, p_coid, p_auth, p_ssrc, p_dist, p_azim, p_rflg);
  END;
$$ LANGUAGE plpgsql;
--
--
DROP FUNCTION IF EXISTS epref.insertAssocCoO(
              p_orid AssocCoO.ORID%TYPE,
              p_coid AssocCoO.COID%TYPE,
              p_auth AssocCoO.AUTH%TYPE,
              p_ssrc AssocCoO.SUBSOURCE%TYPE,
              p_dist AssocCoO.DELTA%TYPE,
              p_azim AssocCoO.SEAZ%TYPE,
              p_rflg AssocCoO.RFLAG%TYPE);
CREATE OR REPLACE FUNCTION epref.insertAssocCoO(
              p_orid AssocCoO.ORID%TYPE,
              p_coid AssocCoO.COID%TYPE,
              p_auth AssocCoO.AUTH%TYPE,
              p_ssrc AssocCoO.SUBSOURCE%TYPE,
              p_dist AssocCoO.DELTA%TYPE,
              p_azim AssocCoO.SEAZ%TYPE,
              p_rflg AssocCoO.RFLAG%TYPE
           ) RETURNS INTEGER AS $$
  DECLARE
    v_istat INTEGER := 0;
  BEGIN
    BEGIN
        INSERT INTO ASSOCCOO (ORID,COID,AUTH,SUBSOURCE,DELTA,SEAZ,RFLAG) 
          VALUES (p_orid,p_coid,p_auth,p_ssrc,p_dist,p_azim,p_rflg);
        --
    EXCEPTION WHEN UNIQUE_VIOLATION THEN
        -- Dont' update just RETURN;
        --UPDATE ASSOCCOO SET AUTH=p_auth,SUBSOURCE=p_ssrc,DELTA=p_dist,SEAZ=p_azim,RFLAG=p_rflg
        --WHERE ORID=p_orid AND COID=p_coid;
        --
        RETURN 0;
        --
    END;
    --
    GET DIAGNOSTICS v_istat = ROW_COUNT;
    RETURN v_istat;
  --
  EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE USING MESSAGE = 'EXCEPTION epref.insertAssocCoO('|| p_orid ||','||p_coid ||' CODE: ' || TO_CHAR(SQLCODE);
        --RETURN -1;
        RAISE;
  END;
$$ LANGUAGE plpgsql;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
DROP FUNCTION IF EXISTS epref.upsertAssocAmM(
              p_magid AssocAmM.MAGID%TYPE,
              p_ampid AssocAmM.AMPID%TYPE,
              p_auth AssocAmM.AUTH%TYPE,
              p_ssrc AssocAmM.SUBSOURCE%TYPE,
              p_wt AssocAmM.WEIGHT%TYPE,
              p_iwgt AssocAmM.IN_WGT%TYPE,
              p_mag AssocAmM.MAG%TYPE,
              p_mres AssocAmM.MAGRES%TYPE,
              p_mcorr AssocAmM.MAGCORR%TYPE,
              p_rflag AssocAmM.RFLAG%TYPE);
CREATE OR REPLACE FUNCTION epref.upsertAssocAmM(
              p_magid AssocAmM.MAGID%TYPE,
              p_ampid AssocAmM.AMPID%TYPE,
              p_auth AssocAmM.AUTH%TYPE,
              p_ssrc AssocAmM.SUBSOURCE%TYPE,
              p_wt AssocAmM.WEIGHT%TYPE,
              p_iwgt AssocAmM.IN_WGT%TYPE,
              p_mag AssocAmM.MAG%TYPE,
              p_mres AssocAmM.MAGRES%TYPE,
              p_mcorr AssocAmM.MAGCORR%TYPE,
              p_rflag AssocAmM.RFLAG%TYPE) RETURNS VOID AS $$
  DECLARE
    v_istat INTEGER := 0;
  BEGIN
    v_istat := epref.insertAssocAmM(p_magid, p_ampid, p_auth, p_ssrc, p_wt, p_iwgt, p_mag, p_mres, p_mcorr, p_rflag);
  END;
$$ LANGUAGE plpgsql;
--
--
DROP FUNCTION IF EXISTS epref.insertAssocAmM(
              p_magid AssocAmM.MAGID%TYPE,
              p_ampid AssocAmM.AMPID%TYPE,
              p_auth AssocAmM.AUTH%TYPE,
              p_ssrc AssocAmM.SUBSOURCE%TYPE,
              p_wt AssocAmM.WEIGHT%TYPE,
              p_iwgt AssocAmM.IN_WGT%TYPE,
              p_mag AssocAmM.MAG%TYPE,
              p_mres AssocAmM.MAGRES%TYPE,
              p_mcorr AssocAmM.MAGCORR%TYPE,
              p_rflag AssocAmM.RFLAG%TYPE);
CREATE OR REPLACE FUNCTION epref.insertAssocAmM(
              p_magid AssocAmM.MAGID%TYPE,
              p_ampid AssocAmM.AMPID%TYPE,
              p_auth AssocAmM.AUTH%TYPE,
              p_ssrc AssocAmM.SUBSOURCE%TYPE,
              p_wt AssocAmM.WEIGHT%TYPE,
              p_iwgt AssocAmM.IN_WGT%TYPE,
              p_mag AssocAmM.MAG%TYPE,
              p_mres AssocAmM.MAGRES%TYPE,
              p_mcorr AssocAmM.MAGCORR%TYPE,
              p_rflag AssocAmM.RFLAG%TYPE
           ) RETURNS INTEGER AS $$
  DECLARE 
    v_istat INTEGER := 0;
  BEGIN
    BEGIN
        INSERT INTO ASSOCAMM (MAGID,AMPID,AUTH,SUBSOURCE,WEIGHT,IN_WGT,MAG,MAGRES,MAGCORR,RFLAG)
          VALUES (p_magid,p_ampid,p_auth,p_ssrc,p_wt,p_iwgt,p_mag,p_mres,p_mcorr,p_rflag);
        --
    EXCEPTION WHEN UNIQUE_VIOLATION THEN
        -- Dont' update just RETURN;
        --UPDATE ASSOCAMM SET AUTH=p_auth,SUBSOURCE=p_ssrc,WEIGHT=p_wt,IN_WGT=p_iwgt,MAG=p_mag,MAGRES=p_mres,MAGCORR=p_mcorr,RFLAG=p_rflg
        --WHERE MAGID=p_magid AND AMPID=p_ampid;
        --
        RETURN 0;
        --
    END;
    --
    GET DIAGNOSTICS v_istat = ROW_COUNT;
    RETURN v_istat;
  --
  EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE USING MESSAGE = 'EXCEPTION epref.insertAssocAmM('|| p_magid ||','|| p_ampid ||' CODE: ' || TO_CHAR(SQLCODE);
        --RETURN -1;
        RAISE;
  END;
$$ LANGUAGE plpgsql;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
--
CREATE OR REPLACE FUNCTION epref.upsertAssocCoM(
              p_magid AssocCoM.MAGID%TYPE,
              p_coid AssocCoM.COID%TYPE,
              p_auth AssocCoM.AUTH%TYPE,
              p_ssrc AssocCoM.SUBSOURCE%TYPE,
              p_wt AssocCoM.WEIGHT%TYPE,
              p_iwgt AssocCoM.IN_WGT%TYPE,
              p_mag AssocCoM.MAG%TYPE,
              p_mres AssocCoM.MAGRES%TYPE,
              p_mcorr AssocCoM.MAGCORR%TYPE,
              p_rflag AssocCoM.RFLAG%TYPE
           ) RETURNS VOID AS $$
  DECLARE
    v_istat INTEGER := 0;
  BEGIN
    v_istat := epref.insertAssocCoM(p_magid, p_coid, p_auth, p_ssrc, p_wt, p_iwgt, p_mag, p_mres, p_mcorr, p_rflag);
  END;
$$ LANGUAGE plpgsql;
--
--
DROP FUNCTION IF EXISTS epref.insertAssocCoM(
              p_magid AssocCoM.MAGID%TYPE,
              p_coid AssocCoM.COID%TYPE,
              p_auth AssocCoM.AUTH%TYPE,
              p_ssrc AssocCoM.SUBSOURCE%TYPE,
              p_wt AssocCoM.WEIGHT%TYPE,
              p_iwgt AssocCoM.IN_WGT%TYPE,
              p_mag AssocCoM.MAG%TYPE,
              p_mres AssocCoM.MAGRES%TYPE,
              p_mcorr AssocCoM.MAGCORR%TYPE,
              p_rflag AssocCoM.RFLAG%TYPE);
CREATE OR REPLACE FUNCTION epref.insertAssocCoM(
              p_magid AssocCoM.MAGID%TYPE,
              p_coid AssocCoM.COID%TYPE,
              p_auth AssocCoM.AUTH%TYPE,
              p_ssrc AssocCoM.SUBSOURCE%TYPE,
              p_wt AssocCoM.WEIGHT%TYPE,
              p_iwgt AssocCoM.IN_WGT%TYPE,
              p_mag AssocCoM.MAG%TYPE,
              p_mres AssocCoM.MAGRES%TYPE,
              p_mcorr AssocCoM.MAGCORR%TYPE,
              p_rflag AssocCoM.RFLAG%TYPE
           ) RETURNS INTEGER AS $$
  DECLARE 
    v_istat INTEGER := 0;
  BEGIN
    BEGIN
        INSERT INTO ASSOCCOM (MAGID,COID,AUTH,SUBSOURCE,WEIGHT,IN_WGT,MAG,MAGRES,MAGCORR,RFLAG) 
          VALUES (p_magid,p_coid,p_auth,p_ssrc,p_wt,p_iwgt,p_mag,p_mres,p_mcorr,p_rflag);
        --
    EXCEPTION WHEN UNIQUE_VIOLATION THEN
        -- Dont' update just RETURN;
        --UPDATE ASSOCCOM SET AUTH=p_auth,SUBSOURCE=p_ssrc,WEIGHT=p_wt,IN_WGT=p_iwgt,MAG=p_mag,MAGRES=p_mres,MAGCORR=p_mcorr,RFLAG=p_rflg
        --WHERE MAGID=p_magid AND COID=p_COID;
        --
        RETURN 0;
        --
    END;
    --
    GET DIAGNOSTICS v_istat = ROW_COUNT;
    RETURN v_istat;
  --
  EXCEPTION
    WHEN OTHERS THEN
        RAISE NOTICE USING MESSAGE =  'EXCEPTION epref.insertAssocCoM('|| p_magid ||','|| p_coid ||' CODE: ' || TO_CHAR(SQLCODE);
        --RETURN -1;
        RAISE;
  END;
$$ LANGUAGE plpgsql;
-- ------------------------------------------------------------------------

DROP FUNCTION IF EXISTS epref.update_eventprefmec(p_evid Event.evid%TYPE,
                               p_mecid Mec.mecid%TYPE,
                               p_mectype Mec.mechtype%TYPE,
                               p_commit INTEGER,
                               p_check_priority INTEGER);
CREATE OR REPLACE FUNCTION epref.update_eventprefmec(p_evid Event.evid%TYPE,
                               p_mecid Mec.mecid%TYPE,
                               p_mectype Mec.mechtype%TYPE,
                               p_commit INTEGER,
                               p_check_priority INTEGER) RETURNS INTEGER AS $$
 DECLARE
    -- reset local variables for reuse
    v_mecid Mec.mecid%TYPE := 0;
    v_prefmec Mec.mecid%TYPE := 0;
    v_orid Origin.orid%TYPE := 0;
    v_status  INTEGER := 0;
    v_chk_prior INTEGER := 0;
    v_pkg_debug INTEGER := 0;
    --
  BEGIN
    BEGIN
      --
      IF (p_evid IS NULL OR p_mecid IS NULL OR p_mectype IS NULL) THEN
         IF (v_pkg_debug > 0) THEN
           RAISE NOTICE USING MESSAGE = 'EPREF.update_eventprefmec NULL input FAILURE p_evid: ' || p_evid || ' p_mecid: ' || p_mecid || ' p_mectype:' || p_mectype;
         END IF;
         RETURN 0;
      END IF;
      --
      -- Get orid associated with the input mec
      BEGIN
          SELECT COALESCE(oridin, 0) INTO v_orid FROM mec WHERE mecid = p_mecid;
      EXCEPTION WHEN NO_DATA_FOUND THEN
        IF (v_pkg_debug > 0) THEN
           RAISE NOTICE 'EPREF.update_eventprefmec FAILED, no Mec row found for input p_mecid: %', p_mecid;
        END IF;
        RETURN 0;
      END;
      --
      -- Get currently set mecid for input mectype and input evid
      SELECT COALESCE(mecid, 0) INTO v_prefmec FROM eventprefmec WHERE evid = p_evid AND mechtype = p_mectype;
      --
      --v_chk_prior := p_check_priority; -- once implemented
      IF (v_chk_prior > 0 AND v_prefmec <> p_mecid) THEN
        -- v_mecid := MECPREF.bestMecidForOriginPref(v_orid, p_mectype); -- fix below -aww 2012/11/20
        --v_mecid := MECPREF.highest_priority(v_prefmec, p_mecid);
        --
        IF (p_mecid <> v_mecid) THEN
          IF (v_pkg_debug > 0) THEN
            RAISE NOTICE USING MESSAGE = 'EPREF.update_eventprefmec type ' || p_mectype ||
                  ' is no-op, highest priority mecid for origin: ' || v_mecid || ' is not the input mecid: '  || p_mecid;
          END IF;
          RETURN 9999;
        END IF;
      END IF;
      --
      -- Here if row exists so
      -- Update eventprefmec table row if its mecid does not match input.
      IF (p_mecid <> v_prefmec) THEN -- update only if different
        UPDATE eventprefmec SET mecid = p_mecid
          WHERE evid = p_evid AND mechtype=p_mectype;
        IF (v_pkg_debug > 0) THEN 
          RAISE NOTICE USING MESSAGE =  'EPREF.update_eventprefmec: updated for evid: ' || p_evid || ' mecid: ' || p_mecid || ' mectype: ' || p_mectype;
        END IF;
        GET DIAGNOSTICS v_status = ROW_COUNT;
        v_status := v_status + 1;
      ELSE
        IF (v_pkg_debug > 0) THEN
           RAISE NOTICE USING MESSAGE = 'EPREF.update_eventprefmec no-op, prefmec for type:' || p_mectype || ' already is input mecid: ' || p_mecid;
        END IF;
        v_status := 1; -- already set 
      END IF;
      --
    EXCEPTION WHEN NO_DATA_FOUND THEN -- Row D.N.E. so create new eventprefmec table
      IF (v_chk_prior > 0) THEN -- first check for valid priority
        --v_status := MECPREF.getMecPriority(p_mecid);
        IF (v_status < 0) THEN
          v_status := -2; -- to flag mec priority rule failure
          IF (v_pkg_debug > 0) THEN 
            RAISE NOTICE USING MESSAGE = 'EPREF.update_eventprefmec for mecid,mectype: ' || p_mecid || ',' || p_mectype || ' FAILED priority rule test, evid: ' || p_evid;
          END IF;
        END IF;
      ELSE
        v_status := 0;
      END IF;
      IF (v_status >= 0) THEN
        INSERT INTO eventprefmec (evid,mechtype,mecid) VALUES (p_evid, p_mectype, p_mecid);
        IF (v_pkg_debug > 0) THEN 
          RAISE NOTICE USING MESSAGE = 'EPREF.update_eventprefmec: insert for evid: ' || p_evid || 'mectype: ' || p_mectype || ' mecid: ' || p_mecid;
        END IF;
        GET DIAGNOSTICS v_status = ROW_COUNT;
        v_status := v_status + 1;
      END IF;
    END;
    --
    IF (p_commit > 0 AND v_status > 1) THEN  -- an update or insert in db tables
      IF (v_pkg_debug > 0) THEN 
        RAISE NOTICE USING MESSAGE = 'EPREF.update_eventprefmec: COMMIT for evid: ' || p_evid || 'mectype: ' || p_mectype || ' mecid: ' || p_mecid;
      END IF;
    END IF;
    --
    RETURN v_status;
    --
  END;
$$ LANGUAGE plpgsql;
-- Set flags indicating what this event is:
-- See constraints on Event.etype for options.
CREATE OR REPLACE FUNCTION EPREF.setEventType(p_evid Event.evid%TYPE, p_type Event.etype%TYPE) RETURNS INTEGER AS $$
  DECLARE
    v_status INTEGER := 0;
    v_type Event.etype%TYPE := NULL;
  BEGIN
    --
    IF (p_evid IS NULL OR p_type IS NULL) THEN
      RETURN 0;
    END IF;
    --
    SELECT coalesce(etype, '') INTO v_type FROM trinetdb.event WHERE evid = p_evid;
    --
    IF (v_type <> p_type) THEN -- update only if different -aww 2008/02/28
      UPDATE trinetdb.event SET etype = p_type WHERE evid = p_evid;
      GET DIAGNOSTICS v_status = ROW_COUNT;
      --
      IF (v_status > 0) THEN
        --does commit
        v_status := EPREF.bump_version(p_evid); -- due to etype change
      END IF;
      --
    ELSE
      v_status := 1; -- already set
    END IF;
    --
    RETURN v_status;
    --
  EXCEPTION WHEN NO_DATA_FOUND THEN
    RETURN -1;
  END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION EPREF.setPreforGtype(p_evid Event.evid%TYPE, p_type Origin.gtype%TYPE) RETURNS INTEGER AS $$
  DECLARE
    v_status INTEGER := 0;
    v_type Origin.gtype%TYPE := NULL;
    v_prefor BIGINT := NULL;
  BEGIN
    --
    IF (p_evid IS NULL OR p_type IS NULL) THEN
      RETURN 0;
    END IF;
    --
    SELECT coalesce(gtype, ''),prefor INTO v_type, v_prefor FROM trinetdb.origin, trinetdb.event  WHERE event.evid = p_evid AND origin.orid=event.prefor;
    --
    IF (v_type <> p_type) THEN -- update only if different -aww 2008/02/28
      UPDATE trinetdb.origin SET gtype = p_type WHERE orid = v_prefor;
      GET DIAGNOSTICS v_status = ROW_COUNT;
      --
      IF (v_status > 0) THEN
        --does commit
        v_status := EPREF.bump_version(p_evid); -- due to gtype change
      END IF;
      --
    ELSE
      v_status := 1; -- already set
    END IF;
    --
    RETURN v_status;
    --
  EXCEPTION WHEN NO_DATA_FOUND THEN
    RETURN -1;
  END;
$$ language plpgsql;
