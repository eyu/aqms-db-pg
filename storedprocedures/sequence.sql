/**
version: 1.0.1 2021-06-16
 
used in production: yes, utc version
 
functions for retrieving sequence values.
 
dependent applications:

- trimag, ampgen, 
 
depends on:

      - sequence schema has to exist.
 
examples: 
```
 select * from sequence.getIncrement('ampseq');
 getincrement 
--------------
            5
(1 row)

archdb=> \gset
archdb=> select sequence.getNext('ampseq',3) a seqrange;
        seqrange        
-------------------
 12768593-12768603
(1 row)
```
The sequence range is parsed by functions aware of the increment size
in the tndb/Database class, which should return the three sequence
values: `12768593, 12768598, 12768603` 
*/

/**
get version number
*/
CREATE OR REPLACE FUNCTION sequence.getPkgId()
RETURNS varchar(80) AS $$
DECLARE
pkgId varchar(80) := 'sequence 1.0.1 2021-06-16';
BEGIN
    RETURN pkgId;
END;
$$ LANGUAGE plpgsql;

/**
get increment value of the named sequence
*/
create or replace FUNCTION sequence.getIncrement(seqname varchar) RETURNS bigint AS $$
DECLARE
	seq_increment bigint;
BEGIN
	EXECUTE 'select increment_by seq_increment from pg_sequences where sequencename = '|| quote_literal(seqname) INTO seq_increment;
	return seq_increment;
END
$$ LANGUAGE plpgsql;


/**
 Return a string containing a range of requested sequence numbers.
 seqnr1 - (seqnr_numseq - increment) 
 If something else has advanced the sequence at the same time, the
 sequences may not advance sequentially with step increment, in that case
 it can return multiple sequence ranges. For example: '2314134-2314135 2314138-2314488' 
 The list can also have isolated sequence numbers, for example: 
 '2314134-2314135 2314138-2314488 2314490 2314498'
 The sequence name is specified by input p_seqname. 
 The number of desired sequenced numbers is specified by in/out p_numseq.
 The number of sequence numbers returned may be less than that requested, 
 the maximum output size of the varchar is 6000
 The total is returned to the p_numseq parameter.
*/
create or replace  FUNCTION sequence.getNext(p_seqname VARCHAR, p_numseq INTEGER) RETURNS VARCHAR AS $$
DECLARE
      numlist VARCHAR(6000);
      tmpval  bigint;
      lastval  bigint;
      numrange  bigint;
      incr_value  bigint;
      sql_cmd VARCHAR(160);
BEGIN
    numlist := '';
    IF (p_numseq <= 0) THEN
        p_numseq := 0;
        RETURN numlist;
    END IF;
    -- if numrange = 0, have no sequence number yet
    -- if numrange = 1, have a single sequential sequence number
    -- if numrange > 1, have at least 2 sequential ones.
    numrange := 0;
    incr_value := sequence.getIncrement(p_seqname);
    --
    sql_cmd := 'SELECT nextval(' || quote_literal(p_seqname) || ')';
    --
    FOR i IN 1..p_numseq LOOP
        -- truncate if the output string will get too large. 
        -- max length is (6000 - 32) (RH: I think this is generous)
        IF (length(numlist) > 5968) THEN
            p_numseq := i - 1;
            IF (numrange > 1) THEN
                numlist := numlist || '-' || lastval;
            END IF;
            RETURN numlist;
        END IF;
        --
        EXECUTE  sql_cmd into tmpval;
        IF (i = 1) THEN
           -- only for first time query id
            numlist := numlist || tmpval;
            lastval := tmpval;
            numrange := 1;
        ELSE
            -- for subsequent queries check ids are consecutive (by step increment size)
            IF (lastval = (tmpval - incr_value)) THEN
                lastval := lastval + incr_value;
                numrange := numrange + 1; -- bump count
            ELSE
                -- end the present open id range with the prior query id value
                IF (numrange > 1) THEN
                    numlist := numlist || '-' || lastval;
                END IF;
                -- now start a new range with current query id value
                numlist := numlist || ' ' || tmpval;
                lastval := tmpval;
                numrange := 1;
            END IF;
        END IF;
    END LOOP;
    -- terminate the last open id range, if any
    IF (numrange > 1) THEN
        numlist := numlist || '-' || lastval;
    END IF;
    --
    -- return string list of sequence id range(s)
    RETURN numlist;
END
$$ LANGUAGE plpgsql;

/** 
 Pipelined function return can be cast as table to get result set
 e.g. "Select column_value from table (sequence.getSeqTable('ARSEQ',?));"
 where the number of desired sequenced numbers is specified by bind variable ?
*/
CREATE OR REPLACE FUNCTION sequence.getSeqTable(p_seqname VARCHAR, p_numseq INTEGER) RETURNS SETOF BIGINT AS $$
  DECLARE
      v_id BIGINT := NULL;
  BEGIN
    FOR idx IN 1..p_numseq LOOP
      SELECT NEXTVAL(p_seqname) INTO v_id;
      RETURN NEXT v_id;
    END LOOP;
  EXCEPTION 
    WHEN NO_DATA_FOUND THEN
      RAISE NOTICE USING MESSAGE='Problem, should always find next sequence: ' || p_seqname;
      RETURN NEXT NULL;
    WHEN OTHERS THEN
      RAISE NOTICE 'OTHERS!';
      RAISE NOTICE '% %', SQLERRM, SQLSTATE;
      RETURN NEXT NULL;
  END;
$$ LANGUAGE plpgsql;
