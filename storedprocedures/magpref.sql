-- create schema if not exists magpref authorization code;
-- grant usage on schema magpref to trinetdb_read, trinetdb_execute;


-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
  -- Package Private 
create or replace FUNCTION magpref.compare_mag_priority(p_magid_old Netmag.magid%TYPE, p_magid_new Netmag.magid%TYPE) RETURNS INTEGER AS $$
DECLARE
    v_priorold INTEGER := -1;
    v_priornew INTEGER := -1;
BEGIN
    --
    -- Need tie breaking comparison like number of channels, better rms, quality etc.
    --
      v_priorold := MAGPREF.getMagPriority(p_magid_old); 
      v_priornew := MAGPREF.getMagPriority(p_magid_new); 
      --
      IF (v_priorold < v_priornew) THEN
        RETURN 1;
      ELSIF (v_priorold = v_priornew) THEN
        IF (v_priornew >= 0) THEN
          RETURN 0;
        ELSE
          RETURN -9;
        END IF;
      END IF;
      --
      -- old > new 
      --
      RETURN -1;
      --
END
$$ LANGUAGE plpgsql;
-- ---------------------------------------------------------------------------
-- ---------------------------------------------------------------------------
  create or replace FUNCTION magpref.highest_priority(p_old_magid Netmag.magid%TYPE, p_new_magid Netmag.magid%TYPE) RETURNS BIGINT AS $$
DECLARE
    v_status INTEGER := -1;
BEGIN
    --
    v_status := magpref.compare_mag_priority(p_old_magid, p_new_magid);
    --
    IF (v_status = -1 ) THEN
      -- old mag higher priority
      RETURN p_old_magid;
    ELSIF (v_status = -9) THEN
      -- both mag failed rules
      RETURN p_old_magid;
    END IF;
    --
    -- else equal priority or new mag has higher priority
    RETURN p_new_magid;
    --
END
$$ LANGUAGE plpgsql;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
create or replace FUNCTION magpref.getMagPriority(
                             p_region      Gazetteer_Region.name%TYPE,
                             p_datetime    Origin.datetime%TYPE,
                             p_magtype     NetMag.magtype%TYPE,
                             p_auth        NetMag.auth%TYPE,
                             p_subsource   NetMag.subsource%TYPE,
                             p_magalgo     NetMag.magalgo%TYPE,
                             p_nsta        NetMag.nsta%TYPE,
                             p_magnitude   NetMag.magnitude%TYPE,
                             p_uncertainty NetMag.uncertainty%TYPE,
                             p_quality     NetMag.quality%TYPE
                           ) RETURNS INTEGER AS $$
DECLARE
        v_prio      INTEGER := NULL;
        v_datetime  INTEGER := 0;
BEGIN
        --
        -- IF (v_pkg_rule_count = 0) THEN
        --   -- no rules so give all magnitudes the same default priority 
        --   RETURN 0;
        -- END IF;
        --
        -- Check for valid datetime, if null set to current time
        -- v_datetime := p_datetime;
        -- IF (v_datetime IS NULL) THEN
        --   v_datetime := UTIL.date2true(SYS_EXTRACT_UTC(SYSTIMESTAMP));
        -- END IF;
        --
        --
        -- find highest priority that matches
        -- Use of max() guarantees only one (largest) value returned
        -- NOTE CHANGED condition from maxuncertainty >= coalesce(p_uncertainty, 0.)  -aww 10/24/2006
          SELECT max(priority) INTO v_prio FROM MagPrefPriority WHERE
                region_name = p_region AND
                (magtype           LIKE p_magtype OR magtype IS NULL) AND
                (minmag            <= p_magnitude OR minmag IS NULL) AND -- inclusive bound
                (maxmag            >= p_magnitude OR maxmag IS NULL) AND -- inclusive bound
                (auth              LIKE p_auth OR auth IS NULL) AND
                (subsource         LIKE p_subsource OR subsource IS NULL) AND -- remove if part of PK -aww
                (magalgo           LIKE p_magalgo OR magalgo IS NULL) AND
                (minreadings       <= coalesce(p_nsta, 0) OR minreadings IS NULL) AND -- inclusive bound
                (maxuncertainty    >= coalesce(p_uncertainty, 9999.) OR maxuncertainty IS NULL) AND -- inclusive bound
                (quality           <= coalesce(p_quality, 0.) OR quality IS NULL) AND -- inclusive bound
                (datetime_on <= truetime.putEpoch(v_datetime,'UTC') AND datetime_off > truetime.putEpoch(v_datetime,'UTC'));
          --
          -- When v_prio is null, no rule satisfies conditions for magtype (or there are no rules for that magtype)
          -- To identify case of no rules, we would need to query count by magtype:
          --    select count(*) into v_prio from magprefpriority where magtype = p_magtype
          -- Setting v_prio=-1 here forces return of old magid by compare_mag_priority function, 
          -- Setting v_prio= 0 below would result in "new" magid being returned by compare_mag_priority
          -- if there were no rules for the input magtype
          IF (v_prio IS NULL) THEN
            v_prio := -1;
          END IF;
        --
          RETURN v_prio;
        --
END
$$ LANGUAGE plpgsql;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
create or replace FUNCTION magpref.getMagPriority(
                             p_region      Gazetteer_Region.name%TYPE,
                             p_magtype     NetMag.magtype%TYPE,
                             p_auth        NetMag.auth%TYPE,
                             p_subsource   NetMag.subsource%TYPE,
                             p_magalgo     NetMag.magalgo%TYPE,
                             p_nsta        NetMag.nsta%TYPE,
                             p_magnitude   NetMag.magnitude%TYPE,
                             p_uncertainty NetMag.uncertainty%TYPE,
                             p_quality     NetMag.quality%TYPE
                           ) RETURNS INTEGER AS $$
DECLARE
BEGIN
        RETURN magpref.getMagPriority(p_region, NULL, p_magtype, p_auth, p_subsource, p_magalgo, p_nsta, p_magnitude, p_uncertainty, p_quality);
END 
$$ LANGUAGE plpgsql;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
create or replace FUNCTION magpref.getMagPriority(p_magid NetMag.magid%TYPE) RETURNS INTEGER AS $$
DECLARE
      v_lat Origin.lat%TYPE;
      v_lon Origin.lon%TYPE;
      v_datetime Origin.datetime%TYPE;
BEGIN
      SELECT o.datetime, o.lat, o.lon INTO v_datetime, v_lat, v_lon FROM Origin o, Netmag n WHERE
        o.orid = n.orid AND n.magid = p_magid;
      --
      RETURN magpref.getMagPriority(p_magid, v_datetime, v_lat, v_lon);
END 
$$ LANGUAGE plpgsql;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
create or replace FUNCTION magpref.getMagPriority(p_magid NetMag.magid%TYPE, p_datetime Origin.datetime%TYPE,
                            p_lat Origin.lat%TYPE, p_lon Origin.lon%TYPE) RETURNS INTEGER AS $$
DECLARE
        v_magtype     NetMag.magtype%TYPE;
        v_auth        NetMag.auth%TYPE;
        v_subsource   NetMag.subsource%TYPE;
        v_magalgo     NetMag.magalgo%TYPE;
        v_nsta        NetMag.nsta%TYPE;
        v_magnitude   NetMag.magnitude%TYPE;
        v_uncertainty NetMag.uncertainty%TYPE;
        v_quality     NetMag.quality%TYPE;
        v_rows     INTEGER;
        -- -------------------------------------
BEGIN
        --
        -- Get all you need to know about the NetMag row
        --
        SELECT magtype, auth, subsource, magalgo, nsta, magnitude, uncertainty, quality
          INTO v_magtype, v_auth, v_subsource, v_magalgo, v_nsta, v_magnitude, v_uncertainty, v_quality
          FROM NetMag WHERE (NetMag.magid = p_magid);
        --
        -- DBMS_OUTPUT.PUT_LINE(p_lat || ' ' || p_lon || ' ' || 
        --                      v_magtype || ' ' || v_auth || ' ' || coalesce(v_subsource,'NULL') || ' ' ||
        --                      coalesce(v_magalgo,'NULL') || ' ' || NVL(v_nsta,0) || ' ' || v_magnitude || ' ' ||
        --                      coalesce(v_uncertainty, 0) || ' ' || NVL(v_quality, 0)
        --                     );
        -- Kludge to get the Percent Variance Reduction mapped into Quality value 0. to 1.
        /*
         -- REMOVED code block since PVR/100 is saved as netmag.quality by applications generating Mw netmag row -aww 2008/10/30 
        IF (v_magtype = 'w') THEN
          SELECT coalesce(PVR,0.)/100. INTO v_quality FROM MEC WHERE MAGID = p_magid;
          --EXCEPTION WHEN NO_DATA_FOUND THEN
          GET DIAGNOSTICS v_rows = ROW_COUNT;
          IF (v_rows = 0) THEN
            v_quality := 1.; -- force Mec missing (historic, imported) to best quality?
          END IF;
        */
        --
        RETURN magpref.getMagPriority( p_datetime, p_lat, p_lon,
                               v_magtype, v_auth,
                               v_subsource, v_magalgo,
                               v_nsta, v_magnitude,
                               v_uncertainty, v_quality
                              );
        --
END
$$ LANGUAGE plpgsql;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
create or replace FUNCTION magpref.getMagPriority(p_magid NetMag.magid%TYPE, p_lat Origin.lat%TYPE, p_lon Origin.lon%TYPE) RETURNS INTEGER AS $$
DECLARE
BEGIN
        RETURN magpref.getMagPriority(p_magid, NULL, p_lat, p_lon);
END
$$ LANGUAGE plpgsql;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
create or replace FUNCTION magpref.getMagPriority( p_datetime    Origin.datetime%TYPE,
                             p_lat         Origin.lat%TYPE,
                             p_lon         Origin.lat%TYPE,
                             p_magtype     NetMag.magtype%TYPE,
                             p_auth        NetMag.auth%TYPE,
                             p_subsource   NetMag.subsource%TYPE,
                             p_magalgo     NetMag.magalgo%TYPE,
                             p_nsta        NetMag.nsta%TYPE,
                             p_magnitude   NetMag.magnitude%TYPE,
                             p_uncertainty NetMag.uncertainty%TYPE,
                             p_quality     NetMag.quality%TYPE
                           ) RETURNS INTEGER AS $$
DECLARE
        v_max_prio    INTEGER := -1;
        v_priority    INTEGER := -1;
	v_count	      INTEGER := 0;
        --v_pkg_name_tab name_tab := name_tab();  -- variable made package level to avoid repeated queries
        -- -------------------------------------
	gazreg RECORD;
BEGIN
        --
        IF (p_lat IS NULL OR p_lon IS NULL) THEN -- all origins should have a lat, lon even if bogus.
          RETURN v_max_prio;
        END IF;
        --
        v_max_prio := magpref.getMagPriority('DEFAULT', p_datetime, p_magtype, p_auth, p_subsource,
                              p_magalgo, p_nsta, p_magnitude, p_uncertainty, p_quality);
        --
        IF (p_lat = 0. AND p_lon = 0.) THEN -- if lat and lon flag bogus use default rule value
          RETURN v_max_prio;
        END IF;
        --
        -- For known lat,lon override default priority value with highest matching region value
        --
	select count(*) into v_count from GAZETTEER_REGION; 
        IF (v_count <= 0) THEN
            -- No region info defined
            --DBMS_OUTPUT.PUT_LINE('INFO: MAGPREF PACKAGE found no regions in GAZETTEER_REGION table.');
            -- RETURN -3;
            RETURN v_max_prio; -- assumes operational intent was to have no rows in this table
        END IF;
        --
        FOR gazreg IN select name from GAZETTEER_REGION LOOP
            IF (geo_region.inside_border(gazreg.name, p_lat, p_lon) = 1) THEN
              v_priority := magpref.getMagPriority(gazreg.name, p_datetime, p_magtype, p_auth, p_subsource,
                              p_magalgo, p_nsta, p_magnitude, p_uncertainty, p_quality);
              IF (v_priority > v_max_prio) THEN
                v_max_prio:= v_priority;
              END IF;
            END IF;
        END LOOP;
        --
        RETURN v_max_prio;
        --
END
$$ LANGUAGE plpgsql;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
create or replace FUNCTION magpref.getMagPriority( p_lat         Origin.lat%TYPE,
                             p_lon         Origin.lat%TYPE,
                             p_magtype     NetMag.magtype%TYPE,
                             p_auth        NetMag.auth%TYPE,
                             p_subsource   NetMag.subsource%TYPE,
                             p_magalgo     NetMag.magalgo%TYPE,
                             p_nsta        NetMag.nsta%TYPE,
                             p_magnitude   NetMag.magnitude%TYPE,
                             p_uncertainty NetMag.uncertainty%TYPE,
                             p_quality     NetMag.quality%TYPE
                           ) RETURNS INTEGER AS $$
DECLARE
BEGIN
        RETURN magpref.getMagPriority(NULL, p_lat, p_lon, p_magtype, p_auth, p_subsource, p_magalgo, p_nsta, p_magnitude, p_uncertainty, p_quality);
END
$$ LANGUAGE plpgsql;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
create or replace FUNCTION magpref.getMagPriorityOfPrefMagType(p_evid Event.evid%TYPE, p_magtype Netmag.magtype%TYPE) RETURNS INTEGER AS $$
DECLARE
        v_magid Netmag.magid%TYPE := 0;
        v_rows integer;
BEGIN
        --
        SELECT magid INTO v_magid FROM EventPrefmag WHERE evid = p_evid and magtype = p_magtype;
        GET DIAGNOSTICS v_rows = ROW_COUNT;
        IF (v_rows = 0) THEN
		RETURN -1;
	END IF;
        RETURN magpref.getMagPriority(v_magid);
        --
END
$$ LANGUAGE plpgsql;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
create or replace FUNCTION magpref.getMagPriorityOfEvent(p_evid Event.evid%TYPE) RETURNS INTEGER AS $$
DECLARE
        v_prefmag Netmag.magid%TYPE := 0;
BEGIN
        --
        v_prefmag := magpref.getPrefMagOfEvent(p_evid);
        --
        IF (v_prefmag <= 0) THEN
            RETURN -1;
        END IF;
        --
        RETURN magpref.getMagPriority(v_prefmag);
        --
END
$$ LANGUAGE plpgsql;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
-- PRIVATE FUNCTION magpref.
    create or replace FUNCTION magpref.setPrefMag(p_evid Event.evid%TYPE, p_magid Netmag.magid%TYPE, p_commit INTEGER) RETURNS BIGINT AS $$
DECLARE
        v_prefmag Netmag.magid%TYPE;
        v_status INTEGER := 0;
        v_magtype NetMag.magtype%TYPE;
BEGIN
        --
        IF (p_evid IS NULL OR p_magid IS NULL) THEN -- error 
          RETURN 0;
        END IF;
        --
        -- Get CURRENT prefmagid, it could be NULL, save for comparison below
        --
        v_prefmag := magpref.getPrefMagOfEvent(p_evid);
        --
        SELECT magtype INTO v_magtype FROM NetMag WHERE (NetMag.magid = p_magid);
        --
        -- Update tables, inserts a new EventPrefMag table row if none exists
        -- v_status := epref.setprefmag_event(p_evid, p_magid); -- replaced by below to not do priority checking! - aww 2008/07/12
        --v_status := epref.setprefmag_magtype(p_evid, p_magid, v_magtype, 1, 1, 1, 0);
        v_status := epref.setprefmag_magtype(p_evid, p_magid, v_magtype, 1, 1, p_commit, 0);
        --
        IF (v_status <= 0) THEN 
          RETURN 0;
        END IF;
        --
        IF (p_magid = v_prefmag) THEN
            RETURN -p_magid;   -- Signal best was already the event preferred.
        ELSE 
            RETURN p_magid;
        END IF;
        --
EXCEPTION WHEN OTHERS THEN
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;
    RETURN 0;

END
$$ LANGUAGE plpgsql;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
-- NOTE : CALLED BY java class MagnitudeTN static method dbaseAutoSetPrefmag(connection,id) via Solution autoSetPrefmag
create or replace FUNCTION magpref.setPrefMagOfEvent(p_evid Event.evid%TYPE) RETURNS INTEGER AS $$
DECLARE
BEGIN
        RETURN magpref.setPrefMagOfEvent(p_evid, 1);
END 
$$ LANGUAGE plpgsql;
    --
    create or replace FUNCTION magpref.setPrefMagOfEvent(p_evid Event.evid%TYPE, p_commit INTEGER) RETURNS INTEGER AS $$
DECLARE
        v_bestmagid  BIGINT := 0;
BEGIN
        --
        v_bestmagid := magpref.bestMagidOfEventPrefMag(p_evid);
        -- No-op if no EventPrefMag table Magnitudes 
        IF (v_bestmagid <= 0) THEN
            RETURN 0;
        END IF;
        --
        RETURN magpref.setPrefMag(p_evid, v_bestmagid, p_commit);
        --
END
$$ LANGUAGE plpgsql;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
create or replace FUNCTION magpref.setPrefMagOfEventByPrefor(p_evid Event.evid%TYPE) RETURNS INTEGER AS $$
BEGIN
        RETURN magpref.setPrefMagOfEventByPrefor(p_evid, 1);
END
$$ LANGUAGE plpgsql;

create or replace FUNCTION magpref.setPrefMagOfEventByPrefor(p_evid Event.evid%TYPE, p_commit INTEGER) RETURNS INTEGER AS $$
DECLARE
        v_prefor     BIGINT;
        v_bestmagid  BIGINT;
        v_prefmag    BIGINT;
BEGIN
        -- Get the prefor
        SELECT prefor INTO v_prefor FROM Event WHERE evid = p_evid;
        --
        IF (v_prefor IS NULL) THEN -- a no-op
            RETURN 0;
        END IF;
        --
        -- Evaluate NetMags associated with the PREFERED ORIGIN
        v_bestmagid  := magpref.bestMagidForOriginPref(v_prefor);
        --
        IF (v_bestmagid <= 0) THEN   -- No  mags
           RETURN 0;
        END IF;
        --
        RETURN magpref.setPrefMag(p_evid, v_bestmagid, p_commit);
END 
$$ LANGUAGE plpgsql;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
create or replace FUNCTION magpref.bestMagidForOriginPref(p_orid Origin.orid%TYPE) RETURNS BIGINT AS $$
DECLARE
	maginfo RECORD;
BEGIN
        --
        -- Get list of current mags in order of priority
        -- Ordered by priority, then 1st one is used. If there is a priority tie
        -- The latest one is used (largest lddate).
        --
        -- Include the origin.prefmag although its orid may be different !!!
        FOR maginfo IN SELECT n.magid as magid, MAGPREF.GETMAGPRIORITY(n.magid, o.datetime, o.lat, o.lon) as prio, n.lddate as lddate FROM NetMag n, Origin o WHERE (n.orid = o.orid OR n.magid=o.prefmag) AND o.orid = p_orid ORDER BY prio DESC, lddate DESC LOOP
        --
        --
        -- Get the first row in the CURSOR list, it will be the one with the highest priority
        -- ( If there is a tie, the latest in time in the list will be used. If no rows, return -1. )
        --
        -- No good mags for this evid (or no such evid)
          IF (maginfo.magid IS NULL OR maginfo.prio < 0) THEN -- added v_bestprio >= 0 condition -aww 2009/03/15
              RETURN 0;
          END IF;
        --
          RETURN  maginfo.magid;
	END LOOP;
EXCEPTION WHEN OTHERS THEN
    RAISE NOTICE USING MESSAGE='magpref.bestMagidForOriginPref(p_orid): ' || SQLERRM || '-' || SQLSTATE;
    RETURN 0;
        --
END 
$$ LANGUAGE plpgsql;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
create or replace FUNCTION magpref.bestMagidForOriginPref(p_orid Origin.orid%TYPE, p_magtype Netmag.magtype%TYPE) RETURNS BIGINT AS $$
DECLARE
	maginfo RECORD;
BEGIN
        --
        -- Get list of current mags in order of priority
        -- Ordered by priority, then 1st one is used. If there is a priority tie
        -- The latest one is used (largest lddate).
        --
        -- Include the origin.prefmag although its orid may be different !!!
        FOR maginfo IN
           SELECT n.magid as magid, MAGPREF.GETMAGPRIORITY(n.magid, o.datetime, o.lat, o.lon) as prio, n.lddate as lddate
               FROM NetMag n, Origin o
               WHERE (n.orid=p_orid OR n.magid=o.prefmag) AND o.orid=p_orid AND n.magtype=p_magtype
               ORDER BY 2 DESC, 3 DESC LOOP
       --
        --
        -- Get the first row in the CURSOR list, it will be the one with the highest priority
        -- ( If there is a tie, the latest in time in the list will be used. If no rows, return -1. )
        --
          IF (maginfo.magid IS NULL OR maginfo.prio < 0) THEN -- added v_bestprio >= 0 condition -aww 2009/03/15
              RETURN 0;
          END IF;
        --
          RETURN  maginfo.magid;
	END LOOP;
        --
END
$$ LANGUAGE plpgsql;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
create or replace FUNCTION magpref.bestMagidForEventPrefor(p_evid Event.evid%TYPE) RETURNS BIGINT AS $$
DECLARE
        v_prefor     BIGINT;
BEGIN
        -- Get the prefor
        SELECT prefor INTO v_prefor FROM Event WHERE evid = p_evid;
        --
        IF (v_prefor IS NULL) THEN
            RETURN 0;
        END IF;
        --
        RETURN magpref.bestMagidForOriginPref(v_prefor); 
END 
$$ LANGUAGE plpgsql;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
create or replace FUNCTION magpref.bestMagidOfEventPrefMag(p_evid Event.evid%TYPE) RETURNS BIGINT AS $$
DECLARE
	maginfo RECORD;
BEGIN
        --
        -- Get the preferred magids for magtypes from EventPrefMag table
        FOR maginfo IN
           SELECT n.magid as magid, MAGPREF.GETMAGPRIORITY(n.magid, o.datetime, o.lat, o.lon) as prio, n.lddate as lddate
               FROM NetMag n, Origin o, EventPrefMag e WHERE
               (o.orid=n.orid) AND (n.magid=e.magid) AND (e.evid = p_evid)
               ORDER BY 2 desc, 3 desc LOOP
        --
        -- Get the first row in the CURSOR list, it will be the one with the highest priority
        -- ( If there is a tie, the latest in time in the list will be used. If no rows, return -1. )
        -- The OPEN actually executes the cursor SELECT statment
        --
        -- No magids for this evid
          IF (maginfo.magid IS NULL OR maginfo.prio < 0) THEN
              RETURN 0;
          END IF;
        --
          RETURN maginfo.magid;
	END LOOP;
        --
    RETURN 0;
END
$$ LANGUAGE plpgsql;
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
create or replace FUNCTION magpref.getPrefMagOfEvent(p_evid Event.evid%TYPE) RETURNS BIGINT AS $$
DECLARE
        v_prefmag        BIGINT;
        --
BEGIN
        SELECT prefmag INTO v_prefmag FROM Event WHERE evid = p_evid;
        --
        IF (v_prefmag IS NULL) THEN
            return -1;
        END IF;
        --
        RETURN v_prefmag;
        --
END
$$ LANGUAGE plpgsql;

-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
create or replace FUNCTION magpref.describeMag(p_magid NetMag.magid%TYPE) RETURNS VARCHAR AS $$
DECLARE
        v_magtype     NetMag.magtype%TYPE;
        v_auth        NetMag.auth%TYPE;
        v_subsource   NetMag.subsource%TYPE;
        v_magalgo     NetMag.magalgo%TYPE;
        v_nsta        NetMag.nsta%TYPE;
        v_magnitude   NetMag.magnitude%TYPE;
        v_uncertainty NetMag.uncertainty%TYPE;
        v_quality     NetMag.quality%TYPE;
	v_rows	integer;
        --
BEGIN
        --
        -- Get all you need to know about the NetMag row
        --
        SELECT magtype, magnitude, auth, subsource, magalgo, nsta, uncertainty, quality
         INTO  v_magtype, v_magnitude, v_auth, v_subsource, v_magalgo, v_nsta, v_uncertainty, v_quality
         FROM NetMag WHERE (NetMag.magid = p_magid);

	GET DIAGNOSTICS v_rows = ROW_COUNT;
	IF (v_rows = 0) THEN
		RETURN NULL;
	END IF;
        --
      /* -- REMOVED 2008/10/30 because PVR/100 is saved in netmag.quality by applications generating Mw magnitude -aww
        -- For 'w' get PVR
        IF (v_magtype = 'w') THEN
            SELECT coalesce(PVR,0.)/100. INTO v_quality FROM MEC WHERE MAGID = p_magid;
	    GET DIAGNOSTICS v_rows = ROW_COUNT;
             --EXCEPTION WHEN NO_DATA_FOUND THEN
	    IF (v_rows = 0) THEN
              v_quality := 1.; -- force Mec missing (historic, imported) to best quality?
            END IF;
        END IF;
      */
        --
        RETURN  'magtype=' || v_magtype || ' ' ||
                ' auth=' || coalesce(v_auth,'NULL') || ' '||
                ' subsource=' || coalesce(v_subsource,'NULL') || ' ' ||
                ' magalgo=' || coalesce(v_magalgo,'NULL') || ' ' ||
                ' nsta=' || coalesce(TO_CHAR(v_nsta),'NULL') || ' ' ||
                ' magnitude=' || coalesce(TO_CHAR(v_magnitude),'NULL') ||' '||
                ' uncertainty=' || coalesce(TO_CHAR(v_uncertainty),'NULL') || ' ' ||
                ' quality=' || coalesce(TO_CHAR(v_quality),'NULL'); 
END
$$ LANGUAGE plpgsql;
