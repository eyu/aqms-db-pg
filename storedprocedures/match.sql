--CallableStatement csN = null;
--        //String sql = "{ ?=call match.getMatch(?) }";
--        String sql = "{ ?=call match.getMatch(?, ?, ?, ?, ?, ?, ?) }";
--        long dupId = 0l;
--        try {
--            if (csN == null) {
--              csN = DataSource.getConnection().prepareCall(sql);
--              csN.registerOutParameter(1, java.sql.Types.BIGINT);
--            }
--            // use current values 
--            csN.setLong(2, getEvidValue());
--            csN.setLong(3, getPreforValue());
--            csN.setString(4, getAuthority()); // origin auth
--            csN.setDouble(5, lat.doubleValue());
--            csN.setDouble(6, lon.doubleValue());
--            csN.setDouble(7, (datetime.isValidNumber() ? datetime.doubleValue() : 0.));
--            csN.setString(8, null); // null subsource
--            csN.execute();
--            dupId = csN.getLong(1);
--

-- --------------------------------------------------------------------------------
-- Used in production: yes, UTC version
--
-- <LIST DEPENDENT APPLICATIONS HERE>
--   org.trinet.jasi.Solution.class, org.trinet.jiggle.Jiggle 
--
-- <DEPENDS ON > (loaded on server)
-- NCEDC Parametric tables Event, Origin, AssocArO, Arrival and EventMatchPassingScore
-- references Wheres package
-- Java org.trinet.util.gazetteer.GeoidalConvert is referenced by Wheres package
-- --------------------------------------------------------------------------------
--
-- Package is used when an event is saved (committed) to the database
-- to determine when two events in database are possibly the same (duplicates).
-- Events are "duplicates" when their comparison score is less then or equal
-- to the value of a set score value that is derived from criteria stored 
-- in table columns of a table named EventMatchPassingScore.
--
-- --------------------------------------------------------------------------------
-- --------------------------------------------------------------------------------
    -- Find matching evid by lat,lon,time, and origin subsource
    -- (subsources don't have to match if input subsource := NULL).
    -- Returns 0 if no matching evid is found, the evid of a matching event, or error code.
------------------------------------------------------------------------------------
--CREATE OR REPLACE FUNCTION match.getPassingScore(p_auth VARCHAR) RETURNS DOUBLE PRECISION AS $$
--  DECLARE
--    v_count INTEGER;
--    v_pkg_window_size NUMBER  := 60.0;
--    v_pkg_max_score NUMBER  := 9999999999.;
--    v_pkg_pass_score NUMBER := NULL;
--    v_pkg_pt_diff NUMBER := 1.;
--    v_pkg_ot_diff NUMBER := 10.;
--    v_pkg_loc_diff NUMBER := 10.;
--    v_pkg_have_score_table BOOLEAN := FALSE; 
--    v_pkg_sql_str VARCHAR2(128) := 'SELECT pickTimeDiff,originTimeDiff,horizDiff FROM eventmatchpassingscore WHERE auth = :net';
--
--  BEGIN
--    -- median p-time difference is less than some value (1 sec default).
--    -- origin time < 10 sec default 
--    -- epicentral distance < 2*(erh1+erh2) (10 km default)
--    -- where erh (o.erhor) is the hypoinverse horizontal uncertainty for the two events
--
--    -- check whether the database has an eventmatchpassingscore table.
--    SELECT count(*) INTO v_count FROM pg_tables WHERE table_name = 'EVENTMATCHPASSINGSCORE';
--    IF (v_count > 0) THEN
--      SELECT pickTimeDiff,originTimeDiff,horizDiff INTO v_pkg_pt_diff, v_pkg_ot_diff, v_pkg_loc_diff FROM eventmatchpassingscore WHERE auth = p_auth;
--    END IF;
--    GET DIAGNOSTICS v_count = ROW_COUNT;
--    IF (v_count = 0) THEN
--      -- nothing specified in table, assign default values
--      v_pkg_pt_diff := 1.;
--      v_pkg_ot_diff := 10.;
--      v_pkg_loc_diff := 10.;
--    END IF;
--    --
--    v_pkg_pass_score := v_pkg_pt_diff + v_pkg_ot_diff + v_pkg_loc_diff;
--    --
--    RETURN v_pkg_pass_score;
--  EXCEPTION
--    WHEN OTHERS THEN
--      --DBMS_OUTPUT.PUT_LINE('Oracle Error: ' || TO_CHAR(SQLCODE) || ' ' || SUBSTR(SQLERRM, 1, 100));
--      v_pkg_pt_diff := 1.;
--      v_pkg_ot_diff := 10.;
--      v_pkg_loc_diff := 10.;
--      v_pkg_pass_score := v_pkg_pt_diff + v_pkg_ot_diff + v_pkg_loc_diff;
--      RETURN v_pkg_pass_score;
--  END;
--$$ LANGUAGE plpgsql;
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--
CREATE OR REPLACE FUNCTION match.getThresholds(p_auth VARCHAR(15)) RETURNS SETOF eventmatchpassingscore AS $$
DECLARE
  v_row eventmatchpassingscore%ROWTYPE;
BEGIN
  FOR v_row IN SELECT * FROM eventmatchpassingscore WHERE auth=p_auth LOOP
      RETURN NEXT v_row;
  END LOOP;
END;
$$ LANGUAGE plpgsql;


CREATE OR REPLACE FUNCTION match.getScore(p_org1 ORIGIN, p_org2 ORIGIN) RETURNS DOUBLE PRECISION AS $$
  -- score = median phase arrival time difference if it is < v_pkg_pt_diff, if not then:
  --       = median phase arrival time difference + abs(origin time diff) + least(horizontal location diff, v_pkg_loc_diff)
  --         if horizontal diff < 2*(orig1.erhor + orig2.erhor), else:
  --       = v_pkg_max_score
  -- Or: 
  --    likely duplicates if median phase time difference is small
  --    not duplicates if the location difference is large
  --    all other case:  calculate total score
  -- effectively, this down-weighs the location difference and makes the time differences most important, specially the
  -- median phase time difference.
  DECLARE
    -- default thresholds (oracle version: package global variables)
    v_row EVENTMATCHPASSINGSCORE%ROWTYPE;
    v_pkg_max_score DOUBLE PRECISION  := 9999999999.;
    v_pkg_pass_score DOUBLE PRECISION := NULL;
    v_pkg_pt_diff DOUBLE PRECISION := 1.;
    v_pkg_ot_diff DOUBLE PRECISION := 10.;
    v_pkg_loc_diff DOUBLE PRECISION := 10.;
    --
    v_count INTEGER := 0;
    v_median_diff DOUBLE PRECISION := 0.;
    v_max_horiz_diff DOUBLE PRECISION := 0.;
    v_dist_diff DOUBLE PRECISION := 0.;
    v_score DOUBLE PRECISION := 9999999999.;
    -- 
    v_pt1 VARCHAR;
    v_pt2 VARCHAR;
  BEGIN
    -- First, retrieve what thresholds to use for determining whether an origin is a duplicate.
    -- check whether the database has an eventmatchpassingscore table.
    SELECT * INTO v_row FROM match.getThresholds(p_org1.auth);
    GET DIAGNOSTICS v_count = ROW_COUNT;
    IF (v_count = 0) THEN
      -- nothing specified in table, assign default values
      v_pkg_pt_diff := 1.;
      v_pkg_ot_diff := 10.;
      v_pkg_loc_diff := 10.;
    ELSE
      v_pkg_pt_diff := v_row.picktimediff;
      v_pkg_ot_diff := v_row.origintimediff;
      v_pkg_loc_diff := v_row.horizdiff;
    END IF;
    --
    v_pkg_pass_score := v_pkg_pt_diff + v_pkg_ot_diff + v_pkg_loc_diff;
    --
    RAISE NOTICE USING MESSAGE='Duplicate if total score < ' || v_pkg_pass_score;
    --
    RAISE NOTICE USING MESSAGE='DEBUG org1 datetime:' || p_org1.datetime;
    RAISE NOTICE USING MESSAGE='DEBUG org2 datetime:' || p_org2.datetime;

    IF ((p_org1.datetime IS NULL) OR (p_org2.datetime IS NULL)) THEN
      RETURN v_pkg_max_score;
    END IF;
    --
    --  If query below is too inefficient, instead implement code here to create 
    --  two separate collections of arrivals
    --  loop over these to find the common channel pick differences
    --  then calc median difference.
    --
    SELECT ABS(PERCENTILE_CONT(.5) WITHIN GROUP (ORDER BY (aa.datetime-bb.datetime) ASC))
    INTO v_median_diff FROM assocaro a, assocaro b, arrival aa, arrival bb 
    WHERE a.orid = p_org1.orid AND b.orid = p_org2.orid AND aa.arid=a.arid AND bb.arid=b.arid 
    AND aa.net=bb.net AND aa.sta=bb.sta AND aa.seedchan=bb.seedchan AND aa.location=bb.location
    AND a.iphase = b.iphase;

    GET DIAGNOSTICS v_count=ROW_COUNT;
    IF (v_count = 0) THEN
        -- make sure it is bigger than threshold, to avoid returning it.
        v_median_diff := (0.1 + v_pkg_pt_diff);
    END IF;
    --
    IF (v_median_diff IS NULL) THEN
      v_median_diff := 0.;
    ELSIF (v_median_diff <= v_pkg_pt_diff) THEN
      RAISE NOTICE USING MESSAGE='DEBUG v_median_diff:' || v_median_diff;
      RETURN v_median_diff;
    END IF;
    --
    v_score := v_median_diff;
    --
    IF (p_org1.erHor IS NOT NULL AND p_org2.erHor IS NOT NULL) THEN
      v_max_horiz_diff := 2*(p_org1.erHor + p_org2.erHor);
    END IF;
    IF (v_max_horiz_diff <= 0. ) THEN
      v_max_horiz_diff := v_pkg_loc_diff;
    END IF;
    RAISE NOTICE USING MESSAGE='DEBUG v_max_horiz_diff :' || v_max_horiz_diff;
    RAISE NOTICE USING MESSAGE='DEBUG ot1-ot2 diff: ' || ABS(p_org1.datetime - p_org2.datetime);
    --  
    IF ( ABS(p_org1.datetime - p_org2.datetime) <= v_pkg_ot_diff ) THEN
      v_score := v_score + ABS(p_org1.datetime - p_org2.datetime);
      v_pt1 := 'SRID=4326;POINT(' || p_org1.lon || ' ' || p_org1.lat || ')';
      v_pt2 := 'SRID=4326;POINT(' || p_org2.lon || ' ' || p_org2.lat || ')';
      -- calculate distance in km
      v_dist_diff := ST_Distance(ST_GeographyFromText(v_pt1),ST_GeographyFromText(v_pt2))/1000.0;
      RAISE NOTICE USING MESSAGE='DEBUG v_dist_diff: ' || v_dist_diff;
      IF (v_dist_diff <= v_max_horiz_diff ) THEN
        v_score := v_score + LEAST(v_dist_diff, v_pkg_loc_diff);
        RETURN v_score;
      END IF;
    END IF;
    --  If it gets this far, return v_pkg_max_score (not duplicates)
    RETURN v_pkg_max_score;
  END;
$$ LANGUAGE plpgsql;
--
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--
CREATE OR REPLACE FUNCTION match.getScore(p_evid1 BIGINT, p_evid2 BIGINT) RETURNS DOUBLE PRECISION AS $$
  DECLARE
    v_pkg_max_score DOUBLE PRECISION  := 9999999999.;
    v_org1_rec ORIGIN%ROWTYPE;
    v_org2_rec ORIGIN%ROWTYPE;
  BEGIN
      SELECT * INTO v_org1_rec FROM origin o WHERE orid = (SELECT prefor FROM event WHERE evid = p_evid1);
      SELECT * INTO v_org2_rec FROM origin o WHERE orid = (SELECT prefor FROM event WHERE evid = p_evid2);
      -- now get score
      RETURN match.getScore(v_org1_rec, v_org2_rec);
  EXCEPTION
      WHEN NO_DATA_FOUND THEN
        RETURN v_pkg_max_score;
      WHEN OTHERS THEN
        --DBMS_OUTPUT.PUT_LINE('Oracle Error: ' || TO_CHAR(SQLCODE) || ' ' || SUBSTR(SQLERRM, 1, 100));
        RETURN v_pkg_max_score;
  END;
$$ LANGUAGE plpgsql;
--
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--
  -- Get all valid dbase events in the time window defined by this
  -- epoch centerTime +- the values of windowSize.
  --
CREATE OR REPLACE FUNCTION match.getEventsInWindow(p_ot DOUBLE PRECISION) RETURNS SETOF origin AS $$
  DECLARE
    v_pkg_window_size DOUBLE PRECISION := 60.0;
  BEGIN  
    RETURN QUERY SELECT o.* FROM origin o, event e
      WHERE e.selectflag = 1 AND o.orid = e.prefor AND
      --  Do we need: o.bogusflag = 0 AND ? -aww removed 2008/03/12
      o.datetime BETWEEN
      truetime.putEpoch(p_ot-v_pkg_window_size,'UTC') AND
      truetime.putEpoch(p_ot+v_pkg_window_size,'UTC')
      ORDER BY o.datetime,o.lddate ASC;
  EXCEPTION
    WHEN NO_DATA_FOUND THEN
      RETURN NEXT NULL;
    WHEN OTHERS THEN
      --DBMS_OUTPUT.PUT_LINE('Oracle Error: ' || TO_CHAR(SQLCODE) || ' ' || SUBSTR(SQLERRM, 1, 100));
      RETURN NEXT NULL;
  END;
$$ LANGUAGE plpgsql;
--
------------------------------------------------------------------------------------
------------------------------------------------------------------------------------
--
  -- When input origin evid is not null, find a matching record with a different evid, 
  -- else the evid in selection window with the lowest passing score.
  -- returns 0 if there are no other events in time window.
CREATE OR REPLACE FUNCTION match.getMatch(p_org_row ORIGIN) RETURNS BIGINT AS $$
  DECLARE
    v_org_rec ORIGIN%ROWTYPE;
    v_row EVENTMATCHPASSINGSCORE%ROWTYPE;
    v_pkg_max_score DOUBLE PRECISION  := 9999999999.;
    v_match_evid origin.evid%TYPE := 0;
    v_best_score DOUBLE PRECISION := v_pkg_max_score;
    v_pass_score DOUBLE PRECISION := 0;
    v_score DOUBLE PRECISION := 0.;
    v_count INTEGER := 0;
  BEGIN
    --
    SELECT * INTO v_row FROM match.getThresholds(p_org_row.auth);
    GET DIAGNOSTICS v_count = ROW_COUNT;
    IF (v_count = 0) THEN
      -- nothing specified in table, assign default values
      v_pass_score := 21.0;
    ELSE
      v_pass_score := v_row.picktimediff + v_row.origintimediff + v_row.horizdiff;
    END IF;
    --
    RAISE NOTICE USING MESSAGE='DEBUG passing score:' || v_pass_score;
    --
    FOR v_org_rec IN SELECT * FROM match.getEventsInWindow(p_org_row.datetime) LOOP
      IF (p_org_row.evid IS NOT NULL AND p_org_row.evid = v_org_rec.evid) THEN
        -- skip this origin
        NULL;
      ELSIF ( (p_org_row.subsource IS NULL) OR (p_org_row.subsource = v_org_rec.subsource) ) THEN
          v_score := match.getScore(p_org_row, v_org_rec);
          --DBMS_OUTPUT.PUT_LINE('DEBUG MATCH.getMatch() evid: ' || p_org_row.evid || ' vs. evid: ' || v_org_recs(i).evid || ' score: ' || v_score);
          IF (v_score <= v_pass_score AND v_score < v_best_score) THEN
              v_best_score := v_score;
              v_match_evid := v_org_rec.evid;
          END IF;
      END IF;
    END LOOP;
  --
    RAISE NOTICE USING MESSAGE='DEBUG best score: ' || v_best_score || ', evid: ' || v_match_evid;
    RETURN v_match_evid;
  --
  END;
$$ LANGUAGE plpgsql;
  --
  --
  -- Return a match where Origin.subsource field must match input subsource 
  -- if input subsource is not null, otherwise any subsource is valid.
  -- Note that it is Origin.subsource and not Event.subsource that is checked.
  -- Event.subsource will not be changed from an RT or Jiggle value.
  --
--
--
CREATE OR REPLACE FUNCTION match.getMatch(p_evid BIGINT, p_orid BIGINT, p_auth VARCHAR(15), p_lat DOUBLE PRECISION, p_lon DOUBLE PRECISION,
                    p_datetime DOUBLE PRECISION, p_subsource VARCHAR(8)) RETURNS BIGINT AS $$
  DECLARE
    v_org_row ORIGIN%ROWTYPE;
  BEGIN
    v_org_row.evid      := p_evid;
    v_org_row.orid      := p_orid;
    v_org_row.auth      := p_auth;
    v_org_row.lat       := p_lat;
    v_org_row.lon       := p_lon;
    v_org_row.datetime  := p_datetime;
    v_org_row.subsource := p_subsource;
    RETURN match.getMatch(v_org_row);
  END;
$$ LANGUAGE plpgsql;
